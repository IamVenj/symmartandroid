import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/item_details.dart';
import 'package:flutter/material.dart';
import 'package:symmart/model/Wish.dart';

class WishScreenData extends StatelessWidget {
  final String toolBarName;
  final List<Wish> wishes;
  final setting;
  const WishScreenData(
      {Key key, this.toolBarName, this.wishes, @required this.setting})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    void showInSnackBar(String value) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value),
        duration: Duration(seconds: 1),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
      ));
    }

    void showSuccessInSnackBar(String value) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(value),
        backgroundColor: Color(0xFF13a89e),
        elevation: 10.0,
        duration: Duration(seconds: 2),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight:
                    Radius.circular(MediaQuery.of(context).size.width * 0.05),
                topLeft:
                    Radius.circular(MediaQuery.of(context).size.width * 0.05))),
      ));
    }

    return new Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          Expanded(
            child: BlocProvider(
                bloc: WishBloc(),
                child: WishBlocSreen(
                    wishes: wishes,
                    showInSnackBar: showInSnackBar,
                    showSuccessInSnackBar: showSuccessInSnackBar,
                    setting: setting)),
          ),
        ],
      ),
    );
  }
}

class WishBlocSreen extends StatelessWidget {
  final List<Wish> wishes;
  final void Function(String value) showInSnackBar;
  final void Function(String value) showSuccessInSnackBar;
  final setting;
  const WishBlocSreen(
      {Key key,
      @required this.wishes,
      this.showInSnackBar,
      this.showSuccessInSnackBar,
      @required this.setting})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double width = size.width;
    final double height = size.height;
    WishBloc _wishBloc = BlocProvider.of<WishBloc>(context);
    return StreamBuilder<List<Wish>>(
      initialData: wishes,
      stream: _wishBloc.wishController.stream,
      builder: (context, AsyncSnapshot<List<Wish>> snapshot) {
        if (snapshot.hasError) {
          return empty();
        }
        if (snapshot.hasData) {
          if (snapshot.data.length == 0) {
            return Center(
              child: Container(
                  width: width * 0.9,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(height: height * 0.1, child: empty()),
                      Text(
                        'EMPTY',
                        style: TextStyle(
                            fontSize: width * 0.1,
                            fontWeight: FontWeight.bold,
                            letterSpacing: width * 0.01,
                            decoration: TextDecoration.overline),
                      ),
                      Text(
                        "Your wishlist is empty!",
                        style: TextStyle(fontSize: width * 0.06),
                      ),
                    ],
                  )),
            );
          } else {
            if (snapshot.data.first.responseCode != "401") {
              return TravelDestinationItem(
                  destination: snapshot.data,
                  showInSnackBar: showInSnackBar,
                  showSuccessInSnackBar: showSuccessInSnackBar,
                  wishes: wishes,
                  setting: setting,
                  wishbloc: _wishBloc);
            } else {
              return Center(
                child: Container(
                    alignment: Alignment.center,
                    width: width * 0.9,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('SORRY',
                            style: TextStyle(
                                fontSize: width * 0.1,
                                fontWeight: FontWeight.bold)),
                        Center(
                          child: Text("You are not logged in yet! Please login",
                              textScaleFactor: 0.8,
                              style: TextStyle(
                                fontSize: width * 0.06,
                              )),
                        ),
                      ],
                    )),
              );
            }
          }
        } else {
          return empty();
        }
      },
    );
  }
}

class TravelDestinationItem extends StatelessWidget {
  final List<Wish> destination;
  final ShapeBorder shape;
  final showSuccessInSnackBar;
  final showInSnackBar;
  final List<Wish> wishes;
  final setting;
  final WishBloc wishbloc;
  const TravelDestinationItem(
      {Key key,
      this.destination,
      this.shape,
      this.showSuccessInSnackBar,
      this.showInSnackBar,
      this.wishes,
      @required this.setting,
      this.wishbloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    removeWish(productId, index) async {
      showInSnackBar("Please wait for a moment...");

      var token = await ApiMain().getToken();
      var response =
          await ApiMain().deleteData(productId, '/wish/remove', token);

      if (response.statusCode == 200) {
        var body = json.decode(response.body);

        if (body["status"] == 1) {
          wishes.removeWhere((test) {
            return int.parse(test.product.id) == int.parse(productId);
          });
          wishbloc.wishController.sink.add(wishes);
          showSuccessInSnackBar("Successfully removed!");
        }
      }
    }

    final ThemeData theme = Theme.of(context);
    final TextStyle descriptionStyle = theme.textTheme.subhead;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double _safeAreaHorizontal = MediaQuery.of(context).padding.left +
        MediaQuery.of(context).padding.right;
    double safeBlockHorizontal = (width - _safeAreaHorizontal) / 100;
    return GridView.builder(
      padding: const EdgeInsets.all(4.0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: (width / (height - height * 0.3))),
      itemCount: destination.length,
      itemBuilder: (BuildContext context, int index) {
        return SafeArea(
            top: true,
            bottom: false,
            child: Container(
                padding: const EdgeInsets.all(4.0),
                height: height,
                width: width * 0.645,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ItemDetails(
                                  toolbarname:
                                      destination[index].product.productName,
                                  productId: destination[index].product.id,
                                  setting: setting,
                                )));
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(width * 0.05),
                            bottomRight: Radius.circular(width * 0.05),
                            bottomLeft: Radius.circular(width * 0.05))),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    borderOnForeground: true,
                    semanticContainer: true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.17,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                child: CachedNetworkImage(
                                  imageUrl: destination[index]
                                      .product
                                      .productImage
                                      .first
                                      .imagePublicId,
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => loader(),
                                  errorWidget: (context, url, error) =>
                                      errorW(),
                                ),
                              ),
                              (destination[index].product.oldPrice != null)
                                  ? Container(
                                      alignment: Alignment.topRight,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Color(0xFF13A89E),
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(
                                                    width * 0.04))),
                                        padding: EdgeInsets.all(width * 0.02),
                                        child: Text(
                                          getPercentage(
                                              destination[index]
                                                  .product
                                                  .currentPrice,
                                              destination[index]
                                                  .product
                                                  .oldPrice),
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: width * 0.05,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    )
                                  : Container(),
                              Positioned(
                                top: 0,
                                left: 0,
                                child: FlatButton.icon(
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    size: width * 0.06,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    removeWish(
                                        destination[index].product.id, index);
                                  },
                                  label: Container(),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(width),
                                    bottomLeft: Radius.circular(width),
                                    bottomRight: Radius.circular(width),
                                  )),
                                  color: Color(0xFF13A89E),
                                  disabledColor: Colors.red,
                                  // tooltip: 'Add to wish',
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.fromLTRB(
                                14.0, 10.0, 10.0, 0.0),
                            child: DefaultTextStyle(
                              style: descriptionStyle,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 4.0, right: 8.0),
                                        child: Text(
                                          destination[index]
                                                  .product
                                                  .currentPrice +
                                              ' Birr',
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: width * 0.049,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                      (destination[index].product.oldPrice !=
                                              null)
                                          ? Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 6.0),
                                              child: Text(
                                                destination[index]
                                                        .product
                                                        .oldPrice +
                                                    ' Birr',
                                                textScaleFactor: 0.8,
                                                style:
                                                    descriptionStyle.copyWith(
                                                        color: Colors
                                                            .deepOrangeAccent,
                                                        fontSize: width * 0.042,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough),
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(bottom: height * 0.008),
                                    child: Text(
                                      (destination[index]
                                                  .product
                                                  .productName
                                                  .length >
                                              30)
                                          ? destination[index]
                                                  .product
                                                  .productName
                                                  .substring(0, 30) +
                                              ' ...'
                                          : destination[index]
                                              .product
                                              .productName,
                                      textScaleFactor: 0.8,
                                      style: descriptionStyle.copyWith(
                                          color: Colors.black87,
                                          fontSize: safeBlockHorizontal * 4.0,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Text(
                                        'Sold by: ',
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize:
                                                safeBlockHorizontal * 3.4),
                                      ),
                                      Text(
                                        (destination[index]
                                                    .product
                                                    .user
                                                    .shopName
                                                    .length >
                                                13)
                                            ? destination[index]
                                                    .product
                                                    .user
                                                    .shopName
                                                    .substring(0, 13) +
                                                ' ...'
                                            : destination[index]
                                                .product
                                                .user
                                                .shopName,
                                        textScaleFactor: 0.8,
                                        softWrap: true,
                                        style: descriptionStyle.copyWith(
                                            color: Colors.black87,
                                            fontSize: safeBlockHorizontal * 3.4,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )));
      },
    );
  }
}

String getPercentage(String currentPrice, String oldPrice) {
  double percentage =
      ((int.parse(currentPrice) / int.parse(oldPrice)) * 100) - 100;
  return percentage.toStringAsFixed(1) + '%';
}
