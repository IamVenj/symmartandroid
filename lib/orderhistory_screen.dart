import 'dart:convert';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/subOrderHistory.dart';

class OrderHistory extends StatefulWidget {
  final String toolbarname;
  final List<MainOrder> orders;

  OrderHistory({Key key, @required this.toolbarname, @required this.orders})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => StateOrderHistory(toolbarname, orders);
}

class Item {
  final String name;
  final String deliveryTime;
  final String oderId;
  final String oderAmount;
  final String paymentType;
  final String address;
  final String cancelOder;

  Item(
      {this.name,
      this.deliveryTime,
      this.oderId,
      this.oderAmount,
      this.paymentType,
      this.address,
      this.cancelOder});
}

class StateOrderHistory extends State<OrderHistory> {
  OrderApi orderApi = OrderApi();
  List list = ['12', '11'];
  bool checkboxValueA = true;
  bool checkboxValueB = false;
  bool checkboxValueC = false;
  VoidCallback showBottomSheetCallback;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String toolbarname;
  List<MainOrder> orders;

  StateOrderHistory(this.toolbarname, this.orders);

  @override
  Widget build(BuildContext context) {
    // IconData _backIcon() {
    //   switch (Theme.of(context).platform) {
    //     case TargetPlatform.android:
    //     case TargetPlatform.fuchsia:
    //       return Icons.arrow_back;
    //     case TargetPlatform.iOS:
    //       return Icons.arrow_back_ios;
    //   }
    //   assert(false);
    //   return null;
    // }

    var size = MediaQuery.of(context).size;
    double width = size.width;
    double height = size.height;

    /*24 is for notification bar on Android*/
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    // final double itemWidth = size.width / 2;

    // final Orientation orientation = MediaQuery.of(context).orientation;
    return new Scaffold(
        key: _scaffoldKey,
        body: (orders.isNotEmpty)
            ? (orders.first.responseCode != "401")
                ? BlocProvider(
                    bloc: MainOrderBloc(),
                    child: MainOrderW(
                      mainOrder: orders,
                      key: PageStorageKey("MainOrders"),
                      showInSnackBar: showInSnackBar,
                      showSuccessInSnackBar: showSuccessInSnackBar,
                    ),
                  )
                : Center(
                    child: Container(
                        width: width * 0.9,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('SORRY',
                                style: TextStyle(
                                    fontSize: width * 0.1,
                                    fontWeight: FontWeight.bold)),
                            Center(
                              child: Text(
                                  "You are not logged in yet! Please login",
                                  textScaleFactor: 0.8,
                                  style: TextStyle(
                                    fontSize: width * 0.06,
                                  )),
                            ),
                          ],
                        )),
                  )
            : Center(
                child: Container(
                    width: width * 0.9,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(height: height * 0.1, child: empty()),
                        Text('EMPTY',
                            style: Theme.of(context).textTheme.display2),
                        Center(
                          child: Text("You have not ordered anything!",
                              style: Theme.of(context).textTheme.headline),
                        ),
                      ],
                    )),
              ));
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  erticalD() => Container(
        margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
      );

  bool a = true;
  String mText = "Press to hide";
}

class MainOrderW extends StatelessWidget {
  MainOrderW(
      {Key key,
      @required this.mainOrder,
      this.showInSnackBar,
      this.showSuccessInSnackBar})
      : super(key: key);
  final List<MainOrder> mainOrder;
  final void Function(String value) showInSnackBar;
  final void Function(String value) showSuccessInSnackBar;

  MainOrderBloc mainOrderBloc;

  @override
  Widget build(BuildContext context) {
    mainOrderBloc = BlocProvider.of<MainOrderBloc>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return StreamBuilder<List<MainOrder>>(
        initialData: mainOrder,
        stream: mainOrderBloc.streamMainOrder,
        builder: (context, snapshot) {
          return (snapshot.hasData && mainOrder.isNotEmpty)
              ? ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext cont, int ind) {
                    return SafeArea(
                        child: Column(children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 5.0, top: 10.0),
                          // color:Colors.black,
                          child: Card(
                              elevation: 2.0,
                              child: Container(
                                  padding: const EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 10.0),
                                  child: GestureDetector(
                                      child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      // three line description
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                width: width * 0.5,
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  "#${snapshot.data[ind].orderCode}",
                                                  textScaleFactor: 0.8,
                                                  style: TextStyle(
                                                    fontSize: width * 0.045,
                                                    fontStyle: FontStyle.normal,
                                                    color: Colors.black87,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    top: height * 0.006),
                                              ),
                                              Container(
                                                width: width * 0.5,
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  (snapshot.data[ind]
                                                              .deliveryPersonnel !=
                                                          null)
                                                      ? 'Delivery Personnel : ' +
                                                          snapshot
                                                              .data[ind]
                                                              .deliveryPersonnel
                                                              .name
                                                      : 'Delivery Personnel : ' +
                                                          'Not Assigned yet',
                                                  textScaleFactor: 0.8,
                                                  style: TextStyle(
                                                      fontSize: width * 0.034,
                                                      color: Colors.black54),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            // width: width * 0.5,
                                            child: Text(
                                              'Order Pin: ${snapshot.data[ind].orderPin}',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          )
                                        ],
                                      ),

                                      Divider(
                                        height: 10.0,
                                        color: Color(0xFF13A89E),
                                      ),

                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Delivery Method',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    width: width * 0.3,
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      snapshot.data[ind]
                                                          .deliveryMethod,
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Payment Method',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      snapshot.data[ind]
                                                          .paymentMethod,
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Payment Status',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      (snapshot.data[ind]
                                                                  .paymentStatus ==
                                                              "0")
                                                          ? 'Not Paid'
                                                          : 'Paid',
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ],
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Color(0xFF13A89E),
                                      ),

                                      Divider(
                                        height: 10.0,
                                        color: Color(0xFF13A89E),
                                      ),

                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Delivery Price',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      "${snapshot.data[ind].deliveryPrice} Birr",
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Discount',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      snapshot.data[ind]
                                                              .discountPrice +
                                                          ' Birr',
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Total Payment',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      snapshot.data[ind]
                                                              .totalPrice +
                                                          " Birr",
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding:
                                                  EdgeInsets.all(width * 0.006),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Bank Code',
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: width * 0.006),
                                                    child: Text(
                                                      snapshot.data[ind]
                                                          .uniqueBankCode,
                                                      textScaleFactor: 0.8,
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ],
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Color(0xFF13A89E),
                                      ),

                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Icon(
                                            Icons.location_on,
                                            size: width * 0.04,
                                            color: Color(0xFF13A89E),
                                          ),
                                          Text(
                                              snapshot.data[ind].customer
                                                  .addresses.first.address,
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black54)),
                                        ],
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Color(0xFF13A89E),
                                      ),
                                      Container(
                                          child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          moreDetail(
                                              context,
                                              snapshot.data[ind].orderCode,
                                              snapshot.data[ind].order,
                                              snapshot.data[ind]),
                                          _status(
                                              snapshot.data[ind]
                                                  .vendorDeliveryConfirmation,
                                              snapshot.data[ind].id),
                                        ],
                                      ))
                                    ],
                                  ))))),
                    ]));
                  })
              : Center(
                  child: Container(
                      width: width * 0.9,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(height: height * 0.1, child: empty()),
                          Text(
                            'EMPTY',
                            style: TextStyle(
                                fontSize: width * 0.1,
                                fontWeight: FontWeight.bold,
                                letterSpacing: width * 0.01,
                                decoration: TextDecoration.overline),
                          ),
                          Text(
                            "You have not ordered anything!",
                            style: TextStyle(fontSize: width * 0.06),
                          ),
                        ],
                      )),
                );
        });
  }

  showSubOrder(BuildContext context, orderCode, order, mainOrderd) {
    return Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SubOrderHistory(
        toolbarname: orderCode,
        order: order,
        mainOrder: mainOrderd,
      );
    }));
  }

  Widget moreDetail(BuildContext context, orderCode, order, mainOrderd) {
    return FlatButton.icon(
        label: Text(
          "View More",
          style: TextStyle(color: Colors.green),
        ),
        icon: const Icon(
          Icons.details,
          size: 18.0,
          color: Colors.green,
        ),
        color: Colors.green.shade100.withOpacity(0.2),
        onPressed: () {
          showSubOrder(context, orderCode, order, mainOrderd);
        });
  }

  Widget _status(vendorDeliveryConfirmation, orderId) {
    if (vendorDeliveryConfirmation == 1) {
      return FlatButton.icon(
          color: Colors.green.shade100.withOpacity(0.2),
          label: Text(
            "Order Delivered",
            style: TextStyle(color: Colors.green),
          ),
          icon: const Icon(
            Icons.check_circle_outline,
            size: 18.0,
            color: Colors.green,
          ),
          onPressed: null);
    } else {
      return FlatButton.icon(
          color: Colors.red.shade100.withOpacity(0.2),
          label: Text(
            "Cancel Order",
            style: TextStyle(color: Colors.red),
          ),
          icon: const Icon(
            Icons.remove_circle_outline,
            size: 18.0,
            color: Colors.red,
          ),
          onPressed: () {
            deleteOrder(orderId);
          });
    }
  }

  deleteOrder(orderId) async {
    showInSnackBar("please wait while cancelling your order!");
    ApiMain apiMain = new ApiMain();
    var token = await apiMain.getToken();
    var response =
        await apiMain.deleteData(orderId, '/main-order/delete', token);

    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      if (body["status"] == "success") {
        mainOrder.removeWhere((test) {
          return test.id == orderId;
        });
        mainOrderBloc.sinkMainOrder.add(mainOrder);
        showSuccessInSnackBar("Order is successfully cancelled!");
      } else if (body["status"] == "failed") {
        showInSnackBar("Sorry! Some problem has occured!");
      }
    }
  }
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}

Widget errorW() {
  return Center(
    child: FlareActor(
      "assets/flare/no wifi.flr",
      alignment: Alignment.center,
      fit: BoxFit.contain,
      animation: "init",
    ),
  );
}
