import 'dart:async';
import 'dart:convert';

import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/Wish.dart';
import 'package:symmart/model/productList.dart';

class CartBloc implements BlocBase {
  int _cartCount;

  StreamController<int> _cartController = StreamController<int>();
  StreamSink<int> get _inCartCount => _cartController.sink;
  Stream<int> get outCartCount => _cartController.stream;

  CartBloc() {
    init();
  }

  Future init() async {
    var user = await ApiMain().getUser();
    if (user != null) {
      _cartCount =
          await ClientDatabaseProvider.db.getCartTotalCount(user['id']);
    } else {
      _cartCount = 0;
    }
    _inCartCount.add(_cartCount);
  }

  @override
  void dispose() {
    _cartController.close();
  }
}

class UserBloc implements BlocBase {
  final _currentUserSubject = BehaviorSubject<Map<String, dynamic>>();
  Observable<Map<String, dynamic>> get outUser => _currentUserSubject.stream;

  StreamController<Map<String, dynamic>> _userController =
      StreamController<Map<String, dynamic>>.broadcast();
  StreamSink<Map<String, dynamic>> get inUser => _userController.sink;

  UserBloc() {
    if (!_currentUserSubject.isClosed) {
      init();
      _userController.stream.listen(_handle);
    }
  }

  _handle(json) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('user', jsonEncode(json['user']));
    var user = await jsonDecode(localStorage.getString('user'));
    _currentUserSubject.sink.add(user);
  }

  init() async {
    var _user = await ApiMain().getUser();
    _currentUserSubject.sink.add(_user);
  }

  @override
  void dispose() {
    _userController.close();
    _currentUserSubject.close();
  }
}

class WishBloc implements BlocBase {
  StreamController<List<Wish>> wishController =
      StreamController<List<Wish>>.broadcast();

  @override
  void dispose() {
    wishController.close();
  }
}

class MainOrderBloc implements BlocBase {
  StreamController<List<MainOrder>> _mainOrderController =
      StreamController<List<MainOrder>>.broadcast();
  Sink<List<MainOrder>> get sinkMainOrder => _mainOrderController.sink;
  Stream<List<MainOrder>> get streamMainOrder => _mainOrderController.stream;

  @override
  void dispose() {
    _mainOrderController.close();
  }
}

class ItemScreenBloc implements BlocBase {
  StreamController<ProductData> itemScreenProductController =
      StreamController<ProductData>.broadcast();

  @override
  void dispose() {
    itemScreenProductController.close();
  }
}
