import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sql.dart';
import 'package:sqflite/sqflite.dart';
import 'package:symmart/model/cart.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';

class ClientDatabaseProvider {
  ClientDatabaseProvider._();

  static final ClientDatabaseProvider db = ClientDatabaseProvider._();
  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstance();
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "symmart3.db");
    return await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE Cart ("
            "id integer primary key NOT NULL,"
            "name TEXT,"
            "qty integer,"
            "price double,"
            "size TEXT,"
            "color TEXT,"
            "userid integer,"
            "img TEXT NOT NULL,"
            "productid integer,"
            "packageid integer,"
            "associatedmodel TEXT NOT NULL"
            ")");
      },
    );
  }

  Future<List<CartModel>> getAllCart(int userId) async {
    final db = await database;
    var response =
        await db.query("Cart", where: "userid = ?", whereArgs: [userId]);
    List<CartModel> list = response.map((f) => CartModel.fromMap(f)).toList();
    return list;
  }

  Future<CartModel> getCartWithID(int id) async {
    final db = await database;
    var response = await db.query("Cart", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? CartModel.fromMap(response.first) : null;
  }

  addToCart(CartModel cart, productName) async {
    final db = await database;
    var raw;
    var response =
        await db.query("Cart", where: "name = ?", whereArgs: [productName]);
    if (response.isNotEmpty) {
      raw = null;
    } else {
      var table = await db.rawQuery("SELECT MAX(id)+1 as id from Cart");
      int id = table.first["id"];
      cart.id = id;
      raw = await db.insert("Cart", cart.toMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }

    return raw;
  }

  getCartTotalCount(int userid) async {
    final db = await database;
    var response =
        await db.query("Cart", where: "userid = ?", whereArgs: [userid]);
    return response.isNotEmpty ? response.length : 0;
  }

  destroyCartWithId(int id) async {
    final db = await database;
    return db.delete("Cart", where: "id = ?", whereArgs: [id]);
  }

  destroyCart(int userId) async {
    final db = await database;
    db.delete("Cart", where: "userid = ?", whereArgs: [userId]);
  }

  updateCartQty(CartModel cart) async {
    final db = await database;
    var response = await db
        .update("Cart", cart.toMap(), where: "id = ?", whereArgs: [cart.id]);
    return response;
  }
}
