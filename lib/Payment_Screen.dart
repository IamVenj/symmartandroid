import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Wish.dart';
import 'package:symmart/model/cart.dart';
import 'package:symmart/model/setting.dart';

class PaymentScreen extends StatefulWidget {
  final totalPrice;
  final List<CompanySettings> setting;
  final deliveryPrice;
  final discountPrice;
  final deliveryMethod;
  final List<CartModel> cartList;
  final subTotal;

  const PaymentScreen(
      {Key key,
      @required this.totalPrice,
      @required this.setting,
      @required this.deliveryPrice,
      @required this.discountPrice,
      @required this.deliveryMethod,
      @required this.cartList,
      @required this.subTotal})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => Payment();
}

class Payment extends State<PaymentScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<dynamic> newData = [];
  List<MainOrder> orders = [];
  List<Wish> wishes = [];
  Map<String, dynamic> homedata = {};

  IconData _backIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.arrow_back;
      case TargetPlatform.iOS:
        return Icons.arrow_back_ios;
    }
    assert(false);
    return null;
  }

  int _currentRadioIndex = 1;
  Map<String, int> _radioDelivery = {
    "Bank": 1,
    "Cash On Delivery": 2,
  };

  bool isPaying = false;
  String paymentMethod;
  bool payedButWaitTen = false;

  List iterableCart = [];
  mapCart() {
    widget.cartList.forEach((f) {
      iterableCart.add(f.toMap());
    });
  }

  @override
  void initState() {
    super.initState();
    setPaymentMethod();
    mapCart();
  }

  String toolbarname = 'CheckOut';

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    AppBar appBar = AppBar(
      leading: IconButton(
        icon: Icon(_backIcon()),
        alignment: Alignment.centerLeft,
        tooltip: 'Back',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      title: Text(toolbarname),
      backgroundColor: Colors.white,
      elevation: 0,
    );

    return new Scaffold(
      key: _scaffoldKey,
      appBar: appBar,
      body: Stack(children: <Widget>[
        new Column(
          children: <Widget>[
            Container(
                margin: EdgeInsets.all(5.0),
                child: Card(
                    elevation: 0,
                    child: Container(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // three line description
                            Padding(
                              padding: EdgeInsets.only(
                                  bottom: height * 0.02, top: height * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            'Delivery',
                                            textScaleFactor: 0.8,
                                            style: TextStyle(
                                                fontSize: width * 0.04,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black38),
                                          ),
                                          SizedBox(
                                            width: width * 0.02,
                                          ),
                                          Icon(
                                            Icons.play_circle_outline,
                                            color: Colors.black38,
                                          ),
                                          SizedBox(
                                            width: width * 0.06,
                                          ),
                                        ],
                                      )),
                                  Container(
                                      alignment: Alignment.center,
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            'Payment',
                                            textScaleFactor: 0.8,
                                            style: TextStyle(
                                                fontSize: width * 0.04,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black),
                                          ),
                                          SizedBox(
                                            width: width * 0.02,
                                          ),
                                          Icon(
                                            Icons.check_circle,
                                            color: Colors.green,
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ],
                        )))),
            _verticalDivider(),
            new Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(
                  left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
              child: new Text(
                'Payment Method',
                textScaleFactor: 0.8,
                style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                    fontSize: width * 0.05),
              ),
            ),
            _verticalDivider(),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  new Container(
                      height: height * 0.23,
                      // margin: EdgeInsets.all(10.0),
                      child: Card(
                        elevation: 0,
                        child: Container(
                            child: Column(
                                children: _radioDelivery.keys.map((f) {
                          return Column(
                            children: <Widget>[
                              RadioListTile(
                                groupValue: _currentRadioIndex,
                                activeColor: Color(0xFF13A89E),
                                value: _radioDelivery[f],
                                title: Text(
                                  f,
                                  style: TextStyle(fontSize: width * 0.04),
                                  textScaleFactor: 0.8,
                                ),
                                onChanged:
                                    (_radioDelivery[f] == _currentRadioIndex)
                                        ? (v) {
                                            setState(() {
                                              _currentRadioIndex = v;
                                            });
                                          }
                                        : null,
                              ),
                              Divider()
                            ],
                          );
                        }).toList())),
                      )),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  payedButWaitTen && !isPaying
                      ? Column(
                          children: <Widget>[
                            Text("Please wait while we wrap up..."),
                            SizedBox(
                              height: height * 0.02,
                            ),
                            Container(height: height * 0.1, child: loader()),
                          ],
                        )
                      : Container(),
                  isPaying
                      ? Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Text(
                              "PROCESSING ORDER...",
                              textAlign: TextAlign.center,
                              textScaleFactor: 0.8,
                              style: TextStyle(
                                  color: Colors.green,
                                  fontSize: width * 0.05,
                                  fontWeight: FontWeight.bold),
                            ),
                            Container(
                              // padding: EdgeInsets.only(top: height * 0.04),
                              height: height * 0.3,
                              width: width * 0.6,
                              child: CircularProgressIndicator(
                                strokeWidth: 2.0,
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.green),
                              ),
                            ),
                          ],
                        )
                      : Container()
                ],
              ),
            ),
          ],
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
              alignment: Alignment.bottomLeft,
              height: height * 0.08248,
              child: Card(
                elevation: 0,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(
                              left: width * 0.02, right: width * 0.02),
                          child: Icon(
                            Icons.account_balance,
                            color: Colors.green,
                            size: width * 0.04,
                          ),
                        ),
                        Text(
                          'Total :',
                          textScaleFactor: 0.8,
                          style: TextStyle(
                              fontSize: width * 0.04,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: EdgeInsets.all(3.0),
                        ),
                        Text(
                          '${widget.totalPrice} ETB',
                          textScaleFactor: 0.8,
                          style: TextStyle(
                              fontSize: width * 0.04, color: Colors.black54),
                        ),
                        Padding(
                          padding: EdgeInsets.all(3.0),
                        ),
                        Icon(
                          Icons.check_circle_outline,
                          color: Colors.green,
                          size: width * 0.04,
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: FlatButton.icon(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              size: width * 0.03,
                            ),
                            color: Colors.green.shade100.withOpacity(0.2),
                            label: Text(
                              'Proceed to Pay',
                              textScaleFactor: 0.8,
                              style: TextStyle(fontSize: width * 0.04),
                            ),
                            disabledColor: Color(0x8013A89E).withOpacity(0.2),
                            disabledTextColor: Color(0xFF13A89E),
                            textColor: Colors.green,
                            onPressed: !isPaying && !payedButWaitTen
                                ? () {
                                    pay();
                                  }
                                : null,
                            shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            )),
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ]),
    );
  }

  setPaymentMethod() {
    if (widget.totalPrice >
        int.parse(widget.setting.first.onlinePaymentLimit)) {
      setState(() {
        _currentRadioIndex = 1;
        paymentMethod = "bank";
      });
    } else {
      setState(() {
        _currentRadioIndex = 2;
        paymentMethod = "on-delivery";
      });
    }
  }

  pay() async {
    setState(() {
      isPaying = true;
      payedButWaitTen = true;
    });

    var data = {
      "payments": paymentMethod,
      "delivery_price": widget.deliveryPrice,
      "total_price_main": widget.totalPrice,
      "discount": widget.discountPrice,
      "deliveryMethod": widget.deliveryMethod,
      "cartList": iterableCart,
      "subtotal": widget.subTotal
    };

    String token = await ApiMain().getToken();
    var response = await ApiMain().postData(data, '/pay', token);
    var body = json.decode(response.body);

    if (response.statusCode == 200) {
      setState(() {
        isPaying = false;
      });
      if (body['status'] == 0) {
        showErrorInSnackBar(body['message']);
        Future.delayed(Duration(seconds: 5), () {
          setState(() {
            payedButWaitTen = false;
          });
        });
      }
      if (body['status'] == 1) {
        showSuccessInSnackBar(body['message']);
        Future.delayed(Duration(seconds: 3), () async {
          var user = await ApiMain().getUser();
          await ClientDatabaseProvider.db.destroyCart(user['id']);
          await HomeApi().getHomeData().then((v) {
            for (var item in v) {
              newData.add(item);
            }
            newData.forEach((f) {
              f.forEach((k, v) {
                homedata[k] = v;
              });
            });
          }).catchError((onError) {
            print(onError);
          });
          var token = await ApiMain().getToken();
          await OrderApi().getMainOrders(token).then((onValue) {
            for (var item in onValue) {
              orders.add(item);
            }
          }).catchError((onError) {
            print(onError);
          });
          await WishApi().getWishes().then((onValue) {
            for (var item in onValue) {
              wishes.add(item);
            }
          }).catchError((onError) {
            print(onError);
          });
          setState(() {
            payedButWaitTen = false;
          });
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return Homescreen(
              authStatus: null,
              data: homedata,
              orders: orders,
              wishes: wishes,
            );
          }));
        });
      }
    }
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.green,
      elevation: 10.0,
      duration: Duration(seconds: 5),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  void showErrorInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.red,
      elevation: 10.0,
      duration: Duration(seconds: 5),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  _verticalDivider() => Container(
        padding: EdgeInsets.all(2.0),
      );
}
