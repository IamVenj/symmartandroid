import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:image_picker/image_picker.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/cloudinary.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:symmart/model/Wish.dart';

const kGoogleApiKey = "AIzaSyBPCRMMp4wCB1RYgrj0lavgRLSn-v9kz8g";
GoogleMapsPlaces placesMap = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class ProfileImage extends StatefulWidget {
  ProfileImage({Key key}) : super(key: key);

  @override
  _ProfileImageState createState() => _ProfileImageState();
}

class _ProfileImageState extends State<ProfileImage> {
  bool isUploadingToCloudinary = false;
  UserBloc userBloc;
  File imageFile;

  @override
  Widget build(BuildContext context) {
    userBloc = BlocProvider.of<UserBloc>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return StreamBuilder<Map<String, dynamic>>(
        stream: userBloc.outUser,
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    new Container(
                        alignment: Alignment.center,
                        child: Container(
                          width: width * 0.20,
                          height: height * 0.092,
                          margin: const EdgeInsets.all(10.0),
                          child: ClipOval(
                            clipBehavior: Clip.antiAlias,
                            child: CachedNetworkImage(
                              imageUrl: (snapshot.data['photo_url'] != null)
                                  ? '${snapshot.data['photo_url']}'
                                  : 'https://symmart.com/public/images/default-avatar.png',
                              fit: BoxFit.cover,
                              alignment: Alignment.center,
                              placeholder: (context, url) => loader(),
                              errorWidget: (context, url, error) => empty(),
                            ),
                          ),
                        )),
                    isUploadingToCloudinary
                        ? LinearProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation(Color(0xFF24A89E)),
                            backgroundColor: Colors.white10,
                          )
                        : Container(),
                  ],
                ),
                new FlatButton.icon(
                  icon: Icon(Icons.image),
                  onPressed: () {
                    _showBottomSheet(context);
                  },
                  label: Text(
                    "Change",
                    textScaleFactor: 0.8,
                  ),
                  textColor: Color(0xFF13A89E),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width * 0.1)),
                  color: Color(0x8013A89E).withOpacity(0.1),
                ),
              ],
            );
          } else {
            return Container();
          }
        });
  }

  void _showBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        elevation: 1.0,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: Color(0xFF737373),
                height: MediaQuery.of(context).size.height * 0.18,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.18,
                  child: _buildBottomNavMenu(setState),
                  decoration: BoxDecoration(
                      color: Theme.of(context).canvasColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                ),
              );
            },
          );
        });
  }

  _openCamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = picture;
    });
    if (imageFile.path.length > 0) {
      Navigator.of(context).pop();
      var response = await saveProfileToCloudinary();
      await uploadToServer(response);
    } else {
      setState(() {
        isUploadingToCloudinary = false;
      });
    }
  }

  _openGallery(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState(() {
      imageFile = picture;
    });
    if (imageFile.path.length > 0) {
      Navigator.of(context).pop();
      var response = await saveProfileToCloudinary();
      await uploadToServer(response);
    }
  }

  saveProfileToCloudinary() async {
    setState(() {
      isUploadingToCloudinary = true;
    });
    String fileName = imageFile.path.split("/").last;
    CloudinaryResponse cResponse = await Cloudinary()
        .uploadImage(imageFile.absolute.path, fileName, "profile");
    return cResponse;
  }

  uploadToServer(CloudinaryResponse cResponse) async {
    var token = await ApiMain().getToken();
    var passedData = {"image": cResponse.secure_url};
    var response =
        await ApiMain().patchData(passedData, '/profile/update', token);
    if (response.statusCode == 200) {
      var body = await json.decode(response.body);
      userBloc.inUser.add(body);
      isUploadingToCloudinary = false;
    }
  }

  Column _buildBottomNavMenu(setModalState) {
    double height = MediaQuery.of(context).size.height;
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: height * 0.01,
        ),
        ListTile(
          dense: true,
          onTap: () {
            _openCamera(context);
          },
          leading: Icon(Icons.camera),
          title: Text(
            "Camera",
            textScaleFactor: 0.8,
          ),
        ),
        Divider(),
        ListTile(
          dense: true,
          onTap: () {
            _openGallery(context);
          },
          leading: Icon(Icons.image),
          title: Text(
            "Gallery",
            textScaleFactor: 0.8,
          ),
        )
      ],
    );
  }
}

class UserAccount extends StatelessWidget {
  _verticalDivider() => Container(
        padding: EdgeInsets.all(2.0),
      );

  UserAccount({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UserBloc _userBloc;
    _userBloc = BlocProvider.of<UserBloc>(context);
    return StreamBuilder<Map<String, dynamic>>(
        stream: _userBloc.outUser,
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return new Container(
              margin: EdgeInsets.only(
                  left: 10.0, top: 20.0, right: 5.0, bottom: 5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Text(
                    "${snapshot.data['name']}",
                    textScaleFactor: 0.8,
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 25.0,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.5,
                    ),
                  ),
                  _verticalDivider(),
                  new Text(
                    "${snapshot.data['phone_number']}",
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        color: Colors.black45,
                        fontSize: 13.0,
                        letterSpacing: 0.5),
                  ),
                  _verticalDivider(),
                  new Text(
                    "${snapshot.data['email']}",
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        color: Colors.black45,
                        fontSize: 13.0,
                        letterSpacing: 0.5),
                  )
                ],
              ),
            );
          } else {
            return Container();
          }
        });
  }
}

class DeliveryAddress extends StatelessWidget {
  const DeliveryAddress({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    UserBloc _userBloc = BlocProvider.of<UserBloc>(context);
    double width = MediaQuery.of(context).size.width;
    return StreamBuilder<Map<String, dynamic>>(
        stream: _userBloc.outUser,
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return Text(
              '${snapshot.data['addresses'][0]['address']}',
              textScaleFactor: 0.8,
              style: TextStyle(
                  color: Colors.black45,
                  fontSize: width * 0.04,
                  letterSpacing: width * 0.002),
            );
          } else {
            return Container();
          }
        });
  }
}

class AccountScreenData extends StatefulWidget {
  final tokenData;
  AccountScreenData({Key key, @required this.tokenData}) : super(key: key);
  _AccountScreenDataState createState() => _AccountScreenDataState();
}

class _AccountScreenDataState extends State<AccountScreenData> {
  List list = ['12', '11', '13'];
  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  final formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  bool _changePasswordLoading = false;

  var userData;

  getUserData() async {
    userData = await ApiMain().getUser();
  }

  @override
  void initState() {
    getUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return new Scaffold(
      key: scaffoldKey,
      body: new Container(
        child: SingleChildScrollView(
            child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          textDirection: TextDirection.ltr,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.all(7.0),
              alignment: Alignment.topCenter,
              height: 260.0,
              child: new Card(
                elevation: 1.0,
                child: Column(
                  children: <Widget>[
                    BlocProvider(
                      bloc: UserBloc(),
                      child: ProfileImage(),
                    ),

                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        BlocProvider(
                          bloc: UserBloc(),
                          child: UserAccount(),
                        ),
                        FlatButton.icon(
                            label: Container(),
                            icon: Icon(
                              Icons.edit,
                              color: Color(0xFF13A89E),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(width * 0.1),
                                    bottomLeft: Radius.circular(width * 0.1))),
                            color: Color(0xFF13A89E).withOpacity(0.2),
                            onPressed: () {
                              editProfile();
                            }),
                      ],
                    ),
                    // VerticalDivider(),
                  ],
                ),
              ),
            ),
            Container(
              height: height * 0.09,
              width: width,
              child: Card(
                margin: EdgeInsets.only(
                  left: width * 0.03,
                  right: width * 0.03,
                ),
                elevation: 1,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          top: width * 0.02, left: width * 0.02),
                      child: Container(
                        width: width * 0.6,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            BlocProvider(
                                bloc: UserBloc(), child: DeliveryAddress()),
                            SizedBox(height: height * 0.004),
                            new Text(
                              'Delivery Address',
                              textScaleFactor: 0.8,
                              style: TextStyle(
                                  fontSize: width * 0.04,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                    FlatButton.icon(
                        label: Container(),
                        icon: Icon(
                          Icons.edit_location,
                          color: Color(0xFF13A89E),
                          size: width * 0.08,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.4),
                                bottomLeft: Radius.circular(width * 0.4))),
                        color: Color(0xFF13A89E).withOpacity(0.2),
                        onPressed: () {
                          editAddress();
                        }),
                  ],
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.all(7.0),
              child: Card(
                elevation: 0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      left: 0,
                      top: 0,
                      bottom: 0,
                      right: width * 0.7,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(width * 0.4),
                                topRight: Radius.circular(width * 0.4)),
                            color: Colors.grey.shade100.withOpacity(0.7)),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: height * 0.06,
                          margin: EdgeInsets.only(left: width * 0.03),
                          width: width,
                          child: InkWell(
                            onTap: () {
                              changePasswordDialog(context);
                            },
                            child: Row(children: <Widget>[
                              new Icon(
                                Icons.vpn_key,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: width * 0.035,
                              ),
                              new Text(
                                'Change Password',
                                textScaleFactor: 0.8,
                                style: TextStyle(
                                  fontSize: width * 0.035,
                                  color: Colors.black87,
                                ),
                              )
                            ]),
                          ),
                        ),
                        Divider(),
                        Container(
                          height: height * 0.06,
                          margin: EdgeInsets.only(left: width * 0.03),
                          width: width,
                          child: InkWell(
                            onTap: !isLoading
                                ? () {
                                    logout();
                                  }
                                : null,
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      new Icon(
                                        Icons.power_settings_new,
                                        color: Colors.grey,
                                      ),
                                      SizedBox(
                                        width: width * 0.035,
                                      ),
                                      new Text(
                                        'Logout',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                          fontSize: width * 0.035,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ],
                                  ),
                                  isLoading
                                      ? Container(
                                          height: height * 0.025,
                                          width: width * 0.05,
                                          child: CircularProgressIndicator(
                                            strokeWidth: 1,
                                            valueColor: AlwaysStoppedAnimation(
                                                Color(0xFF13A89E)),
                                          ))
                                      : Container()
                                ]),
                          ),
                        ),
                        Divider(),
                        Container(
                          height: height * 0.06,
                          margin: EdgeInsets.only(left: width * 0.03),
                          width: width,
                          child: InkWell(
                            onTap: () {
                              deactivateAccount(context);
                            },
                            child: Row(children: <Widget>[
                              new Icon(
                                Icons.do_not_disturb_on,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: width * 0.035,
                              ),
                              new Text(
                                'Deactivate Account',
                                textScaleFactor: 0.8,
                                style: TextStyle(
                                  fontSize: width * 0.035,
                                  color: Colors.black87,
                                ),
                              )
                            ]),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }

  List<dynamic> newData = [];
  Map<String, dynamic> data = {};
  List<MainOrder> orders = [];
  List<Wish> wishes = [];

  getData() async {
    await HomeApi().getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }, onError: (e) {
      print(e);
    }).catchError((onError) {
      print(onError);
    });
    var error = {"responseCode": "401"};
    MainOrder mainOrder = MainOrder.fromJson(error);
    orders.add(mainOrder);
    Wish wishMain = Wish.fromJson(error);
    wishes.add(wishMain);
  }

  void _performLogout(data) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Homescreen(
                authStatus: false,
                data: data,
                orders: orders,
                wishes: wishes)));
  }

  void logout() async {
    setState(() {
      isLoading = true;
    });
    var response = await ApiMain().postData(null, "/logout", widget.tokenData);
    var body = json.decode(response.body);
    if (response.statusCode == 200) {
      if (body["success"]) {
        await getData();
        Map<String, dynamic> user = await ApiMain().getUser();
        await ClientDatabaseProvider.db.destroyCart(user['id']);
        await ApiMain().removeTokenAndUser();
        _performLogout(data);
        setState(() {
          isLoading = false;
        });
      } else {
        showInSnackBar('We are so sorry! an error has occured!');
      }
    }
    if (response.statusCode == 500) {
      showInSnackBar('We are so sorry! an error has occured!');
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  void showErrorInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.red,
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  _verticalDivider() => Container(
        padding: EdgeInsets.all(2.0),
      );

  Future<bool> changePasswordDialog(context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return ChangePasswordDialog(
              showSuccessInSnackBar: showSuccessInSnackBar,
              showErrorInSnackBar: showErrorInSnackBar);
        });
  }

  Future<bool> editProfile() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return BlocProvider(
              bloc: UserBloc(),
              child: EditProfileDialog(
                showSuccessInSnackBar: showSuccessInSnackBar,
                showErrorInSnackBar: showErrorInSnackBar,
                userData: userData,
              ));
        });
  }

  Future<bool> editAddress() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return BlocProvider(
            bloc: UserBloc(),
            child: UpdateAddressDialog(
              userData: userData,
              scaffoldKey: scaffoldKey,
              showSuccessInSnackBar: showSuccessInSnackBar,
              showErrorInSnackBar: showErrorInSnackBar,
            ),
          );
        });
  }

  Future<bool> deactivateAccount(ctx) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return DeactivateAccountDialog(
            showSuccessInSnackBar: showSuccessInSnackBar,
            ctx: ctx,
          );
        });
  }
}

class ChangePasswordDialog extends StatefulWidget {
  final showSuccessInSnackBar;
  final showErrorInSnackBar;
  ChangePasswordDialog(
      {Key key, this.showSuccessInSnackBar, this.showErrorInSnackBar})
      : super(key: key);

  @override
  _ChangePasswordDialogState createState() => _ChangePasswordDialogState();
}

class _ChangePasswordDialogState extends State<ChangePasswordDialog> {
  TextEditingController passwordOldEditController = new TextEditingController();
  TextEditingController passwordNewEditController = new TextEditingController();
  final formKey = GlobalKey<FormState>();

  bool _autovalidate = false;
  bool _changePasswordLoading = false;

  bool _obscureTextCurrent = true;
  bool _obscureTextNew = true;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.04)),
      child: Container(
        height: height * 0.5,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.08)),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Form(
              key: formKey,
              autovalidate: _autovalidate,
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: height * 0.18,
                      ),
                      Container(
                        height: height * 0.12,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.04),
                                topRight: Radius.circular(width * 0.04)),
                            color: Color(0xFF13A89E).withOpacity(0.2)),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: height * 0.06,
                          ),
                          Center(
                            child: ClipOval(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: Container(
                                padding: EdgeInsets.all(width * 0.02),
                                color: Colors.white.withOpacity(0.8),
                                child: Icon(
                                  Icons.lock_open,
                                  size: width * 0.2,
                                  color: Color(0xFF13A89E),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02,
                        right: width * 0.02,
                        top: height * 0.01),
                    child: TextFormField(
                      obscureText: _obscureTextCurrent,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.017),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon:
                              Icon(Icons.lock_outline, color: Colors.grey),
                          suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                if (_obscureTextCurrent == true) {
                                  _obscureTextCurrent = false;
                                } else if (_obscureTextCurrent == false) {
                                  _obscureTextCurrent = true;
                                }
                              });
                            },
                            child: Icon(
                              Icons.remove_red_eye,
                              color: Colors.grey,
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'Your password',
                          labelText: 'Current Password',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          passwordOldEditController.text.length < 3
                              ? 'Password too short.'
                              : null,
                      controller: passwordOldEditController,
                    ),
                  ),
                  Container(
                    width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02,
                        right: width * 0.02,
                        top: height * 0.01),
                    child: TextFormField(
                      obscureText: _obscureTextNew,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.017),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon:
                              Icon(Icons.lock_outline, color: Colors.grey),
                          suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                if (_obscureTextNew == true) {
                                  _obscureTextNew = false;
                                } else if (_obscureTextNew == false) {
                                  _obscureTextNew = true;
                                }
                              });
                            },
                            child: Icon(
                              Icons.remove_red_eye,
                              color: Colors.grey,
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'Your password',
                          labelText: 'New Password',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          passwordNewEditController.text.length < 6
                              ? 'Password too short.'
                              : null,
                      controller: passwordNewEditController,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: width * 0.02, right: width * 0.02),
                    width: width,
                    child: FlatButton.icon(
                      icon: Icon(Icons.vpn_key),
                      label: Text(
                        "Change Password",
                        textScaleFactor: 0.8,
                      ),
                      color: Color(0xFF13A89E).withOpacity(0.2),
                      textColor: Color(0xFF13A89E),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(width * 0.04)),
                      onPressed: () async {
                        await changePassword();
                      },
                    ),
                  ),
                ],
              ),
            ),
            _changePasswordLoading
                ? Positioned(
                    top: height * 0.003,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: height * 0.002,
                        width: width,
                        child: LinearProgressIndicator(
                          backgroundColor: Color(0x8013A89E).withOpacity(0.1),
                          valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  changePassword() async {
    setState(() {
      _changePasswordLoading = true;
    });
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      Map<String, dynamic> dataPassed = {
        "oldPassword": passwordOldEditController.text,
        "newPassword": passwordNewEditController.text
      };
      var token = await ApiMain().getToken();
      var response =
          await ApiMain().postData(dataPassed, '/change-password', token);

      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        if (body['status'] == 1) {
          UserBloc().inUser.add(body);
          Navigator.pop(context);
          widget.showSuccessInSnackBar(body['message']);
        } else if (body['status'] == 0) {
          Navigator.pop(context);
          widget.showErrorInSnackBar(body['message']);
        }
        setState(() {
          _changePasswordLoading = false;
        });
      }
    } else {
      setState(() {
        _changePasswordLoading = false;
      });
      widget.showErrorInSnackBar(
          'Please fix the errors in red before submitting.');
    }
  }
}

class EditProfileDialog extends StatefulWidget {
  final userData;
  final showErrorInSnackBar;
  final showSuccessInSnackBar;
  EditProfileDialog(
      {Key key,
      @required this.showErrorInSnackBar,
      @required this.showSuccessInSnackBar,
      @required this.userData})
      : super(key: key);

  @override
  EeditProfileDialogState createState() => EeditProfileDialogState();
}

class EeditProfileDialogState extends State<EditProfileDialog> {
  TextEditingController fullNameEditController = new TextEditingController();
  TextEditingController phoneEditController = new TextEditingController();
  final formKey = GlobalKey<FormState>();

  bool _autovalidate = false;
  bool _updateUserLoading = false;
  UserBloc userBloc;

  getText() async {
    var userData = await ApiMain().getUser();
    fullNameEditController.text = userData['name'];
    phoneEditController.text = userData['phone_number'];
  }

  @override
  void initState() {
    getText();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    userBloc = BlocProvider.of<UserBloc>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.04)),
      child: Container(
        height: height * 0.45,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.08)),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Form(
              key: formKey,
              autovalidate: _autovalidate,
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: height * 0.18,
                      ),
                      Container(
                        height: height * 0.12,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.04),
                                topRight: Radius.circular(width * 0.04)),
                            color: Color(0xFF13A89E).withOpacity(0.2)),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: height * 0.06,
                          ),
                          Center(
                            child: ClipOval(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              child: Container(
                                padding: EdgeInsets.all(width * 0.02),
                                color: Colors.white.withOpacity(0.8),
                                child: Icon(
                                  Icons.account_circle,
                                  size: width * 0.2,
                                  color: Color(0xFF13A89E),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    // width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02, right: width * 0.02),
                    child: TextFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.017),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon:
                              Icon(Icons.account_circle, color: Colors.grey),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'FullName',
                          labelText: 'FullName',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          val.length == 0 ? 'FullName is required!' : null,
                      controller: fullNameEditController,
                    ),
                  ),
                  Container(
                    width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02,
                        right: width * 0.02,
                        top: height * 0.01),
                    child: TextFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.017),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon:
                              Icon(Icons.phone_iphone, color: Colors.grey),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'Your phone Number',
                          labelText: 'Phone Number',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          val.length == 0 ? 'Phone Number is required!' : null,
                      controller: phoneEditController,
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        width: width,
                        padding: EdgeInsets.only(
                            left: width * 0.02, right: width * 0.02),
                        child: FlatButton.icon(
                          icon: Icon(Icons.account_box),
                          label: Text(
                            "Update Account",
                            textScaleFactor: 0.8,
                          ),
                          color: Color(0xFF13A89E).withOpacity(0.2),
                          textColor: Color(0xFF13A89E),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(width * 0.04)),
                          onPressed: () async {
                            await updateProfile();
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            _updateUserLoading
                ? Positioned(
                    top: height * 0.003,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: height * 0.002,
                        width: width,
                        child: LinearProgressIndicator(
                          backgroundColor: Color(0x8013A89E).withOpacity(0.1),
                          valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  updateProfile() async {
    setState(() {
      _updateUserLoading = true;
    });
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      var passedData = {
        "name": fullNameEditController.text,
        "phoneNumber": phoneEditController.text
      };
      var token = await ApiMain().getToken();
      var response =
          await ApiMain().postData(passedData, '/update-profile', token);
      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        userBloc.inUser.add(body);
        Navigator.pop(context);
        widget.showSuccessInSnackBar(body['message']);
        setState(() {
          _updateUserLoading = false;
        });
        getText();
      }
    } else {
      setState(() {
        _updateUserLoading = false;
      });
      widget.showErrorInSnackBar(
          'Please fix the errors in red before submitting.');
    }
  }
}

class DeactivateAccountDialog extends StatefulWidget {
  final showSuccessInSnackBar;
  final ctx; //The ctx is required for pushing to homescreen after deactivation;
  DeactivateAccountDialog(
      {Key key, @required this.showSuccessInSnackBar, @required this.ctx})
      : super(key: key);

  @override
  _DeactivateAccountDialogState createState() =>
      _DeactivateAccountDialogState();
}

class _DeactivateAccountDialogState extends State<DeactivateAccountDialog> {
  bool _updateUserLoading = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.04)),
      child: Container(
        height: height * 0.3,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.08)),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: height * 0.18,
                    ),
                    Container(
                      height: height * 0.12,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(width * 0.04),
                              topRight: Radius.circular(width * 0.04)),
                          color: Color(0xFF13A89E).withOpacity(0.2)),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.06,
                        ),
                        Center(
                          child: ClipOval(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              padding: EdgeInsets.all(width * 0.02),
                              color: Colors.white.withOpacity(0.8),
                              child: Icon(
                                Icons.phonelink_off,
                                size: width * 0.2,
                                color: Color(0xFF13A89E),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Text(
                  "Are you sure?",
                  textScaleFactor: 0.8,
                  style: TextStyle(fontSize: width * 0.06),
                )
              ],
            ),
            Positioned(
                bottom: 0,
                left: width * 0.01,
                right: width * 0.01,
                child: Container(
                  width: width,
                  child: FlatButton.icon(
                    icon: Icon(Icons.check_circle),
                    label: Text(
                      "Yes, I want to deactivate!",
                      textScaleFactor: 0.8,
                    ),
                    color: Color(0xFF13A89E).withOpacity(0.2),
                    textColor: Color(0xFF13A89E),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.04)),
                    onPressed: () async {
                      await deactivateAcc(widget.ctx);
                    },
                  ),
                )),
            _updateUserLoading
                ? Positioned(
                    top: height * 0.003,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: height * 0.002,
                        width: width,
                        child: LinearProgressIndicator(
                          backgroundColor: Color(0x8013A89E).withOpacity(0.1),
                          valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  List newData = [];
  Map<String, dynamic> data = {};
  List<MainOrder> orders = [];
  List<Wish> wishes = [];

  getHomeData() async {
    var token = await ApiMain().getToken();
    await HomeApi().getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
    await OrderApi().getMainOrders(token).then((onValue) {
      for (var item in onValue) {
        orders.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await WishApi().getWishes().then((onValue) {
      for (var item in onValue) {
        wishes.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  deactivateAcc(ctx) async {
    setState(() {
      _updateUserLoading = true;
    });
    var token = await ApiMain().getToken();

    var response =
        await ApiMain().patchData(null, '/deactivate-account', token);

    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      Map<String, dynamic> user = await ApiMain().getUser();
      await ClientDatabaseProvider.db.destroyCart(user['id']);
      await ApiMain().removeTokenAndUser();
      await getHomeData();
      setState(() {
        _updateUserLoading = false;
      });
      Navigator.pop(context);
      widget.showSuccessInSnackBar(body['message']);
      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(ctx)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return Homescreen(
            orders: orders,
            wishes: wishes,
            authStatus: false,
            data: data,
          );
        }));
      });
    }
  }
}

class UpdateAddressDialog extends StatefulWidget {
  final userData;
  final scaffoldKey;
  final showSuccessInSnackBar;
  final showErrorInSnackBar;

  UpdateAddressDialog(
      {Key key,
      @required this.userData,
      @required this.scaffoldKey,
      @required this.showSuccessInSnackBar,
      @required this.showErrorInSnackBar})
      : super(key: key);

  @override
  _UpdateAddressDialogState createState() => _UpdateAddressDialogState();
}

class _UpdateAddressDialogState extends State<UpdateAddressDialog> {
  TextEditingController addressEditController = new TextEditingController();
  List<Map<String, Object>> address;
  UserBloc userBloc;
  final formKey = GlobalKey<FormState>();

  bool _autovalidate = false;

  bool _updateUserLoading = false;

  @override
  Widget build(BuildContext context) {
    userBloc = BlocProvider.of<UserBloc>(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.04)),
      child: Container(
        height: height * 0.4,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.08)),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: height * 0.18,
                    ),
                    Container(
                      height: height * 0.12,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(width * 0.04),
                              topRight: Radius.circular(width * 0.04)),
                          color: Color(0xFF13A89E).withOpacity(0.2)),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.06,
                        ),
                        Center(
                          child: ClipOval(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              padding: EdgeInsets.all(width * 0.02),
                              color: Colors.white.withOpacity(0.8),
                              child: Icon(
                                Icons.edit_location,
                                size: width * 0.2,
                                color: Color(0xFF13A89E),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Form(
                  key: formKey,
                  autovalidate: _autovalidate,
                  child: Container(
                    width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02,
                        right: width * 0.02,
                        top: height * 0.01),
                    child: TextFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.017),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon:
                              Icon(Icons.my_location, color: Colors.grey),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'Your Address',
                          labelText: 'Address',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          val.length == 0 ? 'Address is required!' : null,
                      controller: addressEditController,
                      onTap: () {
                        handleAutoComplete();
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: height * 0.006,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: width * 0.02, right: width * 0.02),
                      width: width,
                      child: FlatButton.icon(
                        icon: Icon(Icons.location_on),
                        label: Text(
                          "Change Address",
                          textScaleFactor: 0.8,
                        ),
                        color: Color(0xFF13A89E).withOpacity(0.2),
                        textColor: Color(0xFF13A89E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width * 0.04)),
                        onPressed: () async {
                          await updateAddress();
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
            _updateUserLoading
                ? Positioned(
                    top: height * 0.003,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: height * 0.002,
                        width: width,
                        child: LinearProgressIndicator(
                          backgroundColor: Color(0x8013A89E).withOpacity(0.1),
                          valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  updateAddress() async {
    setState(() {
      _updateUserLoading = false;
    });
    // test if address ID exists on the userData map...
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      var passedData = {
        'address': address,
        "id": widget.userData["addresses"][0]["id"]
      };
      var token = await ApiMain().getToken();
      var response =
          await ApiMain().patchData(passedData, '/update-address', token);
      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        userBloc.inUser.add(body);
        Navigator.pop(context);
        widget.showSuccessInSnackBar(body['message']);
        setState(() {
          _updateUserLoading = false;
        });
      }
    } else {
      setState(() {
        _updateUserLoading = false;
      });
      widget.showErrorInSnackBar(
          'Please fix the errors in red before submitting.');
    }
  }

  void onError(PlacesAutocompleteResponse response) {
    widget.scaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<void> handleAutoComplete() async {
    try {
      Prediction p = await PlacesAutocomplete.show(
        context: context,
        strictbounds: true,
        apiKey: kGoogleApiKey,
        onError: onError,
        mode: Mode.overlay,
        language: "en",
        location: Location(9.0371306, 38.7489053),
        radius: 10000,
      );

      PlacesDetailsResponse place =
          await placesMap.getDetailsByPlaceId(p.placeId);

      if (mounted) {
        setState(() {
          if (place.status == "OK") {
            setState(() {
              addressEditController.text =
                  place.result.name + ', ' + place.result.formattedAddress;
              setAddress(place);
            });
          }
        });
      }
    } catch (e) {
      return;
    }
  }

  setAddress(PlacesDetailsResponse place) {
    if (place != null) {
      address = [
        {
          'address': addressEditController.text,
          'latitude': place.result.geometry.location.lat,
          'longitude': place.result.geometry.location.lng
        }
      ];

      return address;
    }
    return null;
  }
}
