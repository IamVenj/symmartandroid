import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/Cart_Screen.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/productApi.dart';
import 'package:flutter/material.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/model/cart.dart';

class ItemDetails extends StatefulWidget {
  final String toolbarname;
  final String productId;
  final setting;
  final cartCount;

  const ItemDetails(
      {Key key,
      this.toolbarname,
      this.productId,
      this.cartCount,
      @required this.setting})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => IItemDetails(toolbarname, cartCount);
}

class IItemDetails extends State<ItemDetails> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List list = ['12', '11'];
  ProductApi productApi = ProductApi();
  int itemQty = 1;
  // String dropdownValue = 'Latest';
  String toolbarname;

  var cartCount;

  IItemDetails(this.toolbarname, this.cartCount);

  List newData = [];
  Map<String, dynamic> data = {};
  Color _selectedColor;
  String _selectedSize;
  bool isLoading = false;
  SharedPreferences localStorage;

  @override
  initState() {
    getCartCount();
    getData();
    super.initState();
  }

  getData() async {
    localStorage = await SharedPreferences.getInstance();
    await productApi.showProduct(widget.productId).then((onValue) {
      newData.add({"product": onValue});
      _selectedColor = Color(int.parse("0xFF" +
          "${onValue.productColors.first.color.first.hex}".substring(1, 7)));

      if (onValue.productSize.length > 0) {
        _selectedSize = onValue.productSize.first.size;
      }

      newData.forEach((f) {
        f.forEach((k, v) {
          setState(() {
            data[k] = v;
          });
        });
      });
    }, onError: (e) {
      print(e);
    }).catchError((onError) {
      print(onError);
    });
  }

  @override
  Widget build(BuildContext context) {
    var sizeCheck;
    List colorsA = [];

    if (data.length > 0) {
      sizeCheck = data['product'].productSize.length;

      if (data['product'].productColors.length > 0) {
        data['product'].productColors.forEach((f) {
          f.color.forEach((color) {
            colorsA.add(Color(int.parse("0xFF" + color.hex.substring(1, 7))));
          });
        });
      }
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final ThemeData theme = Theme.of(context);
    // final TextStyle titleStyle =
    // theme.textTheme.headline.copyWith(color: Colors.white);
    final TextStyle descriptionStyle = theme.textTheme.subhead;
    IconData _backIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }

    IconData addIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.add_circle;
        case TargetPlatform.iOS:
          return Icons.add_circle;
      }
      assert(false);
      return null;
    }

    IconData subIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.remove_circle;
        case TargetPlatform.iOS:
          return Icons.remove_circle;
      }
      assert(false);
      return null;
    }

    return new Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(_backIcon()),
              alignment: Alignment.centerLeft,
              tooltip: 'Back',
              onPressed: () {
                Navigator.pop(context, cartCount);
              },
            ),
            title: Text(toolbarname),
            backgroundColor: Colors.white,
            actions: <Widget>[
              BlocProvider(
                bloc: CartBloc(),
                child: StreamBuilder<int>(
                  stream: CartBloc().outCartCount,
                  initialData: 0,
                  builder: (context, AsyncSnapshot<int> snapshot) {
                    return Stack(
                      children: <Widget>[
                        new FlatButton.icon(
                            padding: EdgeInsets.only(left: width * 0.025),
                            textColor: Color(0xFF13A89E),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(width * 0.1),
                                    bottomLeft: Radius.circular(width * 0.1))),
                            color: Color(0x8013A89E).withOpacity(0.1),
                            icon: new Icon(
                              Icons.shopping_cart,
                            ),
                            label: Container(),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CartScreen(
                                            setting: widget.setting,
                                          )));
                            }),
                        !snapshot.hasData
                            ? new Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  new Icon(Icons.brightness_1,
                                      size: width * 0.05,
                                      color: Color(0xFF13A89E)),
                                  new Container(
                                    child: new Text(
                                      "0",
                                      textScaleFactor: 0.8,
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: width * 0.034,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              )
                            : new Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  new Icon(Icons.brightness_1,
                                      size: width * 0.06,
                                      color: Color(0xFF13A89E)),
                                  new Container(
                                    child: new Text(
                                      snapshot.data.toString(),
                                      textScaleFactor: 0.8,
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: width * 0.034,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                      ],
                    );
                  },
                ),
              ),
            ]),
        body: (data.isNotEmpty)
            ? Container(
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                Card(
                  elevation: 1.0,
                  child: Container(
                    color: Colors.white,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // photo and title
                          SizedBox(
                            height: height * 0.25,
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                new Container(
                                  child: new CarouselSlider(
                                    items: data['product']
                                        .productImage
                                        .map<Widget>((i) {
                                      return Builder(
                                        builder: (BuildContext context) {
                                          return Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                            child: CachedNetworkImage(
                                              imageUrl: i.imagePublicId,
                                              placeholder: (context, url) =>
                                                  loader(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      empty(),
                                              fit: BoxFit.contain,
                                            ),
                                          );
                                        },
                                      );
                                    }).toList(),
                                    autoPlay: true,
                                    autoPlayCurve: Curves.bounceOut,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ]),
                  ),
                ),
                Card(
                  margin: EdgeInsets.fromLTRB(
                      width * 0.03, width * 0.03, width * 0.03, width * 0.03),
                  child: Container(
                      // padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                      child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: 0,
                        top: 0,
                        bottom: 0,
                        right: width * 0.7,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(width * 0.4),
                                  topRight: Radius.circular(width * 0.4)),
                              color: Colors.grey.shade100.withOpacity(0.7)),
                        ),
                      ),
                      Positioned(
                        left: width * 0.65,
                        top: 0,
                        bottom: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(width * 0.16)),
                              color: Colors.grey.shade100.withOpacity(0.7)),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: height * 0.02,
                            left: width * 0.02,
                            right: width * 0.02,
                            bottom: height * 0.02),
                        child: DefaultTextStyle(
                            style: descriptionStyle,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: width * 0.65,
                                  child: Text(
                                    data['product'].productName,
                                    textScaleFactor: 0.8,
                                    style: descriptionStyle.copyWith(
                                        fontSize: width * 0.056,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87),
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    (data['product'].oldPrice != null)
                                        ? Text(
                                            data['product'].oldPrice + " Birr",
                                            textScaleFactor: 0.8,
                                            style: descriptionStyle.copyWith(
                                                fontSize: width * 0.04,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                color: Color(0xFF13a89e)),
                                          )
                                        : Container(),
                                    Text(
                                      data['product'].currentPrice + " Birr",
                                      textScaleFactor: 0.8,
                                      style: descriptionStyle.copyWith(
                                          fontSize: width * 0.05,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                      ),
                    ],
                  )),
                ),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: Card(
                        child: Container(
                            padding: EdgeInsets.fromLTRB(width * 0.02,
                                width * 0.04, width * 0.02, width * 0.04),
                            child: DefaultTextStyle(
                                style: descriptionStyle,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(left: 8.0),
                                          child: (sizeCheck > 0)
                                              ? Column(
                                                  children: <Widget>[
                                                    Text(
                                                      'Size',
                                                      style: TextStyle(
                                                          fontSize:
                                                              width * 0.04),
                                                      textScaleFactor: 0.8,
                                                    ),
                                                    DropdownButton<String>(
                                                      value: _selectedSize,
                                                      icon: Icon(Icons
                                                          .arrow_drop_down),
                                                      iconSize: width * 0.04,
                                                      elevation: 1,
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                      underline: Container(
                                                        height: 2,
                                                        width: width,
                                                        color:
                                                            Color(0xFF13a89e),
                                                      ),
                                                      onChanged:
                                                          (String newValue) {
                                                        setState(() {
                                                          _selectedSize =
                                                              newValue;
                                                        });
                                                      },
                                                      items: data['product']
                                                          .productSize
                                                          .map<
                                                              DropdownMenuItem<
                                                                  String>>(
                                                              (value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: value.size,
                                                          child: Text(
                                                            value.size,
                                                            textScaleFactor:
                                                                0.8,
                                                          ),
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ],
                                                )
                                              : Container(
                                                  width: width * 0.2,
                                                ),
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Text(
                                              'Colors',
                                              style: TextStyle(
                                                  fontSize: width * 0.04),
                                              textScaleFactor: 0.8,
                                            ),
                                            Container(
                                              // width: width * 0.06,
                                              decoration: BoxDecoration(
                                                color: _selectedColor,
                                                shape: BoxShape.circle,
                                              ),
                                              child: DropdownButton<Color>(
                                                iconEnabledColor: Colors.black,
                                                underline: Container(),
                                                items: colorsA
                                                    .map((color) =>
                                                        DropdownMenuItem<Color>(
                                                          child: Container(
                                                              // width: width * 0.055,
                                                              decoration: BoxDecoration(
                                                                  color: color,
                                                                  shape: BoxShape
                                                                      .circle)),
                                                          value: color,
                                                        ))
                                                    .toList(),
                                                onChanged: (Color value) {
                                                  setState(() {
                                                    _selectedColor = value;
                                                  });
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                        // three line description
                                        Column(
                                          children: <Widget>[
                                            Text(
                                              'Qty',
                                              style: TextStyle(
                                                  fontSize: width * 0.04),
                                              textScaleFactor: 0.8,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                new IconButton(
                                                  icon: Icon(
                                                    addIcon(),
                                                    color: Color(0xFF13a89e),
                                                    size: width * 0.055,
                                                  ),
                                                  onPressed: () {
                                                    setState(() {
                                                      itemQty = itemQty + 1;
                                                    });
                                                  },
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 2.0),
                                                ),
                                                Text(
                                                  itemQty.toString(),
                                                  textScaleFactor: 0.8,
                                                  style:
                                                      descriptionStyle.copyWith(
                                                          fontSize:
                                                              width * 0.055,
                                                          color:
                                                              Colors.black87),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 2.0),
                                                ),
                                                new IconButton(
                                                  icon: Icon(subIcon(),
                                                      color: Color(0xFF13a89e),
                                                      size: width * 0.055),
                                                  onPressed: () {
                                                    if (itemQty <= 1) {
                                                    } else {
                                                      setState(() {
                                                        itemQty = itemQty - 1;
                                                      });
                                                    }
                                                  },
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(top: height * 0.02),
                                      child: Container(
                                        width: width,
                                        height: height * 0.04,
                                        child: RaisedButton.icon(
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.padded,
                                          onPressed: () {
                                            (localStorage.containsKey('user') &&
                                                    localStorage
                                                        .containsKey('token'))
                                                ? addToCart(
                                                    data['product'].productName,
                                                    data['product']
                                                        .currentPrice,
                                                    int.parse(data['product']
                                                        .availability),
                                                    data['product']
                                                        .productImage
                                                        .first
                                                        .imagePublicId)
                                                : showInSnackBar(
                                                    "You are required to login!");
                                          },
                                          color: Color(0xFF13A89E),
                                          label: Text('ADD TO CART',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  letterSpacing: width * 0.007,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white,
                                                  fontSize: width * 0.035)),
                                          textColor: Colors.white,
                                          elevation: 13.0,
                                          highlightElevation: 2.0,
                                          highlightColor: Colors.black,
                                          shape: new RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                width * 0.01),
                                          ),
                                          icon: Icon(
                                            Icons.shopping_basket,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ))))),
                Divider(),
                Container(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                    child: DefaultTextStyle(
                        style: descriptionStyle,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            // three line description
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Text(
                                'Details',
                                textScaleFactor: 0.8,
                                style: descriptionStyle.copyWith(
                                    fontSize: width * 0.055,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black87),
                              ),
                            ),
                          ],
                        ))),
                Divider(),
                Container(
                    padding: EdgeInsets.fromLTRB(
                        width * 0.04, width * 0.02, width * 0.02, width * 0.04),
                    child: Container(
                      width: width,
                      child: Text(data['product'].slug,
                          textScaleFactor: 0.8,
                          style: TextStyle(
                              fontSize: width * 0.055, color: Colors.black38)),
                    )),
                Divider()
              ])))
            : loader());
  }

  addToCart(productName, productPrice, availability, productImage) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var user;
    (userJson != null) ? user = json.decode(userJson) : user = null;

    var response = await ClientDatabaseProvider.db.addToCart(
        new CartModel(
            name: productName,
            userid: user['id'],
            price: double.parse(productPrice),
            productid: int.parse(widget.productId),
            qty: itemQty,
            color: "#" + _selectedColor.toString().substring(10, 16),
            size: _selectedSize,
            img: productImage,
            associatedmodel: "App\\Product",
            packageid: null),
        productName);

    await getCartCount();

    (response == null)
        ? showErrorInSnackBar("Item is already in your cart!")
        : showSuccessInSnackBar("Successfully added to your cart");
  }

  getCartCount() async {
    var userData = await ApiMain().getUser();
    if (userData != null) {
      await ClientDatabaseProvider.db
          .getCartTotalCount(userData['id'])
          .then((v) {
        setState(() {
          cartCount = v;
        });
      });
    } else {
      setState(() {
        cartCount = 0;
      });
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  void showErrorInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.red,
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}

Widget errorW() {
  return Center(
    child: FlareActor(
      "assets/flare/no wifi.flr",
      alignment: Alignment.center,
      fit: BoxFit.contain,
      animation: "init",
    ),
  );
}

Widget empty() {
  return FlareActor(
    "assets/flare/empty.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "idle",
  );
}
