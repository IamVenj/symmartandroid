import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/Account_screen.dart';
import 'package:symmart/Cart_Screen.dart';
import 'package:symmart/Package_Screen.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/productApi.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/connection.dart';
import 'package:symmart/item_details.dart';
import 'package:symmart/item_screen.dart';
import 'package:symmart/login_screen.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/Wish.dart';
import 'package:symmart/model/category.dart';
import 'package:symmart/model/package.dart';
import 'package:symmart/orderhistory_screen.dart';
import 'package:symmart/service/searchService.dart';
import 'package:symmart/setting_screen.dart';
import 'package:symmart/signup_screen.dart';
import 'package:symmart/wish_screen.dart';
import 'package:flutter/material.dart';
import 'orderhistory_screen.dart';

class Homescreen extends StatefulWidget {
  final bool authStatus;
  final Map<String, dynamic> data;
  final List<MainOrder> orders;
  final List<Wish> wishes;
  const Homescreen(
      {Key key,
      this.authStatus,
      this.data,
      @required this.orders,
      @required this.wishes})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => new Home();
}

final GlobalKey<NavigatorState> nav = GlobalKey<NavigatorState>();

class Home extends State<Homescreen> {
  PageController _pageController;
  int _selectedIndex = 2;

  final PageStorageBucket bucket = PageStorageBucket();
  LoginScreen acd;
  AccountScreenData account;
  WishScreenData wsd;
  HomeData hd;
  OrderHistory oh;
  SettingScreen ss;
  List<Widget> pages;

  var userData, tokenData, subscription, cartCount;

  @override
  void initState() {
    getUserInfo();
    checkConnection();
    super.initState();
  }

  Future checkConnection() async {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ConnectionScreen()));
      }
    });
  }

  Future getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var tokenJson = localStorage.getString('token');
    var user;
    var token;
    (userJson != null) ? user = json.decode(userJson) : user = null;
    (tokenJson != null) ? token = tokenJson : token = null;
    (user != null)
        ? setState(() {
            userData = user;
          })
        : userData = null;
    (token != null)
        ? setState(() {
            tokenData = token;
          })
        : tokenData = null;
  }

  @override
  void didChangeDependencies() {
    _pageController =
        PageController(initialPage: _selectedIndex, keepPage: true);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (userData == null) {
      acd = LoginScreen(
        key: PageStorageKey('acd'),
      );
    } else {
      account = AccountScreenData(
        key: PageStorageKey('account'),
        tokenData: tokenData,
      );
    }
    wsd = WishScreenData(
      toolBarName: 'Wishlist',
      key: PageStorageKey('wsd'),
      wishes: widget.wishes,
      setting: widget.data['setting'],
    );
    hd = HomeData(
        authState: widget.authStatus,
        data: widget.data,
        cartCount: cartCount,
        wishes: widget.wishes,
        userData: userData,
        setting: widget.data['setting']);
    oh = OrderHistory(
      toolbarname: 'Order History',
      key: PageStorageKey('oh'),
      orders: widget.orders,
    );
    ss = SettingScreen(
      toolbarname: 'Information',
      key: PageStorageKey('ss'),
      setting: widget.data['setting'],
    );

    if (userData != null) {
      pages = [account, wsd, hd, oh, ss];
    } else {
      pages = [acd, wsd, hd, oh, ss];
    }

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    List<Icon> bottomNavigationBarItems = List.of([
      Icon(
        Icons.account_circle,
        size: width * 0.06,
      ),
      Icon(Icons.favorite, size: width * 0.06),
      Icon(Icons.apps, size: width * 0.06),
      Icon(Icons.history, size: width * 0.06),
      Icon(Icons.info, size: width * 0.06),
    ]);
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text("Symmart", style: TextStyle(fontFamily: 'ProductSans')),
        actions: <Widget>[
          IconButton(
            tooltip: 'Search',
            icon: const Icon(Icons.search),
            onPressed: () async {
              var selected = await showSearch<Product>(
                context: context,
                delegate: SearchService(widget.data['category']),
              );
              if (selected != null) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ItemDetails(
                              productId: selected.id,
                              toolbarname: selected.productName,
                              cartCount: cartCount,
                              setting: widget.data['setting'],
                            )));
              }
            },
          ),
          BlocProvider(
            bloc: CartBloc(),
            child: StreamBuilder<int>(
              stream: CartBloc().outCartCount,
              initialData: 0,
              builder: (context, AsyncSnapshot<int> snapshot) {
                return Stack(
                  children: <Widget>[
                    new FlatButton.icon(
                        padding: EdgeInsets.only(left: width * 0.025),
                        textColor: Color(0xFF13A89E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.1),
                                bottomLeft: Radius.circular(width * 0.1))),
                        color: Color(0x8013A89E).withOpacity(0.1),
                        icon: new Icon(
                          Icons.shopping_cart,
                        ),
                        label: Container(),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CartScreen(
                                        setting: widget.data['setting'],
                                      )));
                        }),
                    !snapshot.hasData
                        ? new Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: width * 0.05, color: Color(0xFF13A89E)),
                              new Container(
                                child: new Text(
                                  "0",
                                  textScaleFactor: 0.8,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.034,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        : new Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: width * 0.06, color: Color(0xFF13A89E)),
                              new Container(
                                child: new Text(
                                  snapshot.data.toString(),
                                  textScaleFactor: 0.8,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.034,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: MediaQuery.of(context).size.height * 0.07,
        child: CurvedNavigationBar(
          index: _selectedIndex,
          onTap: (index) => setState(() {
            _selectedIndex = index;
            _pageController.animateToPage(index,
                duration: Duration(milliseconds: 100),
                curve: Curves.decelerate);
          }),
          height: height * 0.07,
          backgroundColor: Colors.transparent,
          animationCurve: Curves.easeIn,
          items: bottomNavigationBarItems,
        ),
      ),
      drawer: Container(
        width: width * 0.75,
        child: new Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(),
              new Card(
                elevation: 0.0,
                child: (widget.data.length > 0)
                    ? (widget.data['drawCategory'].length == 0)
                        ? Padding(
                            padding: EdgeInsets.only(
                                top: height * 0.005, bottom: width * 0.01),
                            child: Center(child: Text('No Categories Found!')),
                          )
                        : SingleChildScrollView(
                            child: Container(
                            height: height,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: AlwaysScrollableScrollPhysics(),
                              itemCount: widget.data['drawCategory'].length,
                              itemBuilder: (BuildContext context, int index) {
                                return ExpansionTile(
                                  key: new PageStorageKey(
                                      widget.data['drawCategory'][index].id),
                                  title: Text(
                                      widget.data['drawCategory'][index]
                                          .categoryName,
                                      textScaleFactor: 0.8,
                                      style:
                                          TextStyle(fontSize: width * 0.045)),
                                  children: <Widget>[
                                    Column(
                                      children: widget
                                          .data['drawCategory'][index].parent
                                          .map<Widget>((f) {
                                        if (f != null) {
                                          return ListTile(
                                            title: Text(
                                              f.categoryName,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle,
                                            ),
                                            onTap: () async {
                                              Navigator.of(context).pop();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ItemScreen(
                                                            toolbarname:
                                                                f.categoryName,
                                                            categoryId: f.id,
                                                            userData: userData,
                                                            setting:
                                                                widget.data[
                                                                    'setting'],
                                                            wishes:
                                                                widget.wishes,
                                                            category:
                                                                widget.data[
                                                                    'category'],
                                                          )));
                                            },
                                          );
                                        } else {
                                          return ListTile();
                                        }
                                      }).toList(),
                                    )
                                  ],
                                );
                              },
                            ),
                          ))
                    : Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 1.0,
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
      body: PageStorage(
        child: PageView(
            controller: _pageController,
            onPageChanged: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            children: pages),
        bucket: bucket,
      ),
    );
  }

  Icon keyloch = new Icon(
    Icons.arrow_forward,
    color: Colors.black26,
  );
}

class HomeData extends StatefulWidget {
  final bool authState;
  final Map<String, dynamic> data;
  final cartCount;
  final List<Wish> wishes;
  final userData;
  final setting;
  const HomeData(
      {Key key,
      this.authState,
      this.data,
      @required this.cartCount,
      @required this.wishes,
      @required this.userData,
      @required this.setting})
      : super(key: key);
  @override
  _HomeDataState createState() => _HomeDataState(authState, data);
}

class _HomeDataState extends State<HomeData> {
  ProductApi productApi = new ProductApi();
  HomeApi homeApi = new HomeApi();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final bool authState;
  final Map<String, dynamic> data;

  _HomeDataState(this.authState, this.data);
  String name = 'My Wishlist';

  @override
  void initState() {
    super.initState();
    getAuthStatus();
  }

  getAuthStatus() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    String token = localStorage.getString('token');

    if (authState == null) {
      if (localStorage.containsKey('user') &&
          localStorage.containsKey('token')) {
        homeApi.checkAuth(token).then((onValue) {
          if (!onValue[0]['authStatus']) {
            setState(() {
              localStorage.remove('user');
              localStorage.remove('token');
            });
          }
        });
      }
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    if (data.length > 0) {
      return Scaffold(
        key: _scaffoldKey,
        body: new SingleChildScrollView(
          child: Container(
            child: new Column(children: <Widget>[
              Card(
                elevation: 0.0,
                child: Container(
                  color: Colors.white,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.15,
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Positioned(
                                left: 0,
                                top: 0,
                                bottom: 0,
                                right: width * 0.7,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomRight:
                                              Radius.circular(width * 0.4),
                                          topRight:
                                              Radius.circular(width * 0.4)),
                                      color: Colors.grey.shade100
                                          .withOpacity(0.7)),
                                ),
                              ),
                              Positioned(
                                left: width * 0.7,
                                top: 0,
                                bottom: 0,
                                right: 0,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          bottomLeft:
                                              Radius.circular(width * 0.4),
                                          topLeft:
                                              Radius.circular(width * 0.4)),
                                      color: Colors.grey.shade100
                                          .withOpacity(0.7)),
                                ),
                              ),
                              new Container(
                                  width: width,
                                  child: Carousel(
                                    snapshotData: data['carousel'],
                                    userData: widget.userData,
                                    setting: widget.setting,
                                    wishes: widget.wishes,
                                    category: widget.data['category'],
                                  ))
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Card(
                  elevation: 4,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: 0,
                        top: 0,
                        bottom: 0,
                        right: width * 0.3,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(width * 0.4),
                                  topRight: Radius.circular(width * 0.4)),
                              color: Color(0x1013A89E).withOpacity(0.06)),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: height * 0.017, bottom: height * 0.017),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: new Text(
                              'LATEST PRODUCTS',
                              style: TextStyle(
                                  letterSpacing: width * 0.006,
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w900),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.36,
                child: (data['latestProduct'].length > 0)
                    ? MlatestProduct(
                        dlatestProduct: data['latestProduct'],
                        itemDetail: widget.cartCount,
                        wishes: widget.wishes,
                        setting: widget.setting,
                        showInSnackBar: showInSnackBar,
                        showSuccessInSnackBar: showSuccessInSnackBar)
                    : empty(),
              ),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Card(
                  elevation: 4,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: width * 0.3,
                        top: 0,
                        bottom: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(width * 0.4),
                                  topLeft: Radius.circular(width * 0.4)),
                              color: Color(0x1013A89E).withOpacity(0.06)),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: height * 0.017, bottom: height * 0.017),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: new Text(
                              'POPULAR CATEGORIES',
                              style: TextStyle(
                                  letterSpacing: width * 0.006,
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w900),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.238,
                  child: (data['category'].length > 0)
                      ? MPopCategories(
                          categories: data['category'],
                          userData: widget.userData,
                          setting: widget.setting,
                          wishes: widget.wishes,
                        )
                      : empty()),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Card(
                  elevation: 4,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: 0,
                        top: 0,
                        bottom: 0,
                        right: width * 0.3,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(width * 0.4),
                                  topRight: Radius.circular(width * 0.4)),
                              color: Color(0x1013A89E).withOpacity(0.06)),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: height * 0.017, bottom: height * 0.017),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: new Text(
                              'BEST SELLING PRODUCTS',
                              style: TextStyle(
                                  letterSpacing: width * 0.006,
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w900),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.36,
                  child: (data['bestSold'].length > 0)
                      ? MlatestProduct(
                          dlatestProduct: data['bestSold'],
                          itemDetail: widget.cartCount,
                          wishes: widget.wishes,
                          setting: widget.setting,
                          showInSnackBar: showInSnackBar,
                          showSuccessInSnackBar: showSuccessInSnackBar)
                      : empty()),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Card(
                  elevation: 4,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: width * 0.3,
                        top: 0,
                        bottom: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(width * 0.4),
                                  topLeft: Radius.circular(width * 0.4)),
                              color: Color(0x1013A89E).withOpacity(0.06)),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: height * 0.017, bottom: height * 0.017),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: new Text(
                              'RECOMMENDED OFFERS',
                              style: TextStyle(
                                  letterSpacing: width * 0.006,
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w900),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.36,
                  child: (data['recommendedOffers'].length > 0)
                      ? MlatestProduct(
                          dlatestProduct: data['recommendedOffers'],
                          itemDetail: widget.cartCount,
                          wishes: widget.wishes,
                          setting: widget.setting,
                          showInSnackBar: showInSnackBar,
                          showSuccessInSnackBar: showSuccessInSnackBar)
                      : empty()),
              Padding(
                padding: EdgeInsets.all(width * 0.02),
                child: Card(
                  elevation: 4,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: 0,
                        top: 0,
                        bottom: 0,
                        right: width * 0.3,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(width * 0.4),
                                  topRight: Radius.circular(width * 0.4)),
                              color: Color(0x1013A89E).withOpacity(0.06)),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              top: height * 0.017, bottom: height * 0.017),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: EdgeInsets.only(left: width * 0.02),
                            child: new Text(
                              'PACKAGE OFFERS',
                              style: TextStyle(
                                  letterSpacing: width * 0.006,
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w900),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                  height: MediaQuery.of(context).size.height * 0.36,
                  child: (data['packages'].length > 0)
                      ? PackageW(
                          package: data['packages'],
                          setting: widget.setting,
                        )
                      : empty()),
            ]),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: loader(),
        ),
      );
    }
  }
}

class Carousel extends StatelessWidget {
  final userData;
  final setting;
  final wishes;
  final category;
  const Carousel(
      {Key key,
      @required this.snapshotData,
      @required this.userData,
      @required this.setting,
      @required this.wishes,
      @required this.category})
      : super(key: key);
  final List<dynamic> snapshotData;

  @override
  Widget build(BuildContext context) {
    if (snapshotData != null) {
      return CarouselSlider(
        items: snapshotData.map<Widget>((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  color: Colors.white10,
                  child: GestureDetector(
                      child: CachedNetworkImage(
                        imageUrl: i.imagePublicId,
                        placeholder: (context, url) => loader(),
                        errorWidget: (context, url, error) => empty(),
                        fit: BoxFit.contain,
                      ),
                      onTap: () async {
                        Navigator.push<Widget>(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ItemScreen(
                              categoryId: i.category.id,
                              toolbarname: i.category.categoryName,
                              userData: userData,
                              setting: setting,
                              wishes: wishes,
                              category: category,
                            ),
                          ),
                        );
                      }));
            },
          );
        }).toList(),
        autoPlay: true,
        enlargeCenterPage: true,
        autoPlayCurve: Curves.bounceOut,
        scrollPhysics: BouncingScrollPhysics(),
      );
    } else {
      return Container();
    }
  }
}

Widget errorW() {
  return Center(
    child: FlareActor(
      "assets/flare/no wifi.flr",
      alignment: Alignment.bottomCenter,
      fit: BoxFit.contain,
      animation: "init",
    ),
  );
}

class DataStorage {
  static List<dynamic> newDataType = [];
}

class MPopCategories extends StatelessWidget {
  const MPopCategories(
      {Key key,
      this.categories,
      this.shapeBorder,
      @required this.userData,
      @required this.setting,
      @required this.wishes})
      : super(key: key);

  final List<CategoryMain> categories;
  final ShapeBorder shapeBorder;
  final userData;
  final setting;
  final wishes;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return ListView.builder(
      itemCount: categories.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ItemScreen(
                          toolbarname: categories[index].categoryName,
                          categoryId: categories[index].id,
                          userData: userData,
                          setting: setting,
                          wishes: wishes,
                          category: categories,
                        )));
          },
          child: new Container(
            margin: EdgeInsets.all(5.0),
            width: width * 0.465,
            height: height,
            child: new Card(
              shape: shapeBorder,
              elevation: 2.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        Positioned.fill(
                            child: CachedNetworkImage(
                          imageUrl: categories[index].imageUrl,
                          placeholder: (context, url) => loader(),
                          errorWidget: (context, url, error) => errorW(),
                          fit: BoxFit.cover,
                        )),
                        Container(
                          color: Colors.black38,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 3.0, bottom: 3.0),
                          alignment: Alignment.bottomLeft,
                          child: new GestureDetector(
                            onTap: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ItemScreen(
                                            toolbarname:
                                                categories[index].categoryName,
                                            categoryId: categories[index].id,
                                            userData: userData,
                                            setting: setting,
                                            wishes: wishes,
                                            category: categories,
                                          )));
                            },
                            child: new Text(
                              categories[index].categoryName,
                              style: TextStyle(
                                  fontSize: width * 0.04,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // ),
          ),
        );
      },
    );
  }
}

class DrawerHeader extends StatelessWidget {
  const DrawerHeader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return StreamBuilder<Map<String, dynamic>>(
        stream: UserBloc().outUser,
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return UserAccountsDrawerHeader(
              currentAccountPicture: ClipOval(
                clipBehavior: Clip.antiAlias,
                child: CachedNetworkImage(
                  imageUrl: (snapshot.data['photo_url'].length > 0)
                      ? '${snapshot.data['photo_url']}'
                      : 'https://symmart.com/public/images/default-avatar.png',
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                  placeholder: (context, url) => loader(),
                  errorWidget: (context, url, error) => empty(),
                ),
              ),
              accountName: new Text('${snapshot.data['name']}',
                  style: Theme.of(context).textTheme.title),
              accountEmail: new Text('${snapshot.data['email']}',
                  style: Theme.of(context).textTheme.subhead),
              decoration: new BoxDecoration(
                  gradient: LinearGradient(colors: [
                    Color(0xFF13A89E),
                    Color(0xFF13A89E).withOpacity(0.2)
                  ]),
                  // backgroundBlendMode: BlendMode.hardLight,
                  image: DecorationImage(
                      image: AssetImage('images/home.jpg'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.9),
                          BlendMode.colorDodge))),
            );
          } else {
            return Container(
              height: height * 0.2,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Positioned.fill(
                    child: Image.asset(
                      'images/home.jpg',
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  Container(
                    foregroundDecoration:
                        BoxDecoration(color: Colors.black.withOpacity(0.86)),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Welcome To Symmart',
                        textScaleFactor: 0.8,
                        style: TextStyle(
                            color: Colors.white, fontSize: width * 0.075),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: height * 0.02),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'New to symmart?',
                              style: TextStyle(
                                  color: Colors.white, fontSize: width * 0.045),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: width * 0.01),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SignupScreen()));
                                  },
                                  child: Text(
                                    'Signup',
                                    style: TextStyle(
                                        color: Color(0xFF13A89E),
                                        fontSize: width * 0.045),
                                  )),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
        });
  }
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}

Widget empty() {
  return FlareActor(
    "assets/flare/empty.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "idle",
  );
}

class MlatestProduct extends StatefulWidget {
  final List<Product> dlatestProduct;
  final ShapeBorder shape;
  final itemDetail;
  final List<Wish> wishes;
  final setting;
  final void Function(String value) showInSnackBar;
  final void Function(String value) showSuccessInSnackBar;

  MlatestProduct(
      {Key key,
      @required this.dlatestProduct,
      this.shape,
      @required this.itemDetail,
      @required this.wishes,
      @required this.setting,
      @required this.showInSnackBar,
      @required this.showSuccessInSnackBar})
      : super(key: key);

  @override
  _MlatestProductState createState() =>
      _MlatestProductState(dlatestProduct, shape, itemDetail, wishes);
}

class _MlatestProductState extends State<MlatestProduct> {
  _MlatestProductState(
      this.dlatestProduct, this.shape, this.itemDetail, this.wishes);

  List<Product> dlatestProduct;
  ShapeBorder shape;
  var itemDetail;
  List<Wish> wishes;

  List fav = [];

  @override
  void initState() {
    favorites(null);
    super.initState();
  }

  addWish(productId, List<Wish> wishes, index) async {
    var token = await ApiMain().getToken();
    if (token != null) {
      widget.showInSnackBar("Please wait for a moment..");
      setState(() {
        fav[index] = true;
      });

      var passedData = {"product_id": productId};
      var response = await ApiMain().postData(passedData, '/add-wish', token);

      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        Wish wish = Wish.fromJson(body['wish']);
        wishes.add(wish);
      }
      widget.showSuccessInSnackBar("Successfully added to your wishlist");
      await favorites(null);
    } else {
      widget.showInSnackBar("your are required to Login to add wishes!");
    }
  }

  removeWish(productId, index) async {
    widget.showInSnackBar("Please wait for a moment..");
    setState(() {
      fav[index] = false;
    });
    var token = await ApiMain().getToken();
    var response = await ApiMain().deleteData(productId, '/wish/remove', token);

    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      if (body["status"] == 1) {
        wishes.removeWhere((test) {
          return int.parse(test.product.id) == int.parse(productId);
        });
        await favorites(productId);
      }
      widget.showSuccessInSnackBar("successfully removed from your wish list");
    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    final ThemeData theme = Theme.of(context);
    final TextStyle descriptionStyle = theme.textTheme.subhead;

    return (dlatestProduct != null)
        ? ListView.builder(
            itemCount: dlatestProduct.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (BuildContext context, int index) {
              return SafeArea(
                  top: true,
                  bottom: false,
                  child: Container(
                      padding: const EdgeInsets.all(4.0),
                      height: height,
                      width: width * 0.625,
                      child: GestureDetector(
                        onTap: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ItemDetails(
                                        productId: dlatestProduct[index].id,
                                        toolbarname:
                                            dlatestProduct[index].productName,
                                        cartCount: itemDetail,
                                        setting: widget.setting,
                                      )));
                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(width * 0.05),
                                  bottomRight: Radius.circular(width * 0.05),
                                  bottomLeft: Radius.circular(width * 0.05))),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          borderOnForeground: true,
                          semanticContainer: true,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: height * 0.17,
                                child: Stack(
                                  children: <Widget>[
                                    Positioned.fill(
                                        child: CachedNetworkImage(
                                      imageUrl: dlatestProduct[index]
                                          .productImage
                                          .first
                                          .imagePublicId,
                                      placeholder: (context, url) => loader(),
                                      errorWidget: (context, url, error) =>
                                          empty(),
                                      fit: BoxFit.cover,
                                    )),
                                    (dlatestProduct[index].oldPrice != null)
                                        ? Container(
                                            alignment: Alignment.topRight,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Color(0xFF13A89E),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  width *
                                                                      0.04))),
                                              padding:
                                                  EdgeInsets.all(width * 0.02),
                                              child: Text(
                                                getPercentage(
                                                    dlatestProduct[index]
                                                        .currentPrice,
                                                    dlatestProduct[index]
                                                        .oldPrice),
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: width * 0.05,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    (wishes.isNotEmpty)
                                        ? Stack(
                                            children: <Widget>[
                                              !fav[index]
                                                  ? Positioned(
                                                      top: 0,
                                                      left: 0,
                                                      child: FlatButton.icon(
                                                        icon: Icon(
                                                          Icons.favorite_border,
                                                          size: width * 0.06,
                                                          color: Colors.white,
                                                        ),
                                                        onPressed: () async {
                                                          await addWish(
                                                              dlatestProduct[
                                                                      index]
                                                                  .id,
                                                              wishes,
                                                              index);
                                                        },
                                                        label: Container(),
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                          topRight:
                                                              Radius.circular(
                                                                  width),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  width),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  width),
                                                        )),
                                                        color:
                                                            Color(0xFF13A89E),
                                                      ),
                                                    )
                                                  : Positioned(
                                                      top: 0,
                                                      left: 0,
                                                      child: FlatButton.icon(
                                                        icon: Icon(
                                                          Icons.favorite,
                                                          size: width * 0.06,
                                                          color: Colors.white,
                                                        ),
                                                        onPressed: () async {
                                                          await removeWish(
                                                              dlatestProduct[
                                                                      index]
                                                                  .id,
                                                              index);
                                                        },
                                                        label: Container(),
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                          topRight:
                                                              Radius.circular(
                                                                  width),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  width),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  width),
                                                        )),
                                                        color:
                                                            Color(0xFF13A89E),
                                                        // tooltip: 'Add to wish',
                                                      ),
                                                    )
                                            ],
                                          )
                                        : !fav[index]
                                            ? Positioned(
                                                top: 0,
                                                left: 0,
                                                child: FlatButton.icon(
                                                  icon: Icon(
                                                    Icons.favorite_border,
                                                    size: width * 0.06,
                                                    color: Colors.white,
                                                  ),
                                                  onPressed: () async {
                                                    await addWish(
                                                        dlatestProduct[index]
                                                            .id,
                                                        wishes,
                                                        index);
                                                  },
                                                  label: Container(),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(width),
                                                    bottomLeft:
                                                        Radius.circular(width),
                                                    bottomRight:
                                                        Radius.circular(width),
                                                  )),
                                                  color: Color(0xFF13A89E),
                                                  // tooltip: 'Add to wish',
                                                ),
                                              )
                                            : Positioned(
                                                top: 0,
                                                left: 0,
                                                child: FlatButton.icon(
                                                  icon: Icon(
                                                    Icons.favorite,
                                                    size: width * 0.06,
                                                    color: Colors.white,
                                                  ),
                                                  onPressed: () async {
                                                    await removeWish(
                                                        dlatestProduct[index]
                                                            .id,
                                                        index);
                                                  },
                                                  label: Container(),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                    topRight:
                                                        Radius.circular(width),
                                                    bottomLeft:
                                                        Radius.circular(width),
                                                    bottomRight:
                                                        Radius.circular(width),
                                                  )),
                                                  color: Color(0xFF13A89E),
                                                  // tooltip: 'Add to wish',
                                                ),
                                              )
                                  ],
                                ),
                              ),
                              Divider(),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.fromLTRB(
                                      14.0, 10.0, 10.0, 0.0),
                                  child: DefaultTextStyle(
                                    style: descriptionStyle,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 4.0, right: 8.0),
                                              child: Text(
                                                dlatestProduct[index]
                                                        .currentPrice +
                                                    ' Birr',
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: height * 0.024,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                            (dlatestProduct[index].oldPrice !=
                                                    null)
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            bottom: 6.0),
                                                    child: Text(
                                                      dlatestProduct[index]
                                                              .oldPrice +
                                                          ' Birr',
                                                      textScaleFactor: 0.8,
                                                      style: descriptionStyle
                                                          .copyWith(
                                                              color: Color(
                                                                  0xFF13A89E),
                                                              fontSize: height *
                                                                  0.019,
                                                              decoration:
                                                                  TextDecoration
                                                                      .lineThrough),
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: height * 0.007),
                                          child: Text(
                                            (dlatestProduct[
                                                            index]
                                                        .productName
                                                        .length >
                                                    40)
                                                ? dlatestProduct[index]
                                                        .productName
                                                        .substring(0, 40) +
                                                    ' ...'
                                                : dlatestProduct[index]
                                                    .productName,
                                            textScaleFactor: 0.8,
                                            style: descriptionStyle.copyWith(
                                                color: Colors.black87,
                                                fontSize: height * 0.02,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ),
                                        Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 8.0, right: 1.0),
                                              child: Text(
                                                'Sold by: ',
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: height * 0.02),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 8.0),
                                              child: Text(
                                                (dlatestProduct[index]
                                                            .user
                                                            .shopName
                                                            .length >
                                                        15)
                                                    ? dlatestProduct[index]
                                                            .user
                                                            .shopName
                                                            .substring(0, 15) +
                                                        '...'
                                                    : dlatestProduct[index]
                                                        .user
                                                        .shopName,
                                                textScaleFactor: 0.8,
                                                style:
                                                    descriptionStyle.copyWith(
                                                        color: Colors.black87,
                                                        fontSize: width * 0.045,
                                                        fontWeight:
                                                            FontWeight.w700),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              // Padding(
                              //   padding: EdgeInsets.only(
                              //       left: width * 0.026, bottom: width * 0.02),
                              //   child: Container(
                              //     child: Column(
                              //       children: <Widget>[
                              //         RaisedButton(
                              //           child: Icon(
                              //             Icons.details,
                              //             color: Colors.black45,
                              //             size: width * 0.04,
                              //           ),
                              //           shape: RoundedRectangleBorder(
                              //               borderRadius:
                              //                   BorderRadius.circular(200)),
                              //           splashColor: Color(0x8013A89E),
                              //           focusColor: Colors.white,
                              //           hoverColor: Colors.white,
                              //           color: Colors.white,
                              //           onPressed: () {
                              //             Navigator.push(
                              //                 context,
                              //                 MaterialPageRoute(
                              //                     builder: (context) =>
                              //                         ItemDetails(
                              //                           productId:
                              //                               dlatestProduct[
                              //                                       index]
                              //                                   .id,
                              //                           toolbarname:
                              //                               dlatestProduct[
                              //                                       index]
                              //                                   .productName,
                              //                           cartCount: itemDetail,
                              //                           setting: widget.setting,
                              //                         )));
                              //           },
                              //         )
                              //       ],
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      )));
            },
          )
        : new Container();
  }

  favorites(wishId) async {
    List d = [];
    List w = [];
    fav.clear();

    for (var prod in dlatestProduct) {
      d.add(prod.id);
    }

    if (wishes.isNotEmpty) {
      if (wishes.first.responseCode != "401") {
        for (var item in wishes) {
          w.add(item.product.id);
        }
      }
    }

    if (wishId != null) {
      w.remove(wishId);
    }

    d.forEach((f) {
      if (w.isNotEmpty) {
        w.contains(f) ? fav.add(true) : fav.add(false);
      } else {
        fav.add(false);
      }
    });

    return fav;
  }
}

class PackageW extends StatelessWidget {
  const PackageW(
      {Key key, @required this.package, this.shape, @required this.setting})
      : super(key: key);

  final List<Package> package;
  final ShapeBorder shape;
  final setting;

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    final ThemeData theme = Theme.of(context);
    final TextStyle descriptionStyle = theme.textTheme.subhead;

    return ListView.builder(
      itemCount: package.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return SafeArea(
            top: true,
            bottom: false,
            child: Container(
                padding: const EdgeInsets.all(4.0),
                height: height,
                width: width * 0.625,
                child: GestureDetector(
                  onTap: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PackageScreen(
                                  packageId: package[index].id,
                                  toolbarname: package[index].packageName,
                                  setting: setting,
                                )));
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(width * 0.05),
                            bottomRight: Radius.circular(width * 0.05),
                            bottomLeft: Radius.circular(width * 0.05))),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    borderOnForeground: true,
                    semanticContainer: true,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.17,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                  child: CachedNetworkImage(
                                imageUrl:
                                    package[index].images.first.imagePublicId,
                                placeholder: (context, url) => loader(),
                                errorWidget: (context, url, error) => empty(),
                                fit: BoxFit.cover,
                              )),
                              Container(
                                alignment: Alignment.topRight,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xFF13A89E),
                                      borderRadius: BorderRadius.only(
                                          bottomLeft:
                                              Radius.circular(width * 0.04))),
                                  padding: EdgeInsets.all(width * 0.02),
                                  child: Text(
                                    package[index].discountPercent + '%',
                                    textScaleFactor: 0.8,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: width * 0.05,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.fromLTRB(
                                14.0, 10.0, 10.0, 0.0),
                            child: DefaultTextStyle(
                              style: descriptionStyle,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 4.0, right: 8.0),
                                        child: Text(
                                          package[index]
                                                  .discountPrice
                                                  .toString() +
                                              ' Birr',
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: height * 0.024,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      package[index].packageName,
                                      textScaleFactor: 0.8,
                                      style: descriptionStyle.copyWith(
                                          color: Colors.black87,
                                          fontSize: height * 0.02,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 8.0, right: 1.0),
                                        child: Text(
                                          'Sold by: ',
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: height * 0.02),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          package[index].user.shopName,
                                          textScaleFactor: 0.8,
                                          style: descriptionStyle.copyWith(
                                              color: Colors.black87,
                                              fontSize: height * 0.02,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )));
      },
    );
  }
}

String getPercentage(String currentPrice, String oldPrice) {
  double percentage;
  if (oldPrice != null) {
    percentage = ((int.parse(currentPrice) / int.parse(oldPrice)) * 100) - 100;
  } else {
    percentage = 0;
  }

  return percentage.toStringAsFixed(1) + '%';
}
