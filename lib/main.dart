import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'dart:async';

import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Wish.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.

        primaryColor: Colors.white,
        primaryColorDark: Colors.white30,
        accentColor: Colors.blue,
        fontFamily: 'ProductSans',
      ),
      home: new MyHomePage(title: 'Symmart'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  HomeApi homeApi = new HomeApi();
  List<dynamic> newData = [];
  Map<String, dynamic> data = {};
  List<MainOrder> orders = [];
  List<Wish> wishes = [];
  StreamSubscription subscription;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  getData() async {
    var token = await ApiMain().getToken();
    await homeApi.getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
    await OrderApi().getMainOrders(token).then((onValue) {
      for (var item in onValue) {
        orders.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await WishApi().getWishes().then((onValue) {
      for (var item in onValue) {
        wishes.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await startTime();
  }

  setConnectionStatus() {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result == ConnectivityResult.none) {
        showInSnackBar("Internet Connection is required!", 10);
      } else {
        showInSnackBar("Connected!", null);
        await getData();
      }
    });
  }

  navigate(bool authStatus) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Homescreen(
                authStatus: authStatus,
                data: data,
                orders: orders,
                wishes: wishes)));
  }

  startTime() async {
    if (newData.length > 0) {
      var _duration;
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      String token = localStorage.getString('token');
      _duration = new Duration(seconds: 0);

      if (localStorage.containsKey('user') &&
          localStorage.containsKey('token')) {
        await homeApi.checkAuth(token).then((onValue) {
          if (!onValue[0]['authStatus']) {
            setState(() {
              localStorage.remove('user');
              localStorage.remove('token');
            });
          }
          if (onValue.length > 0) {
            _duration = new Duration(seconds: 0);
            return new Future.delayed(
                _duration, () => navigate(onValue[0]['authStatus']));
          } else {
            return new Future.delayed(_duration, () => navigate(null));
          }
        }).catchError((onError) {
          return new Future.delayed(_duration, () => navigate(null));
        });
      } else {
        return new Future.delayed(_duration, () => navigate(null));
      }
    }
  }

  void showInSnackBar(String value, duration) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    (duration != null)
        ? scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(value, style: TextStyle(
              fontSize: width * 0.04
            )),
            duration: Duration(seconds: duration),
            backgroundColor: Color(0xFF13A89E),
            elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(
                        width * 0.05),
                    topLeft: Radius.circular(
                        width * 0.05))),
          ))
        : scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(value, style: TextStyle(
              fontSize: width * 0.04
            ),),
            backgroundColor: Color(0xFF13A89E),
            elevation: 10.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(
                        width * 0.05),
                    topLeft: Radius.circular(
                        width * 0.05))),
          ));
  }

  @override
  void initState() {
    setConnectionStatus();
    super.initState();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: scaffoldKey,
      body: new Container(
        alignment: Alignment.center,
        decoration: new BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              // color: Colors.black12,
              margin: new EdgeInsets.all(width * 0.09),
              width: MediaQuery.of(context).size.width,
              height: height * 0.15,
              child: new Image.asset(
                'images/logo.png',
              ),
            ),
            Container(width: width * 0.2, height: height * 0.1, child: loader())
          ],
        ),
      ),
    );
  }
}
