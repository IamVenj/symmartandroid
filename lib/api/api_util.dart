/*
 * This class contains api endpoints; 
 */
class ApiUtil {
  /*
   * MAIN_API_URL is the main url'
   */
  // static const String MAIN_API_URL = 'http://192.168.43.28:8000/api';
  static const String MAIN_API_URL = 'https://symmart.com/api';

  /*
   * These are URL that connect to MAIN_API_URL '
   */
  static const String HOMEPAGE = '/home';
  static const String PARENT_CATEGORIES = '/parent-category';
  static const String MY_USER_ACCOUNT = '/user/account';
  static const String MY_WISHES = '/user/wish';
  static const String MAINORDER = '/user/order';
  static const String AUTHCHECK = '/auth/check';
  static const String ALLCOLOR = '/colors';
  static const String PRODUCTS = '/products';
  static const String SETTING = '/setting';
  static const String SEARCH = '/search';
  /*
   * These are api endpoints that require an argument;
   */
  static String currentProduct(String productId) {
    return MAIN_API_URL + '/product/detail/' + productId;
  }

  static String currentPackage(String packageId) {
    return MAIN_API_URL + '/package/detail/' + packageId;
  }

  static String userAccount(String userId) {
    return MAIN_API_URL + MY_USER_ACCOUNT + userId;
  }

  static String order(String orderId) {
    return MAIN_API_URL + MAINORDER + orderId;
  }
}
