import 'dart:convert';

import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/api_util.dart';
import 'package:symmart/model/Wish.dart';
import 'package:http/http.dart' as http;

class WishApi {
  Future<List<Wish>> getWishes() async {
    String myWishes = ApiUtil.MAIN_API_URL + ApiUtil.MY_WISHES;
    Map<String, String> header;

    var token = await ApiMain().getToken();

    if (token != null) {
      header = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      };
    } else {
      header = {'Accept': 'application/json'};
    }

    var response = await http.get(myWishes, headers: header);

    List<Wish> myWish = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      for (var item in body['wish']) {
        Wish wish = Wish.fromJson(item);
        myWish.add(wish);
      }
    }

    if (response.statusCode == 401) {
      var error = {"responseCode": "401"};
      Wish wish = Wish.fromJson(error);
      myWish.add(wish);
    }

    return myWish;
  }
}
