import 'dart:convert';

import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/api_util.dart';
import 'package:symmart/model/Product.dart';

class SearchApi {
  Future<List<Product>> getSearchresult(data) async {
    var response = await ApiMain().postData(data, ApiUtil.SEARCH, null);
    List<Product> productResult = [];

    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      for (var item in body['results']) {
        Product products = Product.fromJson(item);
        productResult.add(products);
      }
    }

    return productResult;
  }
}
