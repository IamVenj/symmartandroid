import 'dart:convert';

import 'package:symmart/api/api_util.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:http/http.dart' as http;
import 'package:symmart/model/Order.dart';

class OrderApi {
  Future<List<MainOrder>> getMainOrders(String token) async {
    String myMainOrders = ApiUtil.MAIN_API_URL + ApiUtil.MAINORDER;
    Map<String, String> header;

    if (token != null) {
      header = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      };
    } else {
      header = {'Accept': 'application/json'};
    }

    var response = await http.get(myMainOrders, headers: header);
    List<MainOrder> myOrder = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);

      for (var item in body['order']) {
        MainOrder mainOrder = MainOrder.fromJson(item);
        myOrder.add(mainOrder);
      }
    }

    if (response.statusCode == 401) {
      var error = {"responseCode": "401"};
      MainOrder mainOrder = MainOrder.fromJson(error);
      myOrder.add(mainOrder);
    }

    return myOrder;
  }

  Future<List<Order>> getOrders(String token, String orderId) async {
    String orderUrl = ApiUtil.order(orderId);
    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    };

    var response = await http.get(orderUrl, headers: header);

    List<Order> myOrder = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      for (var item in body['order']) {
        Order order = Order.fromJson(item);
        myOrder.add(order);
      }
    }

    return myOrder;
  }
}
