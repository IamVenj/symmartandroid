import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/api/api_util.dart';
import 'package:http/http.dart' as http;

class ApiMain {
  Future<String> getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    return token;
  }

  Future<dynamic> removeTokenAndUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    localStorage.remove('token');
  }

  getUser() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userJson = localStorage.getString('user');
    var user;
    (userJson != null) ? user = json.decode(userJson) : user = null;
    return user;
  }

  updateUserOnSharedPreference(body) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('user', json.encode(body['user']));
    var userJson = json.decode(localStorage.getString('user'));
    return userJson;
  }

  patchData(data, apiUrl, token) async {
    var url = ApiUtil.MAIN_API_URL + apiUrl;
    if (data == null) {
      return await http.patch(url, headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': (token != null) ? "Bearer $token" : '',
      }).catchError((onError) {
        print("This is the onError caught by posData: $onError");
      });
    } else {
      return await http.patch(url, body: jsonEncode(data), headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': (token != null) ? "Bearer $token" : '',
      }).catchError((onError) {
        print("This is the onError caught by posData: $onError");
      });
    }
  }

  postData(data, apiUrl, token) async {
    var url = ApiUtil.MAIN_API_URL + apiUrl;
    if (data == null) {
      return await http.post(url, headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': (token != null) ? "Bearer $token" : '',
      }).catchError((onError) {
        print("This is the onError caught by posData: $onError");
      });
    } else {
      return await http.post(url, body: jsonEncode(data), headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': (token != null) ? "Bearer $token" : '',
      }).catchError((onError) {
        print("This is the onError caught by posData: $onError");
      });
    }
  }

  deleteData(data, apiUrl, token) async {
    var url = ApiUtil.MAIN_API_URL + apiUrl + '/' + data;
    return await http.delete(url, headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': (token != null) ? "Bearer $token" : '',
    }).catchError((onError) {
      print("This is the onError caught by deleteData: $onError");
    });
  }

  getData(apiUrl, token) async {
    var url = ApiUtil.MAIN_API_URL + apiUrl;
    return await http.get(url, headers: {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': (token != null) ? "Bearer $token" : '',
    }).catchError((onError) {
      print(onError);
    });
  }
}
