import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';

class Cloudinary {
  static const CLOUDINARY_CLOUD_NAME = 'ven950';
  static const CLOUDINARY_SECRET = '2OiY5wD2Ngk-E9t445iizpxJ_rU';
  static const CLOUDINARY_KEY = '877574164327292';
  static const CLOUDINARY_URL =
      'https://res.cloudinary.com/ven950/image/upload/';

  cloudinaryClient() {
    CloudinaryClient client = new CloudinaryClient(
        CLOUDINARY_KEY, CLOUDINARY_SECRET, CLOUDINARY_CLOUD_NAME);
    return client;
  }

  uploadImage(String imagePath, String fileName, String folderName) async {
    CloudinaryClient client = cloudinaryClient();
    CloudinaryResponse response = await client.uploadImage(imagePath,
        filename: fileName, folder: folderName);
    return response;
  }
}
