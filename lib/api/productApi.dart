import 'dart:convert';
import 'package:symmart/api/api_util.dart';
import 'package:symmart/model/Color.dart';
import 'package:symmart/model/Product.dart';
import 'package:http/http.dart' as http;
import 'package:symmart/model/package.dart';
import 'package:symmart/model/productList.dart';
import 'package:symmart/model/user.dart';

class ProductApi {
  Future<List<dynamic>> productsBasedOnCategory(data, page) async {
    String productURL = ApiUtil.MAIN_API_URL + ApiUtil.PRODUCTS + "?page=" + page.toString();

    var response = await http.post(productURL,
        body: jsonEncode(data),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json'
        });

    List<ProductData> productList = [];
    List<ColorClass> colorList = [];
    List<User> vendorList = [];
    List<dynamic> pcList = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      ProductData products = ProductData.fromJson(body['products']);
      productList.add(products);

      for (var item in body['colors']) {
        ColorClass colors = ColorClass.fromJson(item);
        colorList.add(colors);
      }
      for (var item in body['vendors']) {
        User user = User.fromJson(item);
        vendorList.add(user);
      }
    }

    pcList.add({"products": productList});
    pcList.add({"colors": colorList});
    pcList.add({"vendors": vendorList});

    return pcList;
  }

  Future<Product> showProduct(String productId) async {
    String productURL = ApiUtil.currentProduct(productId);

    Map<String, String> header = {'Accept': 'application/json'};
    var response = await http.get(productURL, headers: header);

    Product products;

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      products = Product.fromJson(body['product']);
    }

    return products;
  }

  Future<Package> showPackages(String packageId) async {
    String packageUrl = ApiUtil.currentPackage(packageId);

    Map<String, String> header = {'Accept': 'application/json'};
    var response = await http.get(packageUrl, headers: header);

    Package package;
    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      package = Package.fromJson(body['package']);
    }
    return package;
  }
}
