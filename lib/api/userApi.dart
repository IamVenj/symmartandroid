import 'dart:convert';

import 'package:symmart/api/api_util.dart';
import 'package:symmart/model/user.dart';
import 'package:http/http.dart' as http;

class UserApi {
  Future<List<User>> getUserInformation(String token) async {
    String myUserAccountURL = ApiUtil.MAIN_API_URL + ApiUtil.MY_USER_ACCOUNT;

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
    };
    var response = await http.get(myUserAccountURL, headers: header);

    List<User> myAccount = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = await jsonDecode(response.body);
      User user = User.fromJson(body['account']);
      myAccount.add(user);
    }

    return myAccount;
  }
}
