import 'dart:convert';

import 'package:symmart/api/api_util.dart';
import 'package:http/http.dart' as http;
import 'package:symmart/model/HomeCarousel.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/category.dart';
import 'package:symmart/model/package.dart';
import 'package:symmart/model/setting.dart';

class HomeApi {
  Future<List<dynamic>> checkAuth(String token) async {
    String authCheckURL = ApiUtil.MAIN_API_URL + ApiUtil.AUTHCHECK;
    Map<String, String> headerAuth;
    if (token != null) {
      headerAuth = {
        'Accept': 'application/json',
        'Authorization': "Bearer " + token
      };
    } else {
      headerAuth = {'Accept': 'application/json'};
    }
    var auth = [];
    await http.get(authCheckURL, headers: headerAuth).then((onValue) {
      if (onValue.statusCode == 200) {
        auth.add({"authStatus": true});
      } else if (onValue.statusCode == 401) {
        auth.add({"authStatus": false});
      }
    }).catchError((onError) {
      print(onError);
    });

    return auth;
  }

  Future<List<dynamic>> getHomeData() async {
    String orderUrl = ApiUtil.MAIN_API_URL + ApiUtil.HOMEPAGE;

    Map<String, String> header = {'Accept': 'application/json'};
    var response = await http.get(orderUrl, headers: header);

    List home = [];
    List<Product> latestProductArray = [];
    List<Product> recommendedOfferArray = [];
    List<Package> packageArray = [];
    List<Product> bestSoldArray = [];
    List<HomeCarousel> homeCarouselArray = [];
    List<CategoryMain> categoryArray = [];
    List<CategoryMain> drawCategoryArray = [];
    List<CompanySettings> settingsArray = [];

    if (response.statusCode == 200) {
      Map<String, dynamic> body = jsonDecode(response.body);
      for (var itemMain in body['home']) {
        if (itemMain['latestProduct'] != null) {
          for (var item in itemMain['latestProduct']) {
            Product lProduct = Product.fromJson(item);
            latestProductArray.add(lProduct);
          }
        }
        if (itemMain['recommendedOffers'] != null) {
          for (var item in itemMain['recommendedOffers']) {
            Product recommendedOffer = Product.fromJson(item);
            recommendedOfferArray.add(recommendedOffer);
          }
        }
        if (itemMain['packages'] != null) {
          for (var item in itemMain['packages']) {
            Package packageProducts = Package.fromJson(item);
            packageArray.add(packageProducts);
          }
        }
        if (itemMain['bestSold'] != null) {
          for (var item in itemMain['bestSold']) {
            Product bSelling = Product.fromJson(item);
            bestSoldArray.add(bSelling);
          }
        }
        if (itemMain['carousel'] != null) {
          for (var item in itemMain['carousel']) {
            HomeCarousel homeCarousel = HomeCarousel.fromJson(item);
            homeCarouselArray.add(homeCarousel);
          }
        }
        if (itemMain['category'] != null) {
          for (var item in itemMain['category']) {
            CategoryMain category = CategoryMain.fromJson(item);
            categoryArray.add(category);
          }
        }
        if (itemMain['drawCategory'] != null) {
          for (var item in itemMain['drawCategory']) {
            CategoryMain drawCategory = CategoryMain.fromJson(item);
            drawCategoryArray.add(drawCategory);
          }
        }
        if (itemMain['setting'] != null) {
          CompanySettings companySettings =
              CompanySettings.fromJson(itemMain['setting']);
          settingsArray.add(companySettings);
        }
      }
    }

    home.add({"category": categoryArray});
    home.add({"carousel": homeCarouselArray});
    home.add({"bestSold": bestSoldArray});
    home.add({"packages": packageArray});
    home.add({"recommendedOffers": recommendedOfferArray});
    home.add({"latestProduct": latestProductArray});
    home.add({"drawCategory": drawCategoryArray});
    home.add({"setting": settingsArray});

    return home;
  }
}
