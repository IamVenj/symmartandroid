import 'dart:convert';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Wish.dart';

const kGoogleApiKey = "AIzaSyBPCRMMp4wCB1RYgrj0lavgRLSn-v9kz8g";
GoogleMapsPlaces placesMap = GoogleMapsPlaces(apiKey: kGoogleApiKey);

class SignupScreen extends StatefulWidget {
  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;

  const SignupScreen(
      {Key key,
      this.fieldKey,
      this.hintText,
      this.labelText,
      this.helperText,
      this.onSaved,
      this.validator,
      this.onFieldSubmitted})
      : super(key: key);

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Colors.red,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(color: Colors.yellow, fontSize: 24.0),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => Signup();
}

class Signup extends State<SignupScreen> {
  ShapeBorder shape;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  List<PlacesSearchResult> places = [];
  bool mapIsLoading = false;
  String errorMessage;
  PlacesDetailsResponse place;
  ApiMain apiMain;
  var address;

  String email;
  String password;
  String confirmpassword;
  String fullname;
  String phoneNumber;
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();

  bool _autovalidate = false;
  bool formWasEdited = false;
  bool _obscureText = true;
  bool _obscureTextConfirm = true;
  bool isLoading = false;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  // String _validateName(String value) {
  //   formWasEdited = true;
  //   if (value.isEmpty) return 'Name is required.';
  //   final RegExp nameExp = RegExp(r'^[A-Za-z ]+$');
  //   if (!nameExp.hasMatch(value))
  //     return 'Please enter only alphabetical characters.';
  //   return null;
  // }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          title: Text('Signup'),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        body: SafeArea(
            child: Container(
          height: height,
          child: Stack(fit: StackFit.expand, children: <Widget>[
            Positioned.fill(
              child: Image.asset(
                'images/home.jpg',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              foregroundDecoration:
                  BoxDecoration(color: Colors.white.withOpacity(0.86)),
            ),
            new SingleChildScrollView(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: height * 0.02,
                  ),
                  new SafeArea(
                      top: false,
                      bottom: false,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.04, right: width * 0.04),
                        child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(width * 0.07)),
                            elevation: 1,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: width * 0.02, right: width * 0.02),
                              child: Form(
                                  key: formKey,
                                  autovalidate: _autovalidate,
                                  child: SingleChildScrollView(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          SizedBox(
                                            height: height * 0.06,
                                          ),
                                          new Divider(
                                            height: height * 0.01,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                'Welcome to',
                                                style: TextStyle(
                                                    fontSize: width * 0.08),
                                              ),
                                              Text(
                                                'Symmart',
                                                style: TextStyle(
                                                    fontSize: width * 0.08,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ],
                                          ),
                                          new Divider(
                                            height: height * 0.01,
                                          ),
                                          SizedBox(
                                            height: height * 0.06,
                                          ),
                                          Center(
                                              child: Text(
                                            'Signup on Symmart',
                                            style: TextStyle(
                                                fontSize: width * 0.046,
                                                fontWeight: FontWeight.w700),
                                          )),
                                          SizedBox(height: height * 0.02),
                                          TextFormField(
                                            controller: nameController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.account_circle,
                                                    color: Colors.grey),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText: 'Full Name',
                                                labelText: 'Full Name',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) =>
                                                (nameController.text.length ==
                                                        0)
                                                    ? 'Fullname is required!'
                                                    : null,
                                            onSaved: (val) => fullname = val,
                                          ),
                                          SizedBox(height: height * 0.01),
                                          TextFormField(
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(Icons.email,
                                                    color: Colors.grey),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: 3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: 3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText: 'Your email address',
                                                labelText: 'E-mail',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            validator: (val) =>
                                                !val.contains('@')
                                                    ? 'Not a valid email.'
                                                    : null,
                                            onSaved: (val) => email = val,
                                            controller: emailController,
                                          ),
                                          SizedBox(height: height * 0.01),
                                          TextFormField(
                                            controller: phoneController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.phone_iphone,
                                                    color: Colors.grey),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText: 'Phone Number',
                                                labelText: 'Phone Number',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) => (phoneController
                                                        .text.length ==
                                                    0)
                                                ? 'Phone number is required!'
                                                : null,
                                            onSaved: (val) => phoneNumber = val,
                                          ),
                                          SizedBox(height: height * 0.01),
                                          TextFormField(
                                            obscureText: _obscureText,
                                            controller: passwordController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.lock_outline,
                                                    color: Colors.grey),
                                                suffixIcon: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      if (_obscureText ==
                                                          true) {
                                                        _obscureText = false;
                                                      } else if (_obscureText ==
                                                          false) {
                                                        _obscureText = true;
                                                      }
                                                    });
                                                  },
                                                  child: Icon(
                                                    Icons.remove_red_eye,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText: 'Your password',
                                                labelText: 'Password',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) =>
                                                passwordController.text.length <
                                                        6
                                                    ? 'Password too short.'
                                                    : null,
                                            onSaved: (val) => password = val,
                                          ),
                                          SizedBox(height: height * 0.01),
                                          TextFormField(
                                            obscureText: _obscureTextConfirm,
                                            controller:
                                                confirmPasswordController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.lock_outline,
                                                    color: Colors.grey),
                                                suffixIcon: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      if (_obscureTextConfirm ==
                                                          true) {
                                                        _obscureTextConfirm =
                                                            false;
                                                      } else if (_obscureTextConfirm ==
                                                          false) {
                                                        _obscureTextConfirm =
                                                            true;
                                                      }
                                                    });
                                                  },
                                                  child: Icon(
                                                    Icons.remove_red_eye,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText:
                                                    'Confirm Your password',
                                                labelText: 'Confirm Password',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) =>
                                                (confirmPasswordController
                                                            .text !=
                                                        passwordController.text)
                                                    ? 'Password Confirmation is not equal to password!'
                                                    : null,
                                            onSaved: (val) =>
                                                confirmpassword = val,
                                          ),
                                          SizedBox(height: height * 0.01),
                                          TextFormField(
                                            controller: addressController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.017),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.location_searching,
                                                    color: Colors.grey),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.7),
                                                ),
                                                hintText: 'your Address',
                                                labelText: 'Address',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) => (addressController
                                                        .text.length <=
                                                    0)
                                                ? 'Address field is required!'
                                                : null,
                                            onTap: () => handleAutoComplete(),
                                            maxLength: 900,
                                          ),
                                          SizedBox(
                                            height: height * 0.04,
                                          ),
                                          new Container(
                                            child: Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                GestureDetector(
                                                  onTap: !isLoading
                                                      ? () {
                                                          _submit();
                                                        }
                                                      : null,
                                                  child: new Container(
                                                    alignment:
                                                        Alignment.bottomRight,
                                                    decoration: BoxDecoration(
                                                      color: Colors.black,
                                                    ),
                                                    child: Stack(
                                                      children: <Widget>[
                                                        Positioned(
                                                          right: width * 0.04,
                                                          top: height * 0.014,
                                                          child: Icon(
                                                            Icons.save_alt,
                                                            color: Colors.white,
                                                            size: width * 0.04,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 18.0,
                                                                  right: width *
                                                                      0.12,
                                                                  top: 8.0,
                                                                  bottom: 8.0),
                                                          child: Text(
                                                            'Sign Up',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                    width *
                                                                        0.06,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                isLoading
                                                    ? Stack(
                                                        children: <Widget>[
                                                          Container(
                                                            child: loader(),
                                                            height:
                                                                height * 0.0497,
                                                            width: width * 0.1,
                                                          )
                                                        ],
                                                      )
                                                    : Container(),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: height * 0.04,
                                          ),
                                        ]),
                                  )),
                            ) //login,
                            ),
                      )),
                  SizedBox(
                    height: height * 0.02,
                  ),
                ],
              ),
            ),
          ]),
        )));
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  List<dynamic> newData = [];
  Map<String, dynamic> data = {};
  List<MainOrder> orders = [];
  List<Wish> wishes = [];

  getData() async {
    await HomeApi().getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
    var token = await ApiMain().getToken();
    await OrderApi().getMainOrders(token).then((onValue) {
      for (var item in onValue) {
        orders.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await WishApi().getWishes().then((onValue) {
      for (var item in onValue) {
        wishes.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  void _submit() async {
    print("whar");
    final form = formKey.currentState;
    setState(() {
      isLoading = true;
    });

    // add photoUrl when done with authAPI setup

    var passedData = {
      'fullname': nameController.text,
      'email': emailController.text,
      'phoneNumber': phoneController.text,
      'password': passwordController.text,
      'password_confirmation': confirmPasswordController.text,
      // 'photoUrl': confirmPasswordController.text,
      'address': address,
    };

    if (form.validate()) {
      form.save();
      var response = await ApiMain().postData(passedData, '/register', null);
      var body = json.decode(response.body);

      if (response.statusCode == 200) {
        await getData();
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', body['token']);
        localStorage.setString('user', json.encode(body['user']));

        _performLogin(data);
      }
      if (response.statusCode == 422) {
        setState(() {
          isLoading = false;
        });

        if (body['errors']['email'].length > 0) {
          showInSnackBar("${body['errors']['email'][0]}");
        }
      }
    } else {
      setState(() {
        isLoading = false;
      });
      showInSnackBar('Please fix the errors in red before submitting.');
    }
  }

  String validateMobile(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  showListInSnackBar(List<String> value) {
    value.map((k) {
      return scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text('${k[0]}')));
    });
    return null;
  }

  void _performLogin(data) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Homescreen(
                  authStatus: true,
                  data: data,
                  orders: orders,
                  wishes: wishes,
                )));
  }

  void onError(PlacesAutocompleteResponse response) {
    scaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<void> handleAutoComplete() async {
    try {
      Prediction p = await PlacesAutocomplete.show(
        context: context,
        strictbounds: true,
        apiKey: kGoogleApiKey,
        onError: onError,
        mode: Mode.overlay,
        language: "en",
        location: Location(9.0371306, 38.7489053),
        radius: 10000,
      );

      PlacesDetailsResponse place =
          await placesMap.getDetailsByPlaceId(p.placeId);

      if (mounted) {
        setState(() {
          if (place.status == "OK") {
            setState(() {
              addressController.text =
                  place.result.name + ', ' + place.result.formattedAddress;
              setAddress(place);
            });
          }
        });
      }
    } catch (e) {
      return;
    }
  }

  setAddress(PlacesDetailsResponse place) {
    if (place != null) {
      address = [
        {
          'address': addressController.text,
          'latitude': place.result.geometry.location.lat,
          'longitude': place.result.geometry.location.lng
        }
      ];

      return address;
    }
    return null;
  }
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}
