import 'dart:convert';

import 'package:flare_flutter/flare_actor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Wish.dart';
import 'package:symmart/signup_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  final Key fieldKey;
  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;

  const LoginScreen(
      {Key key,
      this.fieldKey,
      this.hintText,
      this.labelText,
      this.helperText,
      this.onSaved,
      this.validator,
      this.onFieldSubmitted})
      : super(key: key);

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Colors.red,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(color: Colors.yellow, fontSize: 24.0),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => Login();
}

class Login extends State<LoginScreen> {
  ShapeBorder shape;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  String email;
  String password;
  final GlobalKey<FormState> formKey2 = GlobalKey<FormState>();

  bool _autovalidate = false;
  bool formWasEdited = false;
  bool _obscureText = true;
  bool isLoading = false;

  TextEditingController emailLoginController = TextEditingController();
  TextEditingController passwordLoginController = TextEditingController();

  // String _validateName(String value) {
  //   formWasEdited = true;
  //   if (value.isEmpty) return 'Name is required.';
  //   final RegExp nameExp = RegExp(r'^[A-Za-z ]+$');
  //   if (!nameExp.hasMatch(value))
  //     return 'Please enter only alphabetical characters.';
  //   return null;
  // }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return new Scaffold(
        key: scaffoldKey,
        body: SafeArea(
            child: Container(
          height: height,
          child: Stack(children: <Widget>[
            Positioned.fill(
              child: Image.asset(
                'images/home.jpg',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              foregroundDecoration:
                  BoxDecoration(color: Colors.white.withOpacity(0.86)),
            ),
            SingleChildScrollView(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: height * 0.1,
                  ),
                  new SafeArea(
                      top: false,
                      bottom: false,
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.04, right: width * 0.04),
                        child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(width * 0.07)),
                            elevation: 1,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  left: width * 0.02, right: width * 0.02),
                              child: Form(
                                  key: formKey,
                                  autovalidate: _autovalidate,
                                  child: SingleChildScrollView(
                                    padding: EdgeInsets.all(width * 0.04),
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          SizedBox(
                                            height: height * 0.06,
                                          ),
                                          new Divider(
                                            height: height * 0.01,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                'Welcome to',
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    fontSize: width * 0.08),
                                              ),
                                              Text(
                                                'Symmart',
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    fontSize: width * 0.08,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ],
                                          ),
                                          new Divider(
                                            height: height * 0.01,
                                          ),
                                          SizedBox(
                                            height: height * 0.06,
                                          ),
                                          Center(
                                              child: Text(
                                            'Login Into Your Account',
                                            textScaleFactor: 0.8,
                                            style: TextStyle(
                                                fontSize: width * 0.046,
                                                fontWeight: FontWeight.w700),
                                          )),
                                          SizedBox(height: height * 0.02),
                                          TextFormField(
                                            controller: emailLoginController,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.006),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(Icons.email,
                                                    color: Colors.grey),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: 3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.01),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.grey.shade100,
                                                    style: BorderStyle.solid,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.01),
                                                ),
                                                hintText: 'Your email address',
                                                labelText: 'E-mail',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            validator: (val) =>
                                                !val.contains('@')
                                                    ? 'Not a valid email.'
                                                    : null,
                                            onSaved: (val) => email = val,
                                          ),
                                          SizedBox(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          TextFormField(
                                            obscureText: _obscureText,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.all(
                                                    height * 0.006),
                                                filled: true,
                                                fillColor: Colors.grey.shade100,
                                                prefixIcon: Icon(
                                                    Icons.lock_outline,
                                                    color: Colors.grey),
                                                suffixIcon: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      if (_obscureText ==
                                                          true) {
                                                        _obscureText = false;
                                                      } else if (_obscureText ==
                                                          false) {
                                                        _obscureText = true;
                                                      }
                                                    });
                                                  },
                                                  child: Icon(
                                                    Icons.remove_red_eye,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.01),
                                                ),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.grey.shade100,
                                                      style: BorderStyle.solid,
                                                      width: .3),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.01),
                                                ),
                                                hintText: 'Your password',
                                                labelText: 'Password',
                                                labelStyle: TextStyle(
                                                    color: Colors.black54)),
                                            validator: (val) => val.length < 6
                                                ? 'Password too short.'
                                                : null,
                                            onSaved: (val) => password = val,
                                            controller: passwordLoginController,
                                          ),
                                          SizedBox(
                                            height: height * 0.04,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                width: width * 0.36,
                                                child: Stack(
                                                  children: <Widget>[
                                                    FlatButton.icon(
                                                      // elevation: 4.0,
                                                      icon: Icon(Icons.vpn_key,
                                                          color: Color(
                                                              0xFF13A89E)),
                                                      label: Text("Sign In",
                                                      textScaleFactor: 0.8,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  width * 0.06,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      onPressed: !isLoading
                                                          ? () {
                                                              _submit();
                                                            }
                                                          : null,
                                                      color: Color(0xFF13A89E)
                                                          .withOpacity(0.2),
                                                      textColor:
                                                          Color(0xFF13A89E),
                                                      disabledColor:
                                                          Color(0xFF13A89E),
                                                      disabledTextColor:
                                                          Colors.white,
                                                    ),
                                                    isLoading
                                                        ? Positioned(
                                                            left: width * 0.01,
                                                            top: height * 0.012,
                                                            child: Container(
                                                              height:
                                                                  height * 0.03,
                                                              width:
                                                                  width * 0.06,
                                                              child:
                                                                  CircularProgressIndicator(
                                                                valueColor:
                                                                    AlwaysStoppedAnimation(
                                                                        Colors
                                                                            .white),
                                                                backgroundColor:
                                                                    Color(
                                                                        0xFF13A89E),
                                                                strokeWidth:
                                                                    1.0,
                                                              ),
                                                            ))
                                                        : Container()
                                                  ],
                                                ),
                                              ),
                                              new GestureDetector(
                                                onTap: () {
                                                  forgotPassword();
                                                },
                                                child: Text(
                                                  'Forgot Password?',
                                                  textScaleFactor: 0.8,
                                                  style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize: width * 0.045,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: height * 0.06,
                                          ),
                                        ]),
                                  )),
                            ) //login,
                            ),
                      )),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Dont have an account? ',
                        textScaleFactor: 0.8,
                        style: TextStyle(
                            fontSize: width * 0.05, color: Colors.black),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignupScreen()));
                        },
                        child: Text(
                          'Signup',
                          textScaleFactor: 0.8,
                          style: TextStyle(
                              fontSize: width * 0.05, color: Color(0xFF13A89E)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ]),
        )));
  }

  void showSuccessInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  void showErrorInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.red,
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  Future<bool> forgotPassword() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return ForgotPasswordDialog(
            showErrorInSnackBar: showErrorInSnackBar,
            showSuccessInSnackBar: showSuccessInSnackBar,
          );
        });
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  List<dynamic> newData = [];
  Map<String, dynamic> data = {};
  List<MainOrder> orders = [];
  List<Wish> wishes = [];

  getData() async {
    await HomeApi().getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
  }

  getAuthData() async {
    var token = await ApiMain().getToken();
    await OrderApi().getMainOrders(token).then((onValue) {
      for (var item in onValue) {
        orders.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await WishApi().getWishes().then((onValue) {
      for (var item in onValue) {
        wishes.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  void _submit() async {
    setState(() {
      isLoading = true;
    });

    final form = formKey.currentState;
    var passedData = {
      "email": emailLoginController.text,
      "password": passwordLoginController.text
    };

    if (form.validate()) {
      form.save();
      var response = await ApiMain().postData(passedData, '/login', null);
      var body = json.decode(response.body);

      if (response.statusCode == 200) {
        if (body['status'] == 0) {
          showInSnackBar(body['message']);
        }
        if (body['status'] == 1) {
          await getData();
          SharedPreferences localStorage =
              await SharedPreferences.getInstance();
          localStorage.setString('token', body['token']);
          UserBloc().inUser.add(body);
          await getAuthData();
          _performLogin(data);
        }
      }
      if (response.statusCode == 401) {
        if (body['error'].length > 0) {
          showInSnackBar("Your credentials are not correct! Please try again");
        }
      }
      setState(() {
        isLoading = false;
      });
    } else {
      setState(() {
        isLoading = false;
      });
      showInSnackBar('Please fix the errors in red before submitting.');
    }
  }

  String validateMobile(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  void _performLogin(data) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Homescreen(
                  authStatus: true,
                  data: data,
                  orders: orders,
                  wishes: wishes,
                )));
  }
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}

class ForgotPasswordDialog extends StatefulWidget {
  ForgotPasswordDialog(
      {Key key, this.showErrorInSnackBar, this.showSuccessInSnackBar})
      : super(key: key);
  final showErrorInSnackBar;
  final showSuccessInSnackBar;
  @override
  _ForgotPasswordDialogState createState() => _ForgotPasswordDialogState();
}

class _ForgotPasswordDialogState extends State<ForgotPasswordDialog> {
  bool _linkSending = false;
  TextEditingController emailController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.04)),
      child: Container(
        height: height * 0.33,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.08)),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: height * 0.18,
                    ),
                    Container(
                      height: height * 0.12,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(width * 0.04),
                              topRight: Radius.circular(width * 0.04)),
                          color: Color(0xFF13A89E).withOpacity(0.2)),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.06,
                        ),
                        Center(
                          child: ClipOval(
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              padding: EdgeInsets.all(width * 0.02),
                              color: Colors.white.withOpacity(0.8),
                              child: Icon(
                                Icons.lock_open,
                                size: width * 0.2,
                                color: Color(0xFF13A89E),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Form(
                  key: formKey,
                  autovalidate: _autovalidate,
                  child: Container(
                    // width: width * 0.8,
                    padding: EdgeInsets.only(
                        left: width * 0.02, right: width * 0.02),
                    child: TextFormField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(height * 0.006),
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          prefixIcon: Icon(Icons.email, color: Colors.grey),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey.shade100,
                                style: BorderStyle.solid,
                                width: .3),
                            // borderRadius: BorderRadius.circular(width * 0.7),
                          ),
                          hintText: 'Your Email',
                          labelText: 'Email',
                          labelStyle: TextStyle(color: Colors.black54)),
                      validator: (val) =>
                          val.length == 0 ? 'Email is required!' : null,
                      controller: emailController,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: width * 0.02,
                    right: width * 0.02,
                  ),
                  width: width,
                  child: FlatButton.icon(
                    icon: Icon(Icons.email),
                    label: Text("Submit"),
                    color: Color(0xFF13A89E).withOpacity(0.2),
                    textColor: Color(0xFF13A89E),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.04)),
                    onPressed: !_linkSending
                        ? () async {
                            await submitForgotPassword();
                          }
                        : null,
                  ),
                )
              ],
            ),
            _linkSending
                ? Positioned(
                    bottom: height * 0.016,
                    right: width * 0.03,
                    child: Container(
                      height: height * 0.02,
                      width: width * 0.04,
                      child: CircularProgressIndicator(
                        strokeWidth: 1.0,
                        valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  submitForgotPassword() async {
    setState(() {
      _linkSending = true;
    });
    final form = formKey.currentState;
    if (form.validate()) {
      var passedData = {"email": emailController.text};
      var response =
          await ApiMain().postData(passedData, '/forgot-password', null);
      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        Navigator.pop(context);
        if (body['status'] == "success") {
          widget.showSuccessInSnackBar(body["message"]);
        } else if (body['status'] == "failed") {
          widget.showErrorInSnackBar(body["message"]);
        }
        setState(() {
          _linkSending = false;
        });
      }
    } else {
      setState(() {
        _linkSending = false;
      });
      widget.showErrorInSnackBar(
          'Please fix the errors in red before submitting.');
    }
  }
}
