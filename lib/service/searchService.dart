import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/SearchApi.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/category.dart';

class SearchService extends SearchDelegate<Product> {
  final List<CategoryMain> categories;
  int _selectedCategory;

  SearchService(this.categories);

  SearchApi searchApi = new SearchApi();

  @override
  List<Widget> buildActions(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return [
      Row(
        children: <Widget>[
          StatefulBuilder(
            builder: (context, StateSetter setState) {
              return DropdownButton<int>(
                value: _selectedCategory,
                icon: Icon(Icons.arrow_drop_down),
                iconSize: width * 0.04,
                elevation: 1,
                isDense: true,
                style: TextStyle(color: Colors.black),
                underline: Container(
                  height: 2,
                  color: Colors.grey.shade100,
                ),
                isExpanded: false,
                onChanged: (int newValue) {
                  setState(() {
                    _selectedCategory = newValue;
                  });
                },
                hint: Text("All Category"),
                items: categories.map<DropdownMenuItem<int>>((value) {
                  return DropdownMenuItem<int>(
                    value: int.parse(value.id),
                    child: Text(value.categoryName),
                  );
                }).toList(),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: () => query = "",
          ),
        ],
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var data = {"category": _selectedCategory, "searchquery": query};
    return FutureBuilder(
        future: searchApi.getSearchresult(data),
        builder: (context, AsyncSnapshot<List<Product>> snapshot) {
          return (!snapshot.hasData)
              ? Container(
                  height: height,
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: height * 0.2,
                          width: width * 0.4,
                          child: loader(),
                        ),
                      ],
                    ),
                  ))
              : (snapshot.data.isEmpty)
                  ? Container(
                      height: height,
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: height * 0.2,
                              width: width * 0.4,
                              child: empty(),
                            ),
                            Text("Sorry! No Result was found",
                            textScaleFactor: 0.86,
                                style: Theme.of(context).textTheme.display1),
                          ],
                        ),
                      ))
                  : (query.isNotEmpty)
                      ? SingleChildScrollView(
                          child: Container(
                            height: height,
                            child: ListView.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int i) {
                                return GestureDetector(
                                  onTap: () {
                                    close(context, snapshot.data[i]);
                                  },
                                  child: Card(
                                    margin: EdgeInsets.only(
                                        left: width * 0.02,
                                        right: width * 0.02,
                                        top: height * 0.01),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            width * 0.02)),
                                    elevation: 1.0,
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          height: height * 0.1,
                                          width: width * 0.25,
                                          child: CachedNetworkImage(
                                            imageUrl: snapshot
                                                .data[i]
                                                .productImage
                                                .first
                                                .imagePublicId,
                                            fit: BoxFit.contain,
                                            placeholder: (context, url) =>
                                                loader(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    empty(),
                                          ),
                                        ),
                                        SizedBox(
                                          width: width * 0.03,
                                        ),
                                        Container(
                                          height: height * 0.1,
                                          width: width * 0.45,
                                          padding: EdgeInsets.only(
                                              bottom: height * 0.01),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              SizedBox(
                                                height: height * 0.01,
                                              ),
                                              Text(
                                                (snapshot.data[i].productName
                                                            .length >
                                                        20)
                                                    ? snapshot
                                                        .data[i].productName
                                                        .substring(0, 20)
                                                    : snapshot
                                                        .data[i].productName,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .title,
                                              ),
                                              SizedBox(
                                                height: height * 0.01,
                                              ),
                                              Flexible(
                                                child: Text((snapshot.data[i]
                                                            .slug.length >
                                                        50)
                                                    ? snapshot.data[i].slug
                                                        .substring(0, 50)
                                                    : snapshot.data[i].slug),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: width * 0.2,
                                          height: height * 0.1,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                "${snapshot.data[i].currentPrice} Birr",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .body1,
                                              ),
                                              (snapshot.data[i].oldPrice !=
                                                      null)
                                                  ? Text(
                                                      "${snapshot.data[i].oldPrice} Birr",
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          decoration:
                                                              TextDecoration
                                                                  .lineThrough,
                                                          fontSize:
                                                              width * 0.03),
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      : Container(
                          height: height,
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: height * 0.2,
                                  width: width * 0.4,
                                  child: loader(),
                                ),
                                Text("Write something..")
                              ],
                            ),
                          ));
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    var data = {"category": _selectedCategory, "searchquery": query};
    return FutureBuilder(
        future: searchApi.getSearchresult(data),
        builder: (context, AsyncSnapshot<List<Product>> snapshot) {
          return (query.isEmpty)
              ? Container(
                  height: height,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: width,
                        child: LinearProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                          backgroundColor: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: height * 0.1,
                      ),
                      Text(
                        "Waiting for a search keyword..",
                        style: Theme.of(context).textTheme.subtitle,
                      )
                    ],
                  ),
                )
              : (!snapshot.hasData)
                  ? Container(
                      height: height,
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: height * 0.2,
                              width: width * 0.4,
                              child: loader(),
                            ),
                          ],
                        ),
                      ))
                  : (snapshot.data.isEmpty)
                      ? Container(
                          height: height,
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: height * 0.2,
                                  width: width * 0.4,
                                  child: empty(),
                                ),
                                Text("Sorry! No Result was found",
                                textScaleFactor: 0.86,
                                    style:
                                        Theme.of(context).textTheme.title),
                              ],
                            ),
                          ))
                      : (query.isNotEmpty)
                          ? SingleChildScrollView(
                              child: Container(
                                height: height,
                                child: ListView.builder(
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (BuildContext context, int i) {
                                    return GestureDetector(
                                      onTap: () {
                                        close(context, snapshot.data[i]);
                                      },
                                      child: Card(
                                        margin: EdgeInsets.only(
                                            left: width * 0.02,
                                            right: width * 0.02,
                                            top: height * 0.01),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                width * 0.02)),
                                        elevation: 1.0,
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: height * 0.1,
                                              width: width * 0.25,
                                              child: CachedNetworkImage(
                                                imageUrl: snapshot
                                                    .data[i]
                                                    .productImage
                                                    .first
                                                    .imagePublicId,
                                                fit: BoxFit.contain,
                                                placeholder: (context, url) =>
                                                    loader(),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        empty(),
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * 0.03,
                                            ),
                                            Container(
                                              height: height * 0.1,
                                              width: width * 0.45,
                                              padding: EdgeInsets.only(
                                                  bottom: height * 0.01),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  SizedBox(
                                                    height: height * 0.01,
                                                  ),
                                                  Text(
                                                    (snapshot
                                                                .data[i]
                                                                .productName
                                                                .length >
                                                            20)
                                                        ? snapshot
                                                            .data[i].productName
                                                            .substring(0, 20)
                                                        : snapshot.data[i]
                                                            .productName,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .title,
                                                        textScaleFactor: 0.86,
                                                  ),
                                                  SizedBox(
                                                    height: height * 0.01,
                                                  ),
                                                  Flexible(
                                                    child: Text((snapshot
                                                                .data[i]
                                                                .slug
                                                                .length >
                                                            50)
                                                        ? snapshot.data[i].slug
                                                            .substring(0, 50)
                                                        : snapshot
                                                            .data[i].slug
                                                            ,textScaleFactor: 0.86,),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              width: width * 0.2,
                                              height: height * 0.1,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "${snapshot.data[i].currentPrice} Birr",
                                                    textScaleFactor: 0.86,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .body1,
                                                  ),
                                                  (snapshot.data[i].oldPrice !=
                                                          null)
                                                      ? Text(
                                                          "${snapshot.data[i].oldPrice} Birr",
                                                          textScaleFactor: 0.86,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.grey,
                                                              decoration:
                                                                  TextDecoration
                                                                      .lineThrough,
                                                              fontSize:
                                                                  width * 0.03),
                                                        )
                                                      : Container(),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            )
                          : Container(
                              height: height,
                              child: Center(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: height * 0.2,
                                      width: width * 0.4,
                                      child: loader(),
                                    ),
                                    Text("Write something..")
                                  ],
                                ),
                              ));
        });
  }
}
