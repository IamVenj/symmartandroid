import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/model/setting.dart';

class SettingScreen extends StatefulWidget {
  final String toolbarname;
  final List<CompanySettings> setting;
  SettingScreen({Key key, this.toolbarname, @required this.setting})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => Setting(toolbarname);
}

class Setting extends State<SettingScreen> {
  List list = ['12', '11'];
  HomeApi homeApi = HomeApi();

  bool switchValue = false;

  // String toolbarname = 'Fruiys & Vegetables';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String toolbarname;

  Setting(this.toolbarname);

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    return new Scaffold(
        key: _scaffoldKey,
        body: Container(
          child: Column(
            children: <Widget>[
              new Container(
                height: height * 0.07,
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 7.0),
                child: Card(
                  margin:
                      EdgeInsets.only(left: width * 0.04, right: width * 0.04),
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: width * 0.7,
                        top: 0,
                        bottom: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(width * 0.1)),
                              color: Colors.grey.shade100.withOpacity(0.7)),
                        ),
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.info),
                          _verticalD(),
                          Text(
                            'Information',
                            style: TextStyle(
                                fontSize: width * 0.07,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: height * 0.04),
                  child: Card(
                    margin: EdgeInsets.only(
                        left: width * 0.04, right: width * 0.04),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          left: 0,
                          top: 0,
                          bottom: 0,
                          right: width * 0.6,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(width * 0.6),
                                    topRight: Radius.circular(width * 0.6)),
                                color: Colors.grey.shade100.withOpacity(0.7)),
                          ),
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.local_shipping,
                                  color: Colors.black54),
                              title: Text(
                                'Shipping Guide',
                                style: TextStyle(
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                ),
                              ),
                              onTap: () {
                                shippingGuideDialog(
                                  context,
                                  widget.setting[0].shippingGuide,
                                );
                              },
                            ),
                            Divider(
                              height: 5.0,
                            ),
                            ListTile(
                              leading: Icon(Icons.assignment_return,
                                  color: Colors.black54),
                              title: Text(
                                'Shipping and Return',
                                style: TextStyle(
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                ),
                              ),
                              onTap: () {
                                shippingReturn(
                                    context, widget.setting[0].shippingReturn);
                              },
                            ),
                            Divider(
                              height: 5.0,
                            ),
                            ListTile(
                              leading: Icon(Icons.info_outline,
                                  color: Colors.black54),
                              title: Text(
                                'Terms and Conditions',
                                style: TextStyle(
                                  fontSize: width * 0.05,
                                  color: Colors.black87,
                                ),
                              ),
                              onTap: () {
                                termsAndConditions(context,
                                    widget.setting[0].termsAndCondition);
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ));
  }

  Future<bool> shippingGuideDialog(context, guide) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return ShippingGuideDialog(
            shippingGuide: guide,
          );
        });
  }

  Future<bool> shippingReturn(context, shippingreturn) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return ShippingReturn(
            shippingUndReturn: shippingreturn,
          );
        });
  }

  Future<bool> termsAndConditions(context, terms) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return TermsAndConditions(
            termsUndConditions: terms,
          );
        });
  }

  _verticalD() => Container(
        margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
      );

  erticalD() => Container(
        margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
      );
}

class ShippingGuideDialog extends StatefulWidget {
  final String shippingGuide;
  ShippingGuideDialog({Key key, @required this.shippingGuide})
      : super(key: key);

  @override
  _ShippingGuideDialogState createState() => _ShippingGuideDialogState();
}

class _ShippingGuideDialogState extends State<ShippingGuideDialog> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      key: scaffoldKey,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.02)),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.02)),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: height * 0.18,
                  ),
                  Container(
                    height: height * 0.12,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(width * 0.02),
                            topRight: Radius.circular(width * 0.02)),
                        color: Color(0xFF13A89E).withOpacity(0.2)),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: height * 0.06,
                      ),
                      Center(
                        child: ClipOval(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Container(
                            padding: EdgeInsets.all(width * 0.02),
                            color: Colors.white.withOpacity(0.8),
                            child: Icon(
                              Icons.local_shipping,
                              size: width * 0.2,
                              color: Color(0xFF13A89E),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    left: width * 0.04,
                    right: width * 0.04,
                    bottom: width * 0.04),
                child: Html(
                    data: """${widget.shippingGuide}""",
                    onLinkTap: (url) {
                      print(url);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShippingReturn extends StatefulWidget {
  final String shippingUndReturn;
  ShippingReturn({Key key, @required this.shippingUndReturn}) : super(key: key);

  @override
  _ShippingReturnState createState() => _ShippingReturnState();
}

class _ShippingReturnState extends State<ShippingReturn> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.02)),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.02)),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: height * 0.18,
                  ),
                  Container(
                    height: height * 0.12,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(width * 0.02),
                            topRight: Radius.circular(width * 0.02)),
                        color: Color(0xFF13A89E).withOpacity(0.2)),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: height * 0.06,
                      ),
                      Center(
                        child: ClipOval(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Container(
                            padding: EdgeInsets.all(width * 0.02),
                            color: Colors.white.withOpacity(0.8),
                            child: Icon(
                              Icons.assignment_return,
                              size: width * 0.2,
                              color: Color(0xFF13A89E),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    left: width * 0.04,
                    right: width * 0.04,
                    bottom: width * 0.04),
                child: Html(
                    data: """${widget.shippingUndReturn}""",
                    onLinkTap: (url) {
                      print(url);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TermsAndConditions extends StatefulWidget {
  final String termsUndConditions;
  TermsAndConditions({Key key, @required this.termsUndConditions})
      : super(key: key);

  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width * 0.02)),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(width * 0.02)),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: height * 0.18,
                  ),
                  Container(
                    height: height * 0.12,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(width * 0.02),
                            topRight: Radius.circular(width * 0.02)),
                        color: Color(0xFF13A89E).withOpacity(0.2)),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: height * 0.06,
                      ),
                      Center(
                        child: ClipOval(
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: Container(
                            padding: EdgeInsets.all(width * 0.02),
                            color: Colors.white.withOpacity(0.8),
                            child: Icon(
                              Icons.info_outline,
                              size: width * 0.2,
                              color: Color(0xFF13A89E),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    left: width * 0.04,
                    right: width * 0.04,
                    bottom: width * 0.04),
                child: Html(
                    data: """${widget.termsUndConditions}""",
                    onLinkTap: (url) {
                      print(url);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
