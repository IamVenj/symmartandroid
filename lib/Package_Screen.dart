import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/Cart_Screen.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/productApi.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/item_details.dart';
import 'package:symmart/model/cart.dart';
import 'package:symmart/model/package.dart';

class PackageScreen extends StatefulWidget {
  final toolbarname;
  final setting;
  final packageId;
  PackageScreen(
      {Key key,
      @required this.toolbarname,
      @required this.setting,
      @required this.packageId})
      : super(key: key);

  @override
  _PackageScreenState createState() =>
      _PackageScreenState(toolbarname, packageId);
}

class _PackageScreenState extends State<PackageScreen> {
  var toolbarname;
  var packageId;
  _PackageScreenState(this.toolbarname, this.packageId);

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Package> packageData = [];
  ProductApi packageApi = new ProductApi();
  SharedPreferences localStorage;
  int itemQty = 1;
  int cartCount = 0;

  @override
  initState() {
    getPackage();
    super.initState();
  }

  getPackage() async {
    localStorage = await SharedPreferences.getInstance();
    packageApi.showPackages(packageId).then((package) {
      setState(() {
        packageData.add(package);
      });
    }).catchError((onError) {
      print(onError);
    });
  }

  IconData _backIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.arrow_back;
      case TargetPlatform.iOS:
        return Icons.arrow_back_ios;
    }
    assert(false);
    return null;
  }

  IconData addIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.add_circle;
      case TargetPlatform.iOS:
        return Icons.add_circle;
    }
    assert(false);
    return null;
  }

  IconData subIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.remove_circle;
      case TargetPlatform.iOS:
        return Icons.remove_circle;
    }
    assert(false);
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle descriptionStyle = Theme.of(context).textTheme.subhead;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(_backIcon()),
              alignment: Alignment.centerLeft,
              tooltip: 'Back',
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(toolbarname),
            backgroundColor: Colors.white,
            actions: <Widget>[
              BlocProvider(
                bloc: CartBloc(),
                child: StreamBuilder<int>(
                  stream: CartBloc().outCartCount,
                  initialData: 0,
                  builder: (context, AsyncSnapshot<int> snapshot) {
                    return Stack(
                      children: <Widget>[
                        new FlatButton.icon(
                            padding: EdgeInsets.only(left: width * 0.025),
                            textColor: Color(0xFF13A89E),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(width * 0.1),
                                    bottomLeft: Radius.circular(width * 0.1))),
                            color: Color(0x8013A89E).withOpacity(0.1),
                            icon: new Icon(
                              Icons.shopping_cart,
                            ),
                            label: Container(),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CartScreen(
                                            setting: widget.setting,
                                          )));
                            }),
                        !snapshot.hasData
                            ? new Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  new Icon(Icons.brightness_1,
                                      size: width * 0.05,
                                      color: Color(0xFF13A89E)),
                                  new Container(
                                    child: new Text(
                                      "0",
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              )
                            : new Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  new Icon(Icons.brightness_1,
                                      size: width * 0.06,
                                      color: Color(0xFF13A89E)),
                                  new Container(
                                    child: new Text(
                                      snapshot.data.toString(),
                                      style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                      ],
                    );
                  },
                ),
              ),
            ]),
        body: (packageData.length > 0)
            ? Container(
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                Card(
                  elevation: 1.0,
                  child: Container(
                    color: Colors.white,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 250.0,
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                new Container(
                                  child: new CarouselSlider(
                                    items: packageData.first.images
                                        .map<Widget>((i) {
                                      return Builder(
                                        builder: (BuildContext context) {
                                          return Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 5.0),
                                            child: CachedNetworkImage(
                                              imageUrl: i.imagePublicId,
                                              placeholder: (context, url) =>
                                                  loader(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      empty(),
                                              fit: BoxFit.contain,
                                            ),
                                          );
                                        },
                                      );
                                    }).toList(),
                                    autoPlay: true,
                                    autoPlayCurve: Curves.bounceOut,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ]),
                  ),
                ),
                Card(
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        right: 0,
                        top: 0,
                        child: Container(
                          width: width * 0.3,
                          height: height,
                          decoration: BoxDecoration(
                              color: Color(0xFF13A89E),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.1),
                              )),
                        ),
                      ),
                      Container(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                          child: DefaultTextStyle(
                              style: descriptionStyle,
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        packageData.first.packageName,
                                        textScaleFactor: 0.8,
                                        style: descriptionStyle.copyWith(
                                            fontSize: width * 0.06,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black87),
                                      ),
                                      SizedBox(
                                        height: height * 0.007,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            packageData.first.availability,
                                            textScaleFactor: 0.8,
                                            style:
                                                TextStyle(color: Colors.green),
                                          ),
                                          SizedBox(
                                            width: width * 0.007,
                                          ),
                                          Text(
                                            "Available",
                                            textScaleFactor: 0.8,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                  Text(
                                    packageData.first.discountPrice + " Birr",
                                    textScaleFactor: 0.8,
                                    style: descriptionStyle.copyWith(
                                        fontSize: width * 0.05,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ))),
                    ],
                  ),
                ),
                Container(
                  height: height * 0.2,
                  width: width,
                  padding: EdgeInsets.only(left: width * 0.04),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: packageData.first.packageProduct.length,
                    itemBuilder: (context, int index) {
                      return Container(
                        width: width * 0.4,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ItemDetails(
                                          productId: packageData.first
                                              .packageProduct[index].product.id,
                                          toolbarname: packageData
                                              .first
                                              .packageProduct[index]
                                              .product
                                              .productName,
                                          setting: widget.setting,
                                        )));
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(width * 0.02)),
                            elevation: 2.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Flexible(
                                  child: Container(
                                      height: height * 0.2,
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned(
                                            top: 0,
                                            left: 0,
                                            right: 0,
                                            bottom: 0,
                                            child: CachedNetworkImage(
                                              imageUrl: packageData
                                                  .first
                                                  .packageProduct[index]
                                                  .product
                                                  .productImage
                                                  .first
                                                  .imagePublicId,
                                              placeholder: (context, url) =>
                                                  loader(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      empty(),
                                            ),
                                          ),
                                          Positioned(
                                            top: 0,
                                            left: 0,
                                            right: 0,
                                            bottom: 0,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.black
                                                      .withOpacity(0.4),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          width * 0.02)),
                                            ),
                                          ),
                                          Positioned(
                                              left: width * 0.01,
                                              bottom: width * 0.01,
                                              child: Text(
                                                (packageData
                                                            .first
                                                            .packageProduct[
                                                                index]
                                                            .product
                                                            .productName
                                                            .length >
                                                        18)
                                                    ? packageData
                                                            .first
                                                            .packageProduct[
                                                                index]
                                                            .product
                                                            .productName
                                                            .substring(0, 18) +
                                                        "..."
                                                    : packageData
                                                        .first
                                                        .packageProduct[index]
                                                        .product
                                                        .productName,
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: width * 0.05,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                        ],
                                      )),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Container(
                    margin: EdgeInsets.all(10.0),
                    child: Card(
                        child: Container(
                            padding: const EdgeInsets.fromLTRB(
                                10.0, 20.0, 10.0, 20.0),
                            child: DefaultTextStyle(
                                style: descriptionStyle,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Text(
                                              'Qty',
                                              textScaleFactor: 0.8,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                new IconButton(
                                                  icon: Icon(addIcon(),
                                                      color: Color(0xFF13a89e)),
                                                  onPressed: () {
                                                    setState(() {
                                                      itemQty = itemQty + 1;
                                                    });
                                                  },
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 2.0),
                                                ),
                                                Text(
                                                  itemQty.toString(),
                                                  textScaleFactor: 0.8,
                                                  style:
                                                      descriptionStyle.copyWith(
                                                          fontSize: 20.0,
                                                          color:
                                                              Colors.black87),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      right: 2.0),
                                                ),
                                                new IconButton(
                                                  icon: Icon(subIcon(),
                                                      color: Color(0xFF13a89e)),
                                                  onPressed: () {
                                                    if (itemQty <= 1) {
                                                    } else {
                                                      setState(() {
                                                        itemQty = itemQty - 1;
                                                      });
                                                    }
                                                  },
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: height * 0.02),
                                          child: Container(
                                            height: height * 0.04,
                                            child: RaisedButton.icon(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize.padded,
                                              onPressed: () {
                                                (localStorage.containsKey(
                                                            'user') &&
                                                        localStorage
                                                            .containsKey(
                                                                'token'))
                                                    ? addToCart(
                                                        packageData
                                                            .first.packageName,
                                                        packageData.first
                                                            .discountPrice,
                                                        packageData
                                                            .first
                                                            .images
                                                            .first
                                                            .imagePublicId)
                                                    : showInSnackBar(
                                                        "You are required to login!");
                                              },
                                              color: Color(0xFF13A89E),
                                              label: Text('ADD TO CART',
                                                  textScaleFactor: 0.8,
                                                  style: TextStyle(
                                                      letterSpacing:
                                                          width * 0.007,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white,
                                                      fontSize: width * 0.035)),
                                              textColor: Colors.white,
                                              elevation: 13.0,
                                              highlightElevation: 2.0,
                                              highlightColor: Colors.black,
                                              shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        width * 0.01),
                                              ),
                                              icon: Icon(
                                                Icons.shopping_basket,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ))))),
                Container(
                    width: width,
                    padding: EdgeInsets.only(
                        top: height * 0.01,
                        left: width * 0.04,
                        right: width * 0.04,
                        bottom: height * 0.02),
                    child: Text(
                      packageData.first.slug,
                      textScaleFactor: 0.8,
                      style: Theme.of(context).textTheme.title,
                    )),
              ])))
            : loader());
  }

  addToCart(packageName, packagePrice, packageImage) async {
    var user = await ApiMain().getUser();

    var response = await ClientDatabaseProvider.db.addToCart(
        new CartModel(
            name: packageName,
            userid: user['id'],
            price: double.parse(packagePrice),
            productid: null,
            qty: itemQty,
            color: null,
            size: null,
            img: packageImage,
            associatedmodel: "App\\SpecialPackage",
            packageid: int.parse(widget.packageId)),
        packageName);

    await getCartCount();

    (response == null)
        ? showErrorInSnackBar("Item is already in your cart!")
        : showSuccessInSnackBar("Successfully added to your cart");
  }

  getCartCount() async {
    var userData = await ApiMain().getUser();
    if (userData != null) {
      await ClientDatabaseProvider.db
          .getCartTotalCount(userData['id'])
          .then((v) {
        setState(() {
          cartCount = v;
        });
      });
    } else {
      setState(() {
        cartCount = 0;
      });
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  void showErrorInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Colors.red,
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }
}
