import 'package:symmart/Payment_Screen.dart';
import 'package:flutter/material.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/model/cart.dart';
import 'package:json_table/json_table.dart';
import 'package:symmart/model/setting.dart';

class Checkout extends StatefulWidget {
  final List<CartModel> cartList;
  final userData;
  final totalPrice;
  final List<CompanySettings> setting;

  const Checkout(
      {Key key,
      @required this.cartList,
      @required this.userData,
      @required this.setting,
      @required this.totalPrice})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => CheckOut();
}

class CheckOut extends State<Checkout> {
  var userData;
  var tokenData;

  List<dynamic> iterableCart = [];

  changeCartlistToMap() {
    widget.cartList.forEach((f) {
      iterableCart.add({
        "id": f.id,
        "name": f.name,
        "quantity": f.qty,
        "price": '${f.price} Birr',
        "size": f.size
      });
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _currentRadioIndex = 1;
  Map<String, int> _radioDelivery = {
    "Symmart Standard Delivery": 1,
    "Symmart Express": 2,
    "Symmart Premium": 3,
    "Symmart National": 4
  };

  var deliveryMethod;

  num cartDelivery = 0;
  num cartListQty = 0;

  @override
  void initState() {
    changeCartlistToMap();
    calculateDeliveryFee();
    super.initState();
  }

  IconData _backIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.arrow_back;
      case TargetPlatform.iOS:
        return Icons.arrow_back_ios;
    }
    assert(false);
    return null;
  }

  String toolbarname = 'CheckOut';

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;

    AppBar appBar = AppBar(
      leading: IconButton(
        icon: Icon(_backIcon()),
        alignment: Alignment.centerLeft,
        tooltip: 'Back',
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      elevation: 0,
      title: Text(toolbarname),
      backgroundColor: Colors.white,
    );

    return new Scaffold(
      key: _scaffoldKey,
      appBar: appBar,
      body: Stack(
        children: <Widget>[
          new Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(5.0),
                  child: Card(
                      elevation: 0,
                      child: Container(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(
                                    bottom: height * 0.02, top: height * 0.02),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        alignment: Alignment.center,
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              'Delivery',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black),
                                            ),
                                            SizedBox(
                                              width: width * 0.02,
                                            ),
                                            Icon(
                                              Icons.check_circle,
                                              color: Colors.green,
                                            ),
                                            SizedBox(
                                              width: width * 0.06,
                                            ),
                                          ],
                                        )),
                                    Container(
                                        alignment: Alignment.center,
                                        child: Row(
                                          children: <Widget>[
                                            Text(
                                              'Payment',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black38),
                                            ),
                                            SizedBox(
                                              width: width * 0.02,
                                            ),
                                            Icon(
                                              Icons.play_circle_outline,
                                              color: Colors.black38,
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          )))),
              _verticalDivider(),
              new Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(
                    left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
                child: new Text(
                  'Delivery Address',
                  textScaleFactor: 0.8,
                  style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: width * 0.05),
                ),
              ),
              new Container(
                  height: height * 0.15,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        height: height * 0.15,
                        width: width * 0.96,
                        margin: EdgeInsets.all(7.0),
                        child: Card(
                          elevation: 0.1,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Column(
                                children: <Widget>[
                                  new Container(
                                    margin: EdgeInsets.only(
                                        left: 12.0,
                                        top: 5.0,
                                        right: 0.0,
                                        bottom: 5.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        _verticalDivider(),
                                        BlocProvider(
                                            bloc: UserBloc(),
                                            child: DeliveryAddress()),
                                        _verticalDivider(),
                                        new Container(
                                          margin: EdgeInsets.only(
                                              left: 00.0,
                                              top: 05.0,
                                              right: 0.0,
                                              bottom: 5.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              new Text(
                                                'Delivery Address',
                                                textScaleFactor: 0.8,
                                                style: TextStyle(
                                                    fontSize: width * 0.04,
                                                    color: Colors.black54,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
              _verticalDivider(),
              new Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(
                    left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
                child: new Text(
                  'Delivery Method',
                  textScaleFactor: 0.8,
                  style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: width * 0.05),
                ),
              ),
              Container(
                height: height * 0.17,
                child: GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, childAspectRatio: 3),
                    shrinkWrap: true,
                    children: _radioDelivery.keys
                        .map(
                          (delivery) => Card(
                            elevation: 0,
                            child: Container(
                              height: height * 0.1,
                              width: width,
                              child: new RadioListTile(
                                dense: true,
                                groupValue: _currentRadioIndex,
                                activeColor: Color(0xFF13A89E),
                                value: _radioDelivery[delivery],
                                title: Text(
                                  delivery,
                                  style: TextStyle(fontSize: width * 0.04),
                                  textScaleFactor: 0.8,
                                ),
                                onChanged: (v) {
                                  setState(() {
                                    _currentRadioIndex = v;
                                  });
                                  calculateDeliveryFee();
                                },
                              ),
                            ),
                          ),
                        )
                        .toList()),
              ),
              _verticalDivider(),
              new Container(
                alignment: Alignment.topLeft,
                margin: EdgeInsets.only(
                    left: 12.0, top: 5.0, right: 0.0, bottom: 5.0),
                child: new Text(
                  'Order Summary',
                  textScaleFactor: 0.8,
                  style: TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: width * 0.05),
                ),
              ),
              SingleChildScrollView(
                child: Container(
                    margin: EdgeInsets.only(
                        left: 12.0, top: 5.0, right: 12.0, bottom: 5.0),
                    height: height * 0.26,
                    child: JsonTable(
                      iterableCart,
                      tableHeaderBuilder: (String header) {
                        return Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 4.0),
                          decoration: BoxDecoration(
                              border: Border.all(width: 0.5),
                              color: Colors.grey[300]),
                          child: Text(
                            header,
                            textAlign: TextAlign.center,
                            textScaleFactor: 0.8,
                            style: Theme.of(context)
                                .textTheme
                                .display1
                                .copyWith(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14.0,
                                    color: Colors.black87),
                          ),
                        );
                      },
                      tableCellBuilder: (value) {
                        return Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.0, vertical: 2.0),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 5.0, color: Colors.white)),
                          child: Text(
                            value,
                            textScaleFactor: 0.8,
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .display1
                                .copyWith(
                                    fontSize: 14.0, color: Colors.grey[900]),
                          ),
                        );
                      },
                    )),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
                alignment: Alignment.bottomCenter,
                height: (totalQty()) ? height * 0.172 : height * 0.144,
                child: Card(
                  elevation: 0.0,
                  borderOnForeground: true,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(width * 0.05),
                          topRight: Radius.circular(width * 0.05))),
                  child: Column(
                    children: <Widget>[
                      (!totalQty())
                          ? Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                (totalQty())
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: height * 0.013),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    left: width * 0.02,
                                                    right: width * 0.02),
                                                child: Icon(
                                                  Icons.check_circle_outline,
                                                  color: Colors.green,
                                                  size: width * 0.04,
                                                )),
                                            Text(
                                              'Discount :',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              '${calculateDiscount()} ETB',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black54),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                Padding(
                                  padding: EdgeInsets.only(top: height * 0.013),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: width * 0.02,
                                            right: width * 0.02),
                                        child: Icon(
                                          Icons.directions_bus,
                                          color: Colors.green,
                                          size: width * 0.04,
                                        ),
                                      ),
                                      Text(
                                        'Delivery :',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                            fontSize: width * 0.04,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                      Text(
                                        '$cartDelivery ETB',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                            fontSize: width * 0.04,
                                            color: Colors.black54),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: height * 0.013),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: width * 0.02,
                                            right: width * 0.02),
                                        child: Icon(
                                          Icons.account_balance,
                                          color: Colors.green,
                                          size: width * 0.04,
                                        ),
                                      ),
                                      Text(
                                        'Total :',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                            fontSize: width * 0.04,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                      (!totalQty())
                                          ? Text(
                                              '${totalPrice()} ETB',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black54),
                                            )
                                          : Text(
                                              '${totalPriceWithDiscount()} ETB',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black54),
                                            ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          : Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                (totalQty())
                                    ? Padding(
                                        padding: EdgeInsets.only(
                                            top: height * 0.013),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    left: width * 0.02,
                                                    right: width * 0.02),
                                                child: Icon(
                                                  Icons.check_circle_outline,
                                                  color: Colors.green,
                                                  size: width * 0.04,
                                                )),
                                            Text(
                                              'Discount :',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              '${calculateDiscount()} ETB',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black54),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                Padding(
                                  padding: EdgeInsets.only(top: height * 0.013),
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: width * 0.02,
                                            right: width * 0.02),
                                        child: Icon(
                                          Icons.directions_bus,
                                          color: Colors.green,
                                          size: 16.0,
                                        ),
                                      ),
                                      Text(
                                        'Delivery :',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                            fontSize: width * 0.04,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                      Text(
                                        '$cartDelivery ETB',
                                        textScaleFactor: 0.8,
                                        style: TextStyle(
                                            fontSize: width * 0.04,
                                            color: Colors.black54),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                      (totalQty())
                          ? Padding(
                              padding: EdgeInsets.only(top: height * 0.013),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: width * 0.02,
                                        right: width * 0.02),
                                    child: Icon(
                                      Icons.account_balance,
                                      color: Colors.green,
                                      size: width * 0.04,
                                    ),
                                  ),
                                  Text(
                                    'Total :',
                                    textScaleFactor: 0.8,
                                    style: TextStyle(
                                        fontSize: width * 0.04,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(3.0),
                                  ),
                                  (!totalQty())
                                      ? Text(
                                          '${totalPrice()} ETB',
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              fontSize: width * 0.04,
                                              color: Colors.black54),
                                        )
                                      : Text(
                                          '${totalPriceWithDiscount()} ETB',
                                          textScaleFactor: 0.8,
                                          style: TextStyle(
                                              fontSize: width * 0.04,
                                              color: Colors.black54),
                                        ),
                                  Padding(
                                    padding: EdgeInsets.all(3.0),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                      Padding(
                        padding: EdgeInsets.all(width * 0.02),
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          child: MaterialButton(
                              minWidth: width * 0.98,
                              elevation: 0,
                              color: Color(0x8013A89E).withOpacity(0.2),
                              child: Text(
                                'Confirm Order',
                                textScaleFactor: 0.8,
                                style: TextStyle(fontSize: width * 0.04),
                              ),
                              focusElevation: 0,
                              hoverElevation: 0,
                              highlightElevation: 0,
                              splashColor: Color(0x8013A89E).withOpacity(0.2),
                              highlightColor:
                                  Color(0x8013A89E).withOpacity(0.2),
                              textColor: Color(0xFF13A89E),
                              onPressed: () {
                                (!totalQty())
                                    ? Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => PaymentScreen(
                                                totalPrice: totalPrice(),
                                                subTotal: widget.totalPrice,
                                                setting: widget.setting,
                                                deliveryPrice: cartDelivery,
                                                discountPrice:
                                                    calculateDiscount(),
                                                deliveryMethod: deliveryMethod,
                                                cartList: widget.cartList)))
                                    : Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => PaymentScreen(
                                                totalPrice:
                                                    totalPriceWithDiscount(),
                                                subTotal: widget.totalPrice,
                                                setting: widget.setting,
                                                deliveryPrice: cartDelivery,
                                                discountPrice:
                                                    calculateDiscount(),
                                                deliveryMethod: deliveryMethod,
                                                cartList: widget.cartList)));
                              },
                              shape: new RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              )),
                        ),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }

  totalPriceWithDiscount() {
    return (widget.totalPrice - calculateDiscount()) + cartDelivery;
  }

  totalPrice() {
    return widget.totalPrice + cartDelivery;
  }

  bool totalQty() {
    cartListQty = 0;
    widget.cartList.forEach((f) {
      if (f != null) {
        setState(() {
          cartListQty += f.qty;
        });
      }
    });
    return (cartListQty >=
            int.parse(widget.setting.first.productsAmountForDiscount))
        ? true
        : false;
  }

  calculateDiscount() {
    if (cartListQty >=
        int.parse(widget.setting.first.productsAmountForDiscount)) {
      return (int.parse(widget.setting.first.productsAmountForDiscount) / 100) *
          widget.totalPrice;
    } else {
      return 0;
    }
  }

  calculateDeliveryFee() {
    if (_currentRadioIndex == 1) {
      setState(() {
        cartDelivery = int.parse(widget.setting.first.symmartStandardPrice);
        deliveryMethod = "Symmart Standard Delivery";
      });
    }
    if (_currentRadioIndex == 2) {
      setState(() {
        cartDelivery = int.parse(widget.setting.first.symmartExpressPrice);
        deliveryMethod = "Symmart Express";
      });
    }
    if (_currentRadioIndex == 3) {
      setState(() {
        cartDelivery = int.parse(widget.setting.first.symmartPremiumPrice);
        deliveryMethod = "Symmart Premium";
      });
    }
    if (_currentRadioIndex == 4) {
      setState(() {
        cartDelivery = int.parse(widget.setting.first.symmartNationalPrice);
        deliveryMethod = "Symmart National";
      });
    }
  }

  _verticalDivider() => Container(
        padding: EdgeInsets.all(2.0),
      );
}

class DeliveryAddress extends StatelessWidget {
  const DeliveryAddress({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    UserBloc _userBloc = BlocProvider.of<UserBloc>(context);
    double width = MediaQuery.of(context).size.width;
    return StreamBuilder<Map<String, dynamic>>(
        stream: _userBloc.outUser,
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
          if (snapshot.hasData) {
            return Text(
              '${snapshot.data['addresses'][0]['address']}',
              textScaleFactor: 0.8,
              style: TextStyle(
                  color: Colors.black45,
                  fontSize: width * 0.04,
                  letterSpacing: width * 0.002),
            );
          } else {
            return Container();
          }
        });
  }
}
