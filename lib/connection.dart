import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/OrderApi.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/homeApi.dart';
import 'package:symmart/api/wishApi.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Wish.dart';

class ConnectionScreen extends StatefulWidget {
  const ConnectionScreen({Key key}) : super(key: key);

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Colors.red,
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(color: Colors.yellow, fontSize: 24.0),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => Connection();
}

class Connection extends State<ConnectionScreen> {
  HomeApi homeApi = new HomeApi();
  List<dynamic> newData = [];
  List<MainOrder> orders = [];
  List<Wish> wishes = [];
  Map<String, dynamic> data = {};
  StreamSubscription subscription;
  bool connectionState = false;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  getData() async {
    await homeApi.getHomeData().then((v) {
      for (var item in v) {
        newData.add(item);
      }
      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }, onError: (e) {
      print(e);
    }).catchError((onError) {
      print(onError);
    });
    var token = await ApiMain().getToken();
    await OrderApi().getMainOrders(token).then((onValue) {
      for (var item in onValue) {
        orders.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
    await WishApi().getWishes().then((onValue) {
      for (var item in onValue) {
        wishes.add(item);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  startTime() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    String token = localStorage.getString('token');

    if (localStorage.containsKey('user') && localStorage.containsKey('token')) {
      await homeApi.checkAuth(token).then((onValue) {
        if (!onValue[0]['authStatus']) {
          setState(() {
            localStorage.remove('user');
            localStorage.remove('token');
          });
        }
        if (onValue.length > 0) {
          return checkConnection(onValue[0]['authStatus']);
        } else {
          return checkConnection(null);
        }
      }).catchError((onError) {
        return checkConnection(null);
      });
    } else {
      return checkConnection(null);
    }
  }

  @override
  void initState() {
    startTime();
    super.initState();
  }

  Future checkConnection(bool authStatus) async {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result != ConnectivityResult.none) {
        setState(() {
          connectionState = true;
        });
        await getData();
        if (newData.length > 0) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => Homescreen(
                        authStatus: authStatus,
                        data: data,
                        orders: orders,
                        wishes: wishes,
                      )));
        }
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return new WillPopScope(
      child: MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.white,
          primaryColorDark: Colors.white30,
          accentColor: Colors.blue,
          fontFamily: 'ProductSans',
        ),
        home: new Scaffold(
            key: scaffoldKey,
            body: SafeArea(
                child: Container(
              height: height,
              child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.center,
                  children: <Widget>[
                    Positioned.fill(
                      child: Image.asset(
                        'images/home.jpg',
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    Container(
                      foregroundDecoration:
                          BoxDecoration(color: Colors.white.withOpacity(0.86)),
                    ),
                    errorW(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: height * 0.4,
                        ),
                        Text('SORRY',
                            textScaleFactor: 0.8,
                            style: TextStyle(
                                fontSize: width * 0.15,
                                fontWeight: FontWeight.bold)),
                        Center(
                          child: Text("There is no internet connection",
                              textScaleFactor: 0.8,
                              style: TextStyle(
                                fontSize: width * 0.06,
                              )),
                        ),
                        (connectionState)
                            ? SizedBox(
                                height: height * 0.03,
                              )
                            : Container(),
                        (connectionState)
                            ? Center(
                                child: CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation(Color(0xFF13A89E)),
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ]),
            ))),
      ),
      onWillPop: _willPopCalback,
    );
  }
}

Future<bool> _willPopCalback() async {
  return false;
}

Widget errorW() {
  return Center(
    child: FlareActor(
      "assets/flare/no wifi.flr",
      alignment: Alignment.center,
      fit: BoxFit.contain,
      animation: "init",
    ),
  );
}
