import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:symmart/Cart_Screen.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/api/productApi.dart';
import 'package:symmart/bloc/bloc.dart';
import 'package:symmart/bloc/blocProvider.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/item_details.dart';
import 'package:flutter/material.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/Wish.dart';
import 'package:symmart/model/productList.dart';
import 'package:symmart/service/searchService.dart';

class ItemScreen extends StatefulWidget {
  final String toolbarname;
  final String categoryId;
  final category;
  final userData;
  final setting;
  final wishes;
  ItemScreen(
      {Key key,
      this.toolbarname,
      this.categoryId,
      @required this.userData,
      @required this.setting,
      @required this.wishes,
      @required this.category})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => ItemClass(toolbarname);
}

class ItemClass extends State<ItemScreen> {
  ProductApi productApi = ProductApi();
  List list = ['12', '11'];
  bool loading = false;
  var passingData;

  Map<String, bool> size = {
    'XS': false,
    'S': false,
    'M': false,
    'L': false,
    'XL': false,
    'XXL': false
  };
  var newData = [];
  Map<String, dynamic> data = {};

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String toolbarname;

  var currentPage = 1;
  ItemScreenBloc itemScreenBloc = new ItemScreenBloc();

  ItemClass(this.toolbarname);

  @override
  void initState() {
    getData();
    getCartCount();
    super.initState();
  }

  getData() async {
    await applyFilter(null, null);
  }

  int cartCount = 0;

  getCartCount() async {
    if (widget.userData != null) {
      await ClientDatabaseProvider.db
          .getCartTotalCount(widget.userData['id'])
          .then((v) {
        setState(() {
          cartCount = v;
        });
      });
    } else {
      setState(() {
        cartCount = 0;
      });
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      duration: Duration(seconds: 5),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(40.0))),
    ));
  }

  void showSuccessInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(value),
      backgroundColor: Color(0xFF13a89e),
      elevation: 10.0,
      duration: Duration(seconds: 2),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05),
              topLeft:
                  Radius.circular(MediaQuery.of(context).size.width * 0.05))),
    ));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    IconData _backIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }

    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(_backIcon()),
          alignment: Alignment.centerLeft,
          tooltip: 'Back',
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(toolbarname),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            tooltip: 'Search',
            icon: const Icon(Icons.search),
            onPressed: () async {
              var selected = await showSearch<Product>(
                context: context,
                delegate: SearchService(widget.category),
              );
              if (selected != null) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ItemDetails(
                              productId: selected.id,
                              toolbarname: selected.productName,
                              cartCount: cartCount,
                              setting: widget.setting,
                            )));
              }
            },
          ),
          IconButton(
              tooltip: 'Sort',
              icon: const Icon(Icons.filter_list),
              onPressed: () {
                _showBottomSheet(context);
              }),
          BlocProvider(
            bloc: CartBloc(),
            child: StreamBuilder<int>(
              stream: CartBloc().outCartCount,
              initialData: 0,
              builder: (context, AsyncSnapshot<int> snapshot) {
                return Stack(
                  children: <Widget>[
                    new FlatButton.icon(
                        padding: EdgeInsets.only(left: width * 0.025),
                        textColor: Color(0xFF13A89E),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(width * 0.1),
                                bottomLeft: Radius.circular(width * 0.1))),
                        color: Color(0x8013A89E).withOpacity(0.1),
                        icon: new Icon(
                          Icons.shopping_cart,
                        ),
                        label: Container(),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CartScreen(
                                        setting: widget.setting,
                                      )));
                        }),
                    !snapshot.hasData
                        ? new Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: width * 0.05, color: Color(0xFF13A89E)),
                              new Container(
                                child: new Text(
                                  "0",
                                  textScaleFactor: 0.8,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.034,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        : new Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              new Icon(Icons.brightness_1,
                                  size: width * 0.06, color: Color(0xFF13A89E)),
                              new Container(
                                child: new Text(
                                  snapshot.data.toString(),
                                  textScaleFactor: 0.8,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.034,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
      body: (data.length > 0)
          ? Column(
              children: <Widget>[
                (data['products'].length > 0)
                    ? Expanded(
                        child: TravelDestinationItem(
                          setting: widget.setting,
                          wishes: widget.wishes,
                          showInSnackBar: showInSnackBar,
                          showSuccessInSnackBar: showSuccessInSnackBar,
                          passingData: passingData,
                          itemScreenBloc: itemScreenBloc,
                          initialData: data['products'][0],
                        ),
                      )
                    : Expanded(
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Text(
                                'SORRY',
                                textScaleFactor: 0.8,
                                style: TextStyle(
                                    fontSize: width * 0.1,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: width * 0.01,
                                    decoration: TextDecoration.overline),
                              ),
                              Text(
                                "Products not found!",
                                style: TextStyle(fontSize: width * 0.06),
                              ),
                            ],
                          ),
                        ),
                      )
              ],
            )
          : loader(),
    );
  }

  bool a = true;
  String mText = "Press to hide";

  int radioValue = 0;
  bool switchValue = false;

  void handleRadioValueChanged(int value) {
    setState(() {
      radioValue = value;
    });
  }

  RangeValues _rangeValues = RangeValues(1, 50000);
  List checkColorValue = [];
  List checkVendorValue = [];
  List checkSizeValue = [];
  String dropdownValue = 'Latest';

  void _showBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        elevation: 1.0,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return Container(
                color: Color(0xFF737373),
                height: MediaQuery.of(context).size.height * 0.9,
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.9,
                  child: _buildBottomNavMenu(setState),
                  decoration: BoxDecoration(
                      color: Theme.of(context).canvasColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                ),
              );
            },
          );
        });
  }

  void onColorCheckboxSelected(bool selected, selectedId, setModalState) {
    if (selected == true) {
      setState(() {
        checkColorValue.add(selectedId);
      });
      setModalState(() {
        checkColorValue.add(selectedId);
      });
    } else {
      setState(() {
        checkColorValue.remove(selectedId);
      });
      setModalState(() {
        checkColorValue.remove(selectedId);
      });
    }
  }

  void onVendorCheckboxSelected(bool selected, selectedId, setModalState) {
    if (selected == true) {
      setState(() {
        checkVendorValue.add(selectedId);
      });
      setModalState(() {
        checkVendorValue.add(selectedId);
      });
    } else {
      setState(() {
        checkVendorValue.remove(selectedId);
      });
      setModalState(() {
        checkVendorValue.remove(selectedId);
      });
    }
  }

  void onSizeCheckboxSelected(bool selected, key, setModalState) {
    if (selected == true) {
      setState(() {
        checkSizeValue.add(key);
      });
      setModalState(() {
        checkSizeValue.add(key);
      });
    } else {
      setState(() {
        checkSizeValue.remove(key);
      });
      setModalState(() {
        checkSizeValue.remove(key);
      });
    }
  }

  Column _buildBottomNavMenu(setModalState) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: height * 0.01, bottom: height * 0.005),
          child: Center(
            child: Text(
              'FILTER',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: width * 0.05,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2),
            ),
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04),
          child: Text(
            'Sort By',
            style: TextStyle(fontSize: width * 0.04),
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
          child: DropdownButton<String>(
            value: dropdownValue,
            icon: Icon(Icons.sort),
            iconSize: width * 0.04,
            elevation: 1,
            style: TextStyle(color: Colors.black),
            underline: Container(
              height: height * 0.001,
              width: width,
              color: Color(0xFF13A89E),
            ),
            isExpanded: true,
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
              setModalState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>['Latest', 'Price: High - Low', 'Price: Low - High']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04),
          child: Text(
            'Filter by price',
            style: TextStyle(fontSize: width * 0.04),
          ),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04, right: width * 0.04),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(_rangeValues.start.toStringAsFixed(2).toString() + ' Birr'),
              Text(_rangeValues.end.toStringAsFixed(2).toString() + ' Birr'),
            ],
          ),
        ),
        RangeSlider(
          min: 1,
          max: 50000,
          labels: RangeLabels('${_rangeValues.start}', '${_rangeValues.end}'),
          values: _rangeValues,
          activeColor: Color(0xFF13A89E),
          inactiveColor: Color(0x8013A89E),
          onChanged: (RangeValues newValue) {
            setState(() => _rangeValues = newValue);
            setModalState(() => _rangeValues = newValue);
          },
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04),
          child: Text(
            'Filter by color',
            style: TextStyle(fontSize: width * 0.04),
          ),
        ),
        Divider(),
        (data.length > 0)
            ? SingleChildScrollView(
                child: Container(
                  height: height * 0.12,
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 12,
                    ),
                    shrinkWrap: true,
                    itemCount: data['colors'].length,
                    itemBuilder: (BuildContext context, int i) {
                      return Padding(
                        padding: EdgeInsets.all(width * 0.013),
                        child: Container(
                          child: Checkbox(
                            onChanged: (bool value) {
                              onColorCheckboxSelected(
                                  value, data['colors'][i].id, setModalState);
                            },
                            value:
                                checkColorValue.contains(data['colors'][i].id),
                            activeColor: Colors.white,
                            checkColor: Color(0xFF13A89E),
                          ),
                          height: height * 0.04,
                          decoration: BoxDecoration(
                              color: Color(int.parse("0xFF" +
                                  "${data['colors'][i].hex}".substring(1, 7)))),
                        ),
                      );
                    },
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation(Color(0xFF13A89)),
                ),
              ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04),
          child: Text(
            'Filter by size',
            style: TextStyle(fontSize: width * 0.04),
          ),
        ),
        Divider(),
        Container(
          height: height * 0.06,
          child: SingleChildScrollView(
              child: GridView(
            shrinkWrap: true,
            children: size.keys.map((key) {
              return Row(
                children: <Widget>[
                  Checkbox(
                    onChanged: (bool value) {
                      setState(() {
                        onSizeCheckboxSelected(value, key, setModalState);
                      });
                      setModalState(() {
                        onSizeCheckboxSelected(value, key, setModalState);
                      });
                    },
                    value: checkSizeValue.contains(key),
                    activeColor: Colors.white,
                    checkColor: Color(0xFF13A89E),
                  ),
                  Text("$key", style: TextStyle(fontSize: width * 0.03))
                ],
              );
            }).toList(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, childAspectRatio: 6),
          )),
        ),
        Divider(),
        Padding(
          padding: EdgeInsets.only(left: width * 0.04),
          child: Text(
            'Filter by Vendor',
            style: TextStyle(fontSize: width * 0.04),
          ),
        ),
        Divider(),
        (data.length > 0)
            ? SingleChildScrollView(
                child: Container(
                  height: height * 0.1,
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2, childAspectRatio: 6),
                    shrinkWrap: true,
                    itemCount: data['vendors'].length,
                    itemBuilder: (BuildContext context, int i) {
                      return Row(
                        children: <Widget>[
                          Checkbox(
                            onChanged: (bool value) {
                              onVendorCheckboxSelected(
                                  value, data['vendors'][i].id, setModalState);
                            },
                            value: checkVendorValue
                                .contains(data['vendors'][i].id),
                            activeColor: Colors.white,
                            checkColor: Color(0xFF13A89E),
                          ),
                          Flexible(child: Text(data['vendors'][i].shopName))
                        ],
                      );
                    },
                  ),
                ),
              )
            : Center(
                child: CircularProgressIndicator(
                  strokeWidth: 1.0,
                  valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                ),
              ),
        Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: width * 0.01, right: width * 0.01),
              child: MaterialButton(
                onPressed: loading
                    ? null
                    : () {
                        applyFilter(setModalState, context);
                      },
                elevation: 1.0,
                minWidth: width * 0.48,
                color: Colors.white,
                child: !loading
                    ? Text('Apply')
                    : CircularProgressIndicator(
                        strokeWidth: 1.0,
                        valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                      ),
                disabledElevation: 4.0,
                disabledColor: Colors.white,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: width * 0.01, right: width * 0.01),
              child: MaterialButton(
                onPressed: loading
                    ? null
                    : () {
                        resetFilter(setModalState);
                      },
                elevation: 1.0,
                minWidth: width * 0.48,
                color: Colors.black,
                disabledColor: Colors.black38,
                child: Text(
                  'Clear',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  applyFilter(setModalState, BuildContext context) async {
    if (setModalState != null) {
      setModalState(() {
        loading = true;
      });
    }

    passingData = {
      "startprice": _rangeValues.start,
      "endprice": _rangeValues.end,
      "size": checkSizeValue,
      "color": checkColorValue,
      "vendor": checkVendorValue,
      "category_Id": int.parse(widget.categoryId),
      "sort": dropdownValue
    };

    print("Start==${_rangeValues.start}|end==${_rangeValues.end}");
    await productApi
        .productsBasedOnCategory(passingData, currentPage)
        .then((v) async {
      if (setModalState != null) {
        setModalState(() {
          loading = false;
        });
      }

      for (var item in v) {
        setState(() {
          newData.add(item);
        });
        if (setModalState != null) {
          setModalState(() {
            newData.add(item);
          });
        }
      }

      newData.forEach((f) {
        f.forEach((k, v) {
          data[k] = v;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
    if (context != null) {
      Navigator.of(context).pop();
    }
    itemScreenBloc.itemScreenProductController.sink.add(data['products'][0]);
  }

  resetFilter(setModalState) {
    setState(() {
      _rangeValues = RangeValues(1, 50000);
      checkColorValue = [];
      checkVendorValue = [];
      checkSizeValue = [];
    });
    setModalState(() {
      _rangeValues = RangeValues(1, 50000);
      checkColorValue = [];
      checkVendorValue = [];
      checkSizeValue = [];
    });
  }
}

enum ProductLoadMoreStatus { LOADING, STABLE }

class TravelDestinationItem extends StatefulWidget {
  final ShapeBorder shape;
  final setting;
  final showInSnackBar;
  final showSuccessInSnackBar;
  final wishes;
  final ItemScreenBloc itemScreenBloc;
  final Map<String, dynamic> passingData;
  final initialData;
  TravelDestinationItem(
      {Key key,
      this.shape,
      @required this.setting,
      @required this.showInSnackBar,
      @required this.showSuccessInSnackBar,
      @required this.wishes,
      @required this.passingData,
      @required this.itemScreenBloc,
      @required this.initialData})
      : super(key: key);

  @override
  _TravelDestinationItemState createState() =>
      _TravelDestinationItemState(initialData, itemScreenBloc, setting, wishes);
}

class _TravelDestinationItemState extends State<TravelDestinationItem> {
  ProductLoadMoreStatus loadMoreStatus = ProductLoadMoreStatus.STABLE;
  final ScrollController scrollController = new ScrollController();
  var setting;
  List fav = [];
  List<Wish> wishes = [];
  List<Product> product = [];
  int currentPageNumber;
  ItemScreenBloc itemScreenBloc;
  ProductData initialData;

  @override
  void initState() {
    currentPageNumber = initialData.page;
    product = initialData.product;
    favorites(null);
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  _TravelDestinationItemState(
      this.initialData, this.itemScreenBloc, this.setting, this.wishes);

  addWish(productId, List<Wish> wishes, index) async {
    var token = await ApiMain().getToken();
    if (token != null) {
      widget.showInSnackBar("Please wait for a moment..");
      setState(() {
        fav[index] = true;
      });
      var passedData = {"product_id": productId};
      var response = await ApiMain().postData(passedData, '/add-wish', token);

      if (response.statusCode == 200) {
        var body = json.decode(response.body);
        Wish wish = Wish.fromJson(body['wish']);
        wishes.add(wish);
      }
      await favorites(null);
      widget.showSuccessInSnackBar("Successfully added to your wishlist");
    } else {
      widget.showInSnackBar("your are required to Login to add wishes!");
    }
  }

  removeWish(productId, index) async {
    widget.showInSnackBar("Please wait for a moment..");
    setState(() {
      fav[index] = false;
    });
    var token = await ApiMain().getToken();
    var response = await ApiMain().deleteData(productId, '/wish/remove', token);

    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      if (body["status"] == 1) {
        wishes.removeWhere((test) {
          return int.parse(test.product.id) == int.parse(productId);
        });
        await favorites(productId);
      }
      widget.showSuccessInSnackBar("successfully removed from your wish list");
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle descriptionStyle = theme.textTheme.subhead;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double _safeAreaHorizontal = MediaQuery.of(context).padding.left +
        MediaQuery.of(context).padding.right;
    double safeBlockHorizontal = (width - _safeAreaHorizontal) / 100;
    return StreamBuilder<ProductData>(
        initialData: initialData,
        stream: itemScreenBloc.itemScreenProductController.stream,
        builder: (context, AsyncSnapshot<ProductData> snapshot) {
          if (snapshot.data.product.isNotEmpty) {
            product = snapshot.data.product;
            return NotificationListener(
              onNotification: onNotification,
              child: GridView.builder(
                controller: scrollController,
                physics: AlwaysScrollableScrollPhysics(),
                padding: const EdgeInsets.all(4.0),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: (width / (height - height * 0.3))),
                itemCount: snapshot.data.product.length,
                itemBuilder: (BuildContext context, int index) {
                  return SafeArea(
                      top: true,
                      bottom: false,
                      child: Container(
                          padding: const EdgeInsets.all(4.0),
                          height: height,
                          width: width * 0.645,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ItemDetails(
                                            productId:
                                                snapshot.data.product[index].id,
                                            toolbarname: snapshot.data
                                                .product[index].productName,
                                            setting: setting,
                                          )));
                            },
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(width * 0.05),
                                      bottomRight:
                                          Radius.circular(width * 0.05),
                                      bottomLeft:
                                          Radius.circular(width * 0.05))),
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              borderOnForeground: true,
                              semanticContainer: true,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    height: height * 0.17,
                                    child: Stack(
                                      children: <Widget>[
                                        (loadMoreStatus != null &&
                                                loadMoreStatus ==
                                                    ProductLoadMoreStatus
                                                        .LOADING)
                                            ? LinearProgressIndicator()
                                            : Container(),
                                        Positioned.fill(
                                          child: CachedNetworkImage(
                                            imageUrl: snapshot
                                                .data
                                                .product[index]
                                                .productImage
                                                .first
                                                .imagePublicId,
                                            placeholder: (context, url) =>
                                                loader(),
                                            errorWidget:
                                                (context, url, error) =>
                                                    empty(),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        (snapshot.data.product[index]
                                                    .oldPrice !=
                                                null)
                                            ? Container(
                                                alignment: Alignment.topRight,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      color: Color(0xFF13A89E),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      width *
                                                                          0.04))),
                                                  padding: EdgeInsets.all(
                                                      width * 0.02),
                                                  child: Text(
                                                    getPercentage(
                                                        snapshot
                                                            .data
                                                            .product[index]
                                                            .currentPrice,
                                                        snapshot
                                                            .data
                                                            .product[index]
                                                            .oldPrice),
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: width * 0.05,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                              )
                                            : Container(),
                                        (wishes.isNotEmpty)
                                            ? Stack(
                                                children: <Widget>[
                                                  !fav[index]
                                                      ? Positioned(
                                                          top: 0,
                                                          left: 0,
                                                          child: Container(
                                                            width: width * 0.23,
                                                            height:
                                                                height * 0.04,
                                                            child:
                                                                FlatButton.icon(
                                                              icon: Icon(
                                                                Icons
                                                                    .favorite_border,
                                                                size: width *
                                                                    0.06,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                              onPressed:
                                                                  () async {
                                                                await addWish(
                                                                    snapshot
                                                                        .data
                                                                        .product[
                                                                            index]
                                                                        .id,
                                                                    wishes,
                                                                    index);
                                                              },
                                                              label:
                                                                  Container(),
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius
                                                                              .only(
                                                                topRight: Radius
                                                                    .circular(
                                                                        width),
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        width),
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        width),
                                                              )),
                                                              color: Color(
                                                                  0xFF13A89E),
                                                              // tooltip: 'Add to wish',
                                                            ),
                                                          ),
                                                        )
                                                      : Positioned(
                                                          top: 0,
                                                          left: 0,
                                                          child:
                                                              FlatButton.icon(
                                                            icon: Icon(
                                                              Icons.favorite,
                                                              size:
                                                                  width * 0.06,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            onPressed:
                                                                () async {
                                                              await removeWish(
                                                                  snapshot
                                                                      .data
                                                                      .product[
                                                                          index]
                                                                      .id,
                                                                  index);
                                                            },
                                                            label: Container(),
                                                            shape:
                                                                RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                              topRight: Radius
                                                                  .circular(
                                                                      width),
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      width),
                                                              bottomRight: Radius
                                                                  .circular(
                                                                      width),
                                                            )),
                                                            color: Color(
                                                                0xFF13A89E),
                                                            // tooltip: 'Add to wish',
                                                          ),
                                                        )
                                                ],
                                              )
                                            : !fav[index]
                                                ? Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: FlatButton.icon(
                                                      icon: Icon(
                                                        Icons.favorite_border,
                                                        size: width * 0.06,
                                                        color: Colors.white,
                                                      ),
                                                      onPressed: () async {
                                                        await addWish(
                                                            snapshot
                                                                .data
                                                                .product[index]
                                                                .id,
                                                            wishes,
                                                            index);
                                                      },
                                                      label: Container(),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                        topRight:
                                                            Radius.circular(
                                                                width),
                                                        bottomLeft:
                                                            Radius.circular(
                                                                width),
                                                        bottomRight:
                                                            Radius.circular(
                                                                width),
                                                      )),
                                                      color: Color(0xFF13A89E),
                                                      // tooltip: 'Add to wish',
                                                    ),
                                                  )
                                                : Positioned(
                                                    top: 0,
                                                    left: 0,
                                                    child: FlatButton.icon(
                                                      icon: Icon(
                                                        Icons.favorite,
                                                        size: width * 0.06,
                                                        color: Colors.white,
                                                      ),
                                                      onPressed: () async {
                                                        await removeWish(
                                                            snapshot
                                                                .data
                                                                .product[index]
                                                                .id,
                                                            index);
                                                      },
                                                      label: Container(),
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                        topRight:
                                                            Radius.circular(
                                                                width),
                                                        bottomLeft:
                                                            Radius.circular(
                                                                width),
                                                        bottomRight:
                                                            Radius.circular(
                                                                width),
                                                      )),
                                                      color: Color(0xFF13A89E),
                                                      // tooltip: 'Add to wish',
                                                    ),
                                                  )
                                      ],
                                    ),
                                  ),
                                  Divider(),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(width * 0.02,
                                          height * 0.01, width * 0.015, 0.0),
                                      child: DefaultTextStyle(
                                        style: descriptionStyle,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      bottom: height * 0.005,
                                                      right: width * 0.02),
                                                  child: Text(
                                                    snapshot.data.product[index]
                                                            .currentPrice +
                                                        ' Birr',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        color: Colors.black87,
                                                        fontSize:
                                                            safeBlockHorizontal *
                                                                4.5,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                  ),
                                                ),
                                                (snapshot.data.product[index]
                                                            .oldPrice !=
                                                        null)
                                                    ? Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: height *
                                                                    0.005),
                                                        child: Text(
                                                          snapshot
                                                                  .data
                                                                  .product[
                                                                      index]
                                                                  .oldPrice +
                                                              ' Birr',
                                                          textScaleFactor: 0.8,
                                                          style: descriptionStyle.copyWith(
                                                              color: Color(
                                                                  0xFF13A89E),
                                                              fontSize:
                                                                  safeBlockHorizontal *
                                                                      3.5,
                                                              decoration:
                                                                  TextDecoration
                                                                      .lineThrough),
                                                        ),
                                                      )
                                                    : Container(),
                                              ],
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  bottom: height * 0.008),
                                              child: Text(
                                                (snapshot
                                                            .data
                                                            .product[index]
                                                            .productName
                                                            .length >
                                                        30)
                                                    ? snapshot
                                                            .data
                                                            .product[index]
                                                            .productName
                                                            .substring(0, 30) +
                                                        ' ...'
                                                    : snapshot
                                                        .data
                                                        .product[index]
                                                        .productName,
                                                textScaleFactor: 0.8,
                                                style: descriptionStyle.copyWith(
                                                    color: Colors.black87,
                                                    fontSize:
                                                        safeBlockHorizontal *
                                                            4.0,
                                                    fontWeight:
                                                        FontWeight.w700),
                                              ),
                                            ),
                                            Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Text(
                                                  'Sold by: ',
                                                  textScaleFactor: 0.8,
                                                  style: TextStyle(
                                                      color: Colors.black87,
                                                      fontSize:
                                                          safeBlockHorizontal *
                                                              3.4),
                                                ),
                                                Text(
                                                  (snapshot
                                                              .data
                                                              .product[index]
                                                              .user
                                                              .shopName
                                                              .length >
                                                          13)
                                                      ? snapshot
                                                              .data
                                                              .product[index]
                                                              .user
                                                              .shopName
                                                              .substring(
                                                                  0, 13) +
                                                          ' ...'
                                                      : snapshot
                                                          .data
                                                          .product[index]
                                                          .user
                                                          .shopName,
                                                  textScaleFactor: 0.8,
                                                  softWrap: true,
                                                  style: descriptionStyle.copyWith(
                                                      color: Colors.black87,
                                                      fontSize:
                                                          safeBlockHorizontal *
                                                              3.4,
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )));
                },
              ),
            );
          } else {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    'SORRY',
                    textScaleFactor: 0.8,
                    style: TextStyle(
                        fontSize: width * 0.1,
                        fontWeight: FontWeight.bold,
                        letterSpacing: width * 0.01,
                        decoration: TextDecoration.overline),
                  ),
                  Text(
                    "Products not found!",
                    textScaleFactor: 0.8,
                    style: TextStyle(fontSize: width * 0.06),
                  ),
                ],
              ),
            );
          }
        });
  }

  favorites(wishId) async {
    List d = [];
    List w = [];
    fav.clear();

    if (product.isNotEmpty) {
      for (var prod in product) {
        d.add(prod.id);
      }
    }

    if (wishes.isNotEmpty) {
      if (wishes.first.responseCode != "401") {
        for (var item in wishes) {
          w.add(item.product.id);
        }
      }
    }

    if (wishId != null) {
      w.remove(wishId);
    }

    d.forEach((f) {
      if (w.isNotEmpty) {
        w.contains(f) ? fav.add(true) : fav.add(false);
      } else {
        fav.add(false);
      }
    });

    return fav;
  }

  bool onNotification(Notification notification) {
    // print(product.length != initialData.product.length);
    // if (product.length != initialData.product.length) {
    //   product.addAll(initialData.product);
    // }
    if (notification is ScrollUpdateNotification) {
      if (scrollController.position.maxScrollExtent > scrollController.offset &&
          scrollController.position.maxScrollExtent - scrollController.offset <=
              50) {
        if (loadMoreStatus != null &&
            loadMoreStatus == ProductLoadMoreStatus.STABLE) {
          loadMoreStatus = ProductLoadMoreStatus.LOADING;
          ProductData blocData;
          ProductApi()
              .productsBasedOnCategory(
                  widget.passingData, currentPageNumber + 1)
              .then((v) async {
            for (var item in v) {
              currentPageNumber = item['products'].page;
              setState(() {
                product.addAll(item['products'].product);
                blocData = item['products'];
              });
            }
            itemScreenBloc.itemScreenProductController.sink.add(blocData);
            loadMoreStatus = ProductLoadMoreStatus.STABLE;
          }).catchError((onError) {
            print(onError);
          });
        }
      }
    }
    return true;
  }
}

String getPercentage(String currentPrice, String oldPrice) {
  double percentage =
      ((int.parse(currentPrice) / int.parse(oldPrice)) * 100) - 100;
  return percentage.toStringAsFixed(1) + '%';
}

Widget loader() {
  return FlareActor(
    "assets/flare/loader.flr",
    alignment: Alignment.center,
    fit: BoxFit.contain,
    animation: "Loading",
  );
}

Widget errorW() {
  return Center(
    child: FlareActor(
      "assets/flare/no wifi.flr",
      alignment: Alignment.center,
      fit: BoxFit.contain,
      animation: "init",
    ),
  );
}
