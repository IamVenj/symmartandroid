import 'package:symmart/model/Color.dart';
import 'package:symmart/model/MainOrder.dart';
import 'package:symmart/model/Product.dart';
import 'package:symmart/model/package.dart';
import 'package:symmart/model/user.dart';

class Order {
  Package package;
  Product product;
  User vendor;
  String orderCode;
  String quantity;
  ColorClass color;
  String paidAmount;
  MainOrder mainOrder;
  String id;

  Order(this.package, this.product, this.color, this.mainOrder, this.orderCode,
      this.paidAmount, this.quantity, this.vendor, this.id);

  Order.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.color = (jsonObject['color'] != null)
        ? ColorClass.fromJson(jsonObject['color'])
        : null;
    this.mainOrder = (jsonObject['main_order_id'] != null)
        ? MainOrder.fromJson(jsonObject['main_order_id'])
        : null;
    this.orderCode =
        (jsonObject['order_code'] != null) ? jsonObject['order_code'] : null;
    this.package = (jsonObject['package'] != null)
        ? Package.fromJson(jsonObject['package'])
        : null;
    this.product = (jsonObject['product'] != null)
        ? Product.fromJson(jsonObject['product'])
        : null;
    this.paidAmount = jsonObject['paid_amount'].toString();
    this.quantity = jsonObject['quantity'].toString();
    this.vendor = (jsonObject['vendor'] != null)
        ? User.fromJson(jsonObject['vendor'])
        : null;
  }
}
