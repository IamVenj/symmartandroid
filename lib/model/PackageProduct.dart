import 'package:symmart/model/Product.dart';

class PackageProduct {
  String id;
  String packageId;
  String productId;
  Product product;

  PackageProduct(this.id, this.packageId, this.product, this.productId);

  PackageProduct.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.packageId = jsonObject['package_id'].toString();
    this.product = Product.fromJson(jsonObject['product']);
  }
}
