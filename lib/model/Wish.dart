import 'package:symmart/model/Product.dart';
import 'package:symmart/model/user.dart';

class Wish {
  String id;
  Product product;
  User user;
  String responseCode;

  Wish(this.id, this.product, this.user, this.responseCode);

  Wish.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.user =
        (jsonObject['user'] != null) ? User.fromJson(jsonObject['user']) : null;
    this.product = (jsonObject['product'] != null)
        ? Product.fromJson(jsonObject['product'])
        : null;
    this.responseCode = (jsonObject['responseCode'] != null)
        ? jsonObject['responseCode']
        : null;
  }
}
