class ProductImage {
  String id;
  String imagePublicId;
  String imageVersion;

  ProductImage(this.id, this.imagePublicId, this.imageVersion);

  ProductImage.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.imagePublicId = jsonObject['image_public_id'].toString();
    this.imageVersion = jsonObject['image_version'].toString();
  }
}
