class ProductSize {
  String id;
  String productId;
  String size;

  ProductSize(this.id, this.productId, this.size);

  ProductSize.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.productId = jsonObject['product_id'].toString();
    this.size = jsonObject['size'].toString();
  }
}
