import 'package:symmart/model/Color.dart';

class ProductColor {
  String id;
  String colorId;
  String productId;
  List<ColorClass> color;

  ProductColor(this.id, this.color, this.colorId, this.productId);

  ProductColor.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.color = [];
    for (var item in jsonObject['colors']) {
      this.color.add(ColorClass.fromJson(item));
    }
    this.colorId = jsonObject['color_id'].toString();
    this.productId = jsonObject['product_id'].toString();
  }
}
