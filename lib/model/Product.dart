import 'package:symmart/model/ProductColor.dart';
import 'package:symmart/model/ProductImage.dart';
import 'package:symmart/model/ProductSize.dart';
import 'package:symmart/model/category.dart';
import 'package:symmart/model/user.dart';

class Product {
  String id;
  String productName;
  User user;
  String slug;
  CategoryMain category;
  String brand;
  String currentPrice;
  String oldPrice;
  String productLocation;
  String availability;
  String sku;
  String orders;
  String visits;
  List<ProductColor> productColors;
  List<ProductImage> productImage;
  List<ProductSize> productSize;

  Product(
      this.id,
      this.productName,
      this.availability,
      this.brand,
      this.category,
      this.currentPrice,
      this.oldPrice,
      this.orders,
      this.productColors,
      this.productImage,
      this.productLocation,
      this.productSize,
      this.sku,
      this.slug,
      this.user,
      this.visits);

  Product.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.productName = jsonObject['product_name'].toString();
    this.availability = jsonObject['availability'].toString();
    this.brand = jsonObject['brand'].toString();
    this.category = (jsonObject['category'] != null)
        ? CategoryMain.fromJson(jsonObject['category'])
        : null;
    this.currentPrice = jsonObject['current_price'].toString();
    this.oldPrice = (jsonObject['old_price'] != null)
        ? jsonObject['old_price'].toString()
        : null;
    this.orders = jsonObject['orders'].toString();
    this.productColors = [];
    if (jsonObject['product_colors'] != null) {
      for (var item in jsonObject['product_colors']) {
        this.productColors.add(ProductColor.fromJson(item));
      }
    }
    this.productImage = [];
    if (jsonObject['product_image'] != null) {
      for (var item in jsonObject['product_image']) {
        this.productImage.add(ProductImage.fromJson(item));
      }
    }
    this.productLocation = jsonObject['product_location'].toString();
    this.productSize = [];
    if (jsonObject['product_size'] != null) {
      for (var item in jsonObject['product_size']) {
        this.productSize.add(ProductSize.fromJson(item));
      }
    }

    this.sku = jsonObject['sku'].toString();
    this.slug = jsonObject['slug'].toString();
    this.user =
        (jsonObject['user'] != null) ? User.fromJson(jsonObject['user']) : null;
    this.visits = jsonObject['visits'].toString();
  }
}
