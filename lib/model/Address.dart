class Address {
  String id;
  String address;
  String longitude;
  String latitude;

  Address(this.id, this.address, this.latitude, this.longitude);

  Address.fromJson(Map<String, dynamic> jsonObject) {
    this.id = (jsonObject['id'] != null) ? jsonObject['id'].toString() : "";
    this.address =
        (jsonObject['address'] != null) ? jsonObject['address'].toString() : "";
    this.latitude = (jsonObject['latitude'] != null)
        ? jsonObject['latitude'].toString()
        : "";
    this.longitude = (jsonObject['longitude'] != null)
        ? jsonObject['longitude'].toString()
        : "";
  }
}
