import 'package:symmart/model/PackageProduct.dart';
import 'package:symmart/model/ProductImage.dart';
import 'package:symmart/model/user.dart';

class Package {
  String id;
  String packageName;
  String discountPrice;
  String slug;
  String availability;
  String discountPercent;
  User user;
  List<PackageProduct> packageProduct;
  List<ProductImage> images;

  Package(
      this.availability,
      this.discountPrice,
      this.id,
      this.images,
      this.packageName,
      this.packageProduct,
      this.slug,
      this.user,
      this.discountPercent);

  Package.fromJson(Map<String, dynamic> jsonObject) {
    this.availability = (jsonObject['availability'] != null)
        ? jsonObject['availability']
        : null;
    this.discountPrice = (jsonObject['discount_price'] != null)
        ? jsonObject['discount_price'].toString()
        : null;
    this.id = (jsonObject['id'] != null) ? jsonObject['id'].toString() : null;
    this.discountPercent = (jsonObject['discount_percent'] != null)
        ? jsonObject['discount_percent']
        : null;
    this.images = [];
    if (jsonObject['images'] != null) {
      for (var item in jsonObject['images']) {
        this.images.add(ProductImage.fromJson(item));
      }
    }

    this.packageName = (jsonObject['package_name'] != null)
        ? jsonObject['package_name']
        : null;
    this.packageProduct = [];
    if (jsonObject['package_product'] != null) {
      for (var item in jsonObject['package_product']) {
        this.packageProduct.add(PackageProduct.fromJson(item));
      }
    }
    this.slug = (jsonObject['slug'] != null) ? jsonObject['slug'] : null;
    this.user =
        (jsonObject['user'] != null) ? User.fromJson(jsonObject['user']) : null;
  }
}
