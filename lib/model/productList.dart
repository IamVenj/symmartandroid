import 'package:symmart/model/Product.dart';

class ProductData {
  int page;
  int totalResults;
  List<Product> product;
  int totalPages;

  ProductData(
    this.page,
    this.product,
    this.totalResults,
    this.totalPages
  );

  ProductData.fromJson(Map<String, dynamic> jsonObject){
    this.page = jsonObject['current_page'];
    this.product = [];
    for (var item in jsonObject['data']) {
      this.product.add(Product.fromJson(item));
    }
    this.totalPages = jsonObject['total'];
    this.totalResults = jsonObject['data'].length;
  } 
}