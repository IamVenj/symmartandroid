import 'package:symmart/model/Address.dart';
import 'package:symmart/model/Wish.dart';

class User {
  String id;
  String name;
  String shopName;
  String email;
  String password;
  String role;
  String phoneNumber;
  String phoneNumber2;
  String phoneNumber3;
  String aboutShop;
  String photoUrl;
  String imageVersion;
  String activation;
  String businessType;
  List<Address> addresses;
  List<Wish> wish;

  User(
      this.aboutShop,
      this.activation,
      this.businessType,
      this.email,
      this.id,
      this.imageVersion,
      this.name,
      this.password,
      this.phoneNumber,
      this.phoneNumber2,
      this.phoneNumber3,
      this.photoUrl,
      this.role,
      this.shopName,
      this.addresses,
      this.wish);

  User.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.aboutShop = (jsonObject['about_shop'] != null)
        ? jsonObject['about_shop'].toString()
        : null;
    this.activation = jsonObject['activation'].toString();
    this.addresses = [];
    if (jsonObject['addresses'] != null) {
      for (var item in jsonObject['addresses']) {
        this.addresses.add(Address.fromJson(item));
      }
    }
    this.businessType = jsonObject['business_type'].toString();
    this.email = jsonObject['email'].toString();
    this.imageVersion = (jsonObject['image_version'] != null)
        ? jsonObject['image_version'].toString()
        : null;
    this.name =
        (jsonObject['name'] != null) ? jsonObject['name'].toString() : null;
    this.phoneNumber = jsonObject['phone_number'].toString();
    this.phoneNumber2 = jsonObject['phone_number2'].toString();
    this.phoneNumber3 = jsonObject['phone_number3'].toString();
    this.photoUrl = (jsonObject['photo_url'] != null)
        ? jsonObject['photo_url'].toString()
        : null;
    this.role = jsonObject['role'].toString();
    this.shopName = (jsonObject['shop_name'] != null)
        ? jsonObject['shop_name'].toString()
        : null;
    this.wish = [];
    if (jsonObject['wish'] != null) {
      for (var item in jsonObject['wish']) {
        this.wish.add(Wish.fromJson(item));
      }
    }
  }
}
