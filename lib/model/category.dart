class CategoryMain {
  String id;
  String categoryName;
  String imagePublicId;
  String imageVersion;
  String imageUrl;
  String parentId;
  List<CategoryMain> parent;

  CategoryMain(this.id, this.categoryName, this.imagePublicId,
      this.imageVersion, this.imageUrl, this.parentId, this.parent);

  CategoryMain.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.categoryName = jsonObject['category_name'];
    this.imagePublicId = jsonObject['image_public_id'];
    this.imageVersion = jsonObject['image_version'];
    this.imageUrl = jsonObject['image_url'];
    this.parentId = jsonObject['parent_id'].toString();
    this.parent = [];
    if (jsonObject['parent'] != null) {
      for (var item in jsonObject['parent']) {
        this.parent.add(CategoryMain.fromJson(item));
      }
    }
  }
}
