class DeliveryPersonnel {
  String name;
  String phoneNumber;
  String id;

  DeliveryPersonnel(this.name, this.phoneNumber, this.id);
  DeliveryPersonnel.fromJson(Map<String, dynamic> jsonObject) {
    this.name = jsonObject['name'];
    this.id = jsonObject['id'].toString();
    this.phoneNumber = (jsonObject['phone_number'] != null)
        ? jsonObject['phone_number'].toString()
        : null;
  }
}
