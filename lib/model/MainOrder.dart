import 'package:symmart/model/DeliveryPersonnel.dart';
import 'package:symmart/model/Order.dart';
import 'package:symmart/model/user.dart';

class MainOrder {
  String id;
  String uniqueBankCode;
  String orderCode;
  String totalPrice;
  String discountPrice;
  String deliveryPrice;
  String paymentMethod;
  User customer;
  String deliveryMethod;
  String vendorDeliveryConfirmation;
  String customerDeliveryConfirmation;
  String paymentStatus;
  String orderPin;
  DeliveryPersonnel deliveryPersonnel;
  String responseCode;
  List<Order> order;

  MainOrder(
      this.id,
      this.uniqueBankCode,
      this.orderCode,
      this.totalPrice,
      this.customer,
      this.customerDeliveryConfirmation,
      this.deliveryMethod,
      this.deliveryPersonnel,
      this.deliveryPrice,
      this.discountPrice,
      this.orderPin,
      this.paymentMethod,
      this.paymentStatus,
      this.vendorDeliveryConfirmation,
      this.responseCode,
      this.order);

  MainOrder.fromJson(Map<String, dynamic> jsonObject) {
    this.id = (jsonObject['id'] != null) ? jsonObject['id'].toString() : null;
    this.uniqueBankCode = (jsonObject['unique_bank_code'] != null)
        ? jsonObject['unique_bank_code'].toString()
        : null;
    this.customer =
        (jsonObject['user'] != null) ? User.fromJson(jsonObject['user']) : null;
    this.customerDeliveryConfirmation =
        (jsonObject['customer_delivery_confirmation'] != null)
            ? jsonObject['customer_delivery_confirmation'].toString()
            : null;
    this.deliveryMethod = (jsonObject['delivery_method'] != null)
        ? jsonObject['delivery_method']
        : null;
    this.deliveryPersonnel = (jsonObject['d_personnel'] != null)
        ? DeliveryPersonnel.fromJson(jsonObject['d_personnel'])
        : null;
    this.deliveryPrice = (jsonObject['delivery_price'] != null)
        ? jsonObject['delivery_price'].toString()
        : null;
    this.discountPrice = (jsonObject['discount_price'] != null)
        ? jsonObject['discount_price'].toString()
        : null;
    this.orderCode = (jsonObject['order_code'] != null)
        ? jsonObject['order_code'].toString()
        : null;
    this.orderPin = (jsonObject['order_pin'] != null)
        ? jsonObject['order_pin'].toString()
        : null;
    this.paymentMethod = (jsonObject['payment_method'] != null)
        ? jsonObject['payment_method']
        : null;
    this.paymentStatus = (jsonObject['payment_status'] != null)
        ? jsonObject['payment_status'].toString()
        : null;
    this.totalPrice = (jsonObject['total_price'] != null)
        ? jsonObject['total_price'].toString()
        : null;
    this.vendorDeliveryConfirmation =
        jsonObject['vendor_delivery_confirmation'].toString();
    this.responseCode = (jsonObject["responseCode"] != null)
        ? jsonObject["responseCode"]
        : null;
    this.order = [];
    if (jsonObject['order'] != null) {
      for (var item in jsonObject['order']) {
        this.order.add(Order.fromJson(item));
      }
    }
  }
}
