import 'package:symmart/model/category.dart';

class HomeCarousel {
  String id;
  String imagePublicId;
  String imageVersion;
  CategoryMain category;

  HomeCarousel(this.imagePublicId, this.imageVersion, this.category, this.id);

  HomeCarousel.fromJson(Map<String, dynamic> jsonObject) {
    this.id = jsonObject['id'].toString();
    this.imagePublicId = jsonObject['image_public_id'];
    this.imageVersion = jsonObject['image_version'];
    this.category = CategoryMain.fromJson(jsonObject['category']);
  }
}
