class CartModel {
  int id;
  String name;
  int qty;
  double price;
  String size;
  String color;
  int userid;
  String img;
  int productid;
  int packageid;
  String associatedmodel;

  CartModel(
      {this.id,
      this.color,
      this.name,
      this.price,
      this.qty,
      this.size,
      this.userid,
      this.img,
      this.productid,
      this.associatedmodel,
      this.packageid});

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "qty": qty,
        "price": price,
        "size": size,
        "color": color,
        "userid": userid,
        "img": img,
        "productid": productid,
        "associatedmodel": associatedmodel,
        "packageid": packageid
      };

  factory CartModel.fromMap(Map<String, dynamic> json) => new CartModel(
      id: json['id'],
      name: json['name'],
      qty: json['qty'],
      price: json['price'],
      size: json['size'],
      color: json['color'],
      userid: json['userid'],
      img: json['img'],
      productid: json['productid'],
      packageid: json['packageid'],
      associatedmodel: json['associatedmodel']);
}
