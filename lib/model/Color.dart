class ColorClass {
  String id;
  String colourName;
  String hex;

  ColorClass(this.colourName, this.hex, this.id);

  ColorClass.fromJson(Map<String, dynamic> jsonColor) {
    this.colourName = jsonColor['colour_name'];
    this.hex = jsonColor['hex'];
    this.id = jsonColor['id'].toString();
  }
}
