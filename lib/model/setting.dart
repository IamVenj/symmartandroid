class CompanySettings {
  String id;
  String companyName;
  String slug;
  String location;
  String phoneNumber;
  String email;
  String facebook;
  String twitter;
  String googlePlus;
  String instagram;
  String productsAmountForDiscount;
  String discount;
  String deliveryperkm;
  String address;
  String lat;
  String lng;
  String cutIndividual;
  String cutBusiness;
  String shippingGuide;
  String shippingReturn;
  String about;
  String termsAndCondition;
  String creditHint;
  String onlinePaymentLimit;
  String symmartStandardPrice;
  String symmartPremiumPrice;
  String symmartNationalPrice;
  String symmartExpressPrice;

  CompanySettings(
      this.about,
      this.address,
      this.companyName,
      this.creditHint,
      this.cutBusiness,
      this.cutIndividual,
      this.deliveryperkm,
      this.discount,
      this.email,
      this.facebook,
      this.googlePlus,
      this.id,
      this.instagram,
      this.lat,
      this.lng,
      this.location,
      this.onlinePaymentLimit,
      this.phoneNumber,
      this.productsAmountForDiscount,
      this.shippingGuide,
      this.shippingReturn,
      this.slug,
      this.symmartExpressPrice,
      this.symmartNationalPrice,
      this.symmartPremiumPrice,
      this.symmartStandardPrice,
      this.termsAndCondition,
      this.twitter);

  CompanySettings.fromJson(Map<String, dynamic> jsonObject) {
    this.about = (jsonObject['about'] != null) ? jsonObject['about'] : null;
    this.address =
        (jsonObject['address'] != null) ? jsonObject['address'] : null;
    this.companyName = (jsonObject['company_name'] != null)
        ? jsonObject['company_name']
        : null;
    this.creditHint =
        (jsonObject['credit_hint'] != null) ? jsonObject['credit_hint'] : null;
    this.cutBusiness = (jsonObject['cut_business'] != null)
        ? jsonObject['cut_business'].toString()
        : null;
    this.cutIndividual = (jsonObject['cut_individual'] != null)
        ? jsonObject['cut_individual'].toString()
        : null;
    this.deliveryperkm = (jsonObject['delivery_per_km'] != null)
        ? jsonObject['delivery_per_km'].toString()
        : null;
    this.discount = (jsonObject['discount'] != null)
        ? jsonObject['discount'].toString()
        : null;
    this.email = (jsonObject['email'] != null) ? jsonObject['email'] : null;
    this.facebook =
        (jsonObject['facebook'] != null) ? jsonObject['facebook'] : null;
    this.googlePlus =
        (jsonObject['google_plus'] != null) ? jsonObject['google_plus'] : null;
    this.id = (jsonObject['id'] != null) ? jsonObject['id'].toString() : null;
    this.instagram =
        (jsonObject['instagram'] != null) ? jsonObject['instagram'] : null;
    this.lat =
        (jsonObject['lat'] != null) ? jsonObject['lat'].toString() : null;
    this.lng =
        (jsonObject['lng'] != null) ? jsonObject['lng'].toString() : null;
    this.location = (jsonObject['location'] != null)
        ? jsonObject['location'].toString()
        : null;
    this.onlinePaymentLimit = (jsonObject['online_payment_limit'] != null)
        ? jsonObject['online_payment_limit'].toString()
        : null;
    this.phoneNumber = (jsonObject['phone_number'] != null)
        ? jsonObject['phone_number'].toString()
        : null;
    this.productsAmountForDiscount =
        (jsonObject['products_amount_for_discount'] != null)
            ? jsonObject['products_amount_for_discount'].toString()
            : null;
    this.shippingGuide = (jsonObject['shipping_guide'] != null)
        ? jsonObject['shipping_guide']
        : null;
    this.shippingReturn = (jsonObject['shipping_return'] != null)
        ? jsonObject['shipping_return']
        : null;
    this.slug = (jsonObject['slug'] != null) ? jsonObject['slug'] : null;
    this.symmartExpressPrice = (jsonObject['symmart_express_price'] != null)
        ? jsonObject['symmart_express_price'].toString()
        : null;
    this.symmartNationalPrice = (jsonObject['symmart_national_price'] != null)
        ? jsonObject['symmart_national_price'].toString()
        : null;
    this.symmartPremiumPrice = (jsonObject['symmart_premium_price'] != null)
        ? jsonObject['symmart_premium_price'].toString()
        : null;
    this.symmartStandardPrice = (jsonObject['symmart_standard_price'] != null)
        ? jsonObject['symmart_standard_price'].toString()
        : null;
    this.twitter = (jsonObject['twitter'] != null)
        ? jsonObject['twitter'].toString()
        : null;
    this.termsAndCondition = (jsonObject['terms_and_condition'] != null)
        ? jsonObject['terms_and_condition']
        : null;
  }
}
