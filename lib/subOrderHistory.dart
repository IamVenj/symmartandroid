import 'package:flutter/material.dart';
import 'package:symmart/model/Order.dart';

class SubOrderHistory extends StatefulWidget {
  final String toolbarname;
  final List<Order> order;
  final mainOrder;
  SubOrderHistory(
      {Key key, this.toolbarname, this.order, @required this.mainOrder})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => StateSubOrderHistory(toolbarname);
}

class StateSubOrderHistory extends State<SubOrderHistory> {
  List list = ['12', '11'];
  bool checkboxValueA = true;
  bool checkboxValueB = false;
  bool checkboxValueC = false;
  VoidCallback showBottomSheetCallback;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String toolbarname;

  StateSubOrderHistory(this.toolbarname);

  @override
  Widget build(BuildContext context) {
    IconData _backIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.arrow_back;
        case TargetPlatform.iOS:
          return Icons.arrow_back_ios;
      }
      assert(false);
      return null;
    }

    // var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    // final double itemWidth = size.width / 2;

    // final Orientation orientation = MediaQuery.of(context).orientation;
    return new Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(_backIcon()),
            alignment: Alignment.centerLeft,
            tooltip: 'Back',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text("Order #$toolbarname"),
          backgroundColor: Colors.white,
        ),
        body: SubOrder(
          order: widget.order,
          mainOrder: widget.mainOrder,
        ));
  }

  erticalD() => Container(
        margin: EdgeInsets.only(left: 10.0, right: 0.0, top: 0.0, bottom: 0.0),
      );

  bool a = true;
  String mText = "Press to hide";
}

class SubOrder extends StatelessWidget {
  final List<Order> order;
  final mainOrder;
  SubOrder({Key key, @required this.order, @required this.mainOrder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return ListView.builder(
        itemCount: order.length,
        itemBuilder: (BuildContext cont, int ind) {
          return SafeArea(
              child: Column(children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    left: 10.0, right: 10.0, bottom: 5.0, top: 10.0),
                child: Card(
                    elevation: 4.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.02)),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          left: 0,
                          top: 0,
                          bottom: 0,
                          right: width * 0.7,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(width * 0.4),
                                    topRight: Radius.circular(width * 0.4)),
                                color: Colors.grey.shade100.withOpacity(0.7)),
                          ),
                        ),
                        Container(
                            padding: const EdgeInsets.fromLTRB(
                                10.0, 10.0, 10.0, 10.0),
                            child: GestureDetector(
                                child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    (order[ind].product != null)
                                        ? order[ind].product.productName
                                        : order[ind].package.packageName,
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontStyle: FontStyle.normal,
                                      color: Colors.black87,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 3.0),
                                ),
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    'Vendor :' + order[ind].vendor.shopName,
                                    style: TextStyle(
                                        fontSize: 13.0, color: Colors.black54),
                                  ),
                                ),
                                Divider(
                                  height: height * 0.01,
                                  color: Colors.black,
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: (order[ind].product != null)
                                      ? <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Item SKU',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      order[ind].product.sku,
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                // mainAxisAlignment:
                                                // MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Item Price',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      (order[ind].product !=
                                                              null)
                                                          ? "${order[ind].product.currentPrice} Birr"
                                                          : "${order[ind].package.discountPrice} Birr",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Qunatity',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      order[ind].quantity,
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ]
                                      : <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                // mainAxisAlignment:
                                                // MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Item Price',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      (order[ind].product !=
                                                              null)
                                                          ? "${order[ind].product.currentPrice} Birr"
                                                          : "${order[ind].package.discountPrice} Birr",
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Qunatity',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      order[ind].quantity,
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                        ],
                                ),
                                Divider(
                                  height: height * 0.01,
                                  color: Colors.black,
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        padding: EdgeInsets.all(3.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              'Total Price',
                                              style: TextStyle(
                                                  fontSize: 13.0,
                                                  color: Colors.black54),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 3.0),
                                              child: Text(
                                                (int.parse(order[ind]
                                                                .paidAmount) *
                                                            int.parse(order[ind]
                                                                .quantity))
                                                        .toString() +
                                                    ' Birr',
                                                style: TextStyle(
                                                    fontSize: 15.0,
                                                    color: Colors.black87),
                                              ),
                                            ),
                                          ],
                                        )),
                                    Container(
                                        padding: EdgeInsets.all(3.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              'Payment Status',
                                              style: TextStyle(
                                                  fontSize: 13.0,
                                                  color: Colors.black54),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 3.0),
                                              child: paymentStatement(
                                                  mainOrder.paymentStatus,
                                                  mainOrder.paymentMethod,
                                                  context),
                                            )
                                          ],
                                        )),
                                  ],
                                ),
                                Container(
                                    child: _status(
                                        int.parse(mainOrder
                                            .vendorDeliveryConfirmation),
                                        int.parse(mainOrder
                                            .customerDeliveryConfirmation)))
                              ],
                            ))),
                      ],
                    ))),
          ]));
        });
  }

  Widget _status(vendor, customer) {
    if (vendor == 1 && customer == 1) {
      return FlatButton.icon(
          color: Colors.green.shade100.withOpacity(0.2),
          label: Text(
            "Delivered",
            style: TextStyle(color: Colors.green),
          ),
          icon: const Icon(
            Icons.check_circle_outline,
            size: 18.0,
            color: Colors.green,
          ),
          onPressed: null);
    } else {
      return Container();
    }
  }

  Widget paymentStatement(paymentState, paymentMethod, context) {
    double width = MediaQuery.of(context).size.width;
    if (paymentState == "0" && paymentMethod == "bank") {
      return Row(
        children: <Widget>[
          Icon(Icons.move_to_inbox,
              color: Color(0xFF13A89E), size: width * 0.035),
          SizedBox(
            width: width * 0.01,
          ),
          Text(
            "Pending",
            style: TextStyle(fontSize: width * 0.035, color: Color(0xFF13A89E)),
          ),
        ],
      );
    } else if (paymentState == "0" && paymentMethod == "on-delivery") {
      return Row(
        children: <Widget>[
          Icon(Icons.not_interested,
              color: Color(0xFF13A89E), size: width * 0.035),
          SizedBox(
            width: width * 0.01,
          ),
          Text(
            "Not Paid",
            style: TextStyle(fontSize: width * 0.035, color: Colors.red),
          ),
        ],
      );
    } else if (paymentState == "1") {
      return Row(
        children: <Widget>[
          Icon(Icons.check_circle, color: Colors.green, size: width * 0.035),
          SizedBox(
            width: width * 0.01,
          ),
          Text(
            "Paid",
            style: TextStyle(fontSize: width * 0.035, color: Colors.green),
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}
