import 'package:cached_network_image/cached_network_image.dart';
import 'package:symmart/HomeScreen.dart';
import 'package:symmart/api/api_main_request.dart';
import 'package:symmart/checkout_screen.dart';
import 'package:flutter/material.dart';
import 'package:symmart/db/database.dart';
import 'package:symmart/model/cart.dart';
import 'package:symmart/model/setting.dart';

class CartScreen extends StatefulWidget {
  final List<CompanySettings> setting;

  const CartScreen({Key key, @required this.setting}) : super(key: key);
  @override
  State<StatefulWidget> createState() => Cart();
}

class Cart extends State<CartScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  var userData;
  var tokenData;
  bool isRemovingItem = false;

  num cartListQty = 0;

  @override
  void initState() {
    super.initState();
    getCartItem();
  }

  Future getUserInfo() async {
    userData = await ApiMain().getUser();
    tokenData = await ApiMain().getToken();
  }

  List<CartModel> cartList = [];
  Map<String, dynamic> cartData = {};
  int cartCount;
  num cartSubTotal = 0;

  getCartItem() async {
    await getUserInfo();
    if (userData != null) {
      await ClientDatabaseProvider.db
          .getAllCart(userData['id'])
          .then((returnedCartList) {
        for (var item in returnedCartList) {
          setState(() {
            cartList.add(item);
          });
        }
        setState(() {
          cartCount = returnedCartList.length;
        });
        totalPrice();
      });
    } else {
      setState(() {
        cartCount = 0;
      });
    }
  }

  IconData _backIcon() {
    switch (Theme.of(context).platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        return Icons.arrow_back;
      case TargetPlatform.iOS:
        return Icons.arrow_back_ios;
    }
    assert(false);
    return null;
  }

  clearCart() async {
    setState(() {
      isRemovingItem = true;
    });
    var user = await ApiMain().getUser();
    if (user != null) {
      await ClientDatabaseProvider.db.destroyCart(user['id']);
      cartList.clear();
      getCartItem();
    }
    setState(() {
      isRemovingItem = false;
    });
  }

  removeItem(id) async {
    setState(() {
      isRemovingItem = true;
    });
    var user = await ApiMain().getUser();
    if (user != null) {
      await ClientDatabaseProvider.db.destroyCartWithId(id);
      cartList.clear();
      getCartItem();
    }
    setState(() {
      isRemovingItem = false;
    });
  }

  String pincode;
  @override
  Widget build(BuildContext context) {
    String toolbarname = 'My Cart ($cartCount)';

    IconData addIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.add_circle;
        case TargetPlatform.iOS:
          return Icons.add_circle;
      }
      assert(false);
      return null;
    }

    IconData subIcon() {
      switch (Theme.of(context).platform) {
        case TargetPlatform.android:
        case TargetPlatform.fuchsia:
          return Icons.remove_circle;
        case TargetPlatform.iOS:
          return Icons.remove_circle;
      }
      assert(false);
      return null;
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    double dd = width * 0.82;

    return new Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(_backIcon()),
            alignment: Alignment.centerLeft,
            tooltip: 'Back',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            toolbarname,
          ),
          actions: <Widget>[
            FlatButton.icon(
                icon: Icon(Icons.clear_all),
                onPressed: !isRemovingItem
                    ? () {
                        clearCart();
                      }
                    : null,
                label: Text("Clear Cart"),
                textColor: Color(0xFF13A89E),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(width * 0.1),
                        bottomLeft: Radius.circular(width * 0.1))),
                color: Color(0x8013A89E).withOpacity(0.1),
                disabledColor: Colors.deepOrangeAccent.withOpacity(0.1))
          ],
          backgroundColor: Colors.white,
        ),
        body: (cartList.isNotEmpty)
            ? (widget.setting.isNotEmpty)
                ? Stack(
                    children: <Widget>[
                      isRemovingItem
                          ? Container(
                              width: width,
                              child: LinearProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(Color(0xFF13A89E)),
                                backgroundColor: Colors.white12,
                              ))
                          : Container(),
                      Column(
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(
                                  left: width * 0.022,
                                  top: height * 0.008,
                                  right: width * 0.02,
                                  bottom: 0.0),
                              height: height * 0.803,
                              child: ListView.builder(
                                  itemCount: cartList.length,
                                  itemBuilder: (BuildContext cont, int ind) {
                                    return SafeArea(
                                        child: Container(
                                      alignment: Alignment.topLeft,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                              alignment: Alignment.topLeft,
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                    height: height * 0.13,
                                                    width: dd,
                                                    child: Card(
                                                      elevation: 0.2,
                                                      child: Stack(
                                                        children: <Widget>[
                                                          Positioned(
                                                            right:
                                                                width * -0.05,
                                                            top: height * -0.01,
                                                            child:
                                                                FlatButton.icon(
                                                              icon: Icon(
                                                                  Icons.clear),
                                                              onPressed:
                                                                  !isRemovingItem
                                                                      ? () {
                                                                          removeItem(
                                                                              cartList[ind].id);
                                                                        }
                                                                      : null,
                                                              label:
                                                                  Container(),
                                                              textColor: Color(
                                                                  0xFF13A89E),
                                                              shape: RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.only(
                                                                      topLeft: Radius.circular(
                                                                          width *
                                                                              0.1),
                                                                      bottomLeft:
                                                                          Radius.circular(width *
                                                                              0.1))),
                                                              color: Color(
                                                                      0x8013A89E)
                                                                  .withOpacity(
                                                                      0.1),
                                                              disabledColor: Colors
                                                                  .deepOrangeAccent
                                                                  .withOpacity(
                                                                      0.1),
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: <Widget>[
                                                              SizedBox(
                                                                  height:
                                                                      height *
                                                                          0.17,
                                                                  width: width *
                                                                      0.2,
                                                                  child:
                                                                      CachedNetworkImage(
                                                                    imageUrl: (cartList[ind].img !=
                                                                            null)
                                                                        ? cartList[ind]
                                                                            .img
                                                                        : '',
                                                                    fit: BoxFit
                                                                        .contain,
                                                                    placeholder:
                                                                        (context,
                                                                                url) =>
                                                                            loader(),
                                                                    errorWidget: (context,
                                                                            url,
                                                                            error) =>
                                                                        empty(),
                                                                  )),
                                                              SizedBox(
                                                                  height:
                                                                      height *
                                                                          0.12,
                                                                  child:
                                                                      Container(
                                                                    width:
                                                                        width *
                                                                            0.27,
                                                                    alignment:
                                                                        Alignment
                                                                            .topLeft,
                                                                    child:
                                                                        Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left: width *
                                                                              0.02,
                                                                          top: height *
                                                                              0.01),
                                                                      child:
                                                                          Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        children: <
                                                                            Widget>[
                                                                          _verticalD(),
                                                                          Flexible(
                                                                            child:
                                                                                Text(
                                                                              (cartList[ind].name.length > 23) ? cartList[ind].name.substring(0, 23) + '...' : cartList[ind].name,
                                                                              textScaleFactor: 0.8,
                                                                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: height * 0.02, color: Colors.black),
                                                                            ),
                                                                          ),
                                                                          _verticalD(),
                                                                          Text(
                                                                            cartList[ind].price.toString() +
                                                                                ' ETB',
                                                                            textScaleFactor:
                                                                                0.86,
                                                                            style:
                                                                                TextStyle(fontSize: height * 0.019, color: Colors.black54),
                                                                          ),
                                                                          _verticalD(),
                                                                          Row(
                                                                            mainAxisAlignment: (cartList[ind].size != null)
                                                                                ? MainAxisAlignment.spaceEvenly
                                                                                : MainAxisAlignment.center,
                                                                            mainAxisSize:
                                                                                MainAxisSize.max,
                                                                            children: <Widget>[
                                                                              (cartList[ind].color != null)
                                                                                  ? Container(
                                                                                      height: height * 0.02,
                                                                                      width: width * 0.04,
                                                                                      decoration: BoxDecoration(color: Color(int.parse("0xFF" + cartList[ind].color.substring(1, 7))), borderRadius: BorderRadius.circular(width * 0.01)),
                                                                                    )
                                                                                  : Container(),
                                                                              Text(
                                                                                (cartList[ind].size != null) ? cartList[ind].size : "",
                                                                                textScaleFactor: 0.8,
                                                                                style: TextStyle(fontSize: width * 0.04, fontWeight: FontWeight.w600),
                                                                              )
                                                                            ],
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  )),
                                                              Padding(
                                                                padding: EdgeInsets.only(
                                                                    top: height *
                                                                        0.02),
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    new IconButton(
                                                                      icon: Icon(
                                                                          addIcon(),
                                                                          size: width *
                                                                              0.045,
                                                                          color: Colors
                                                                              .amber
                                                                              .shade500),
                                                                      onPressed:
                                                                          () {
                                                                        setState(
                                                                            () {
                                                                          cartList[ind].qty +=
                                                                              1;
                                                                        });
                                                                        updateQty(
                                                                            cartList[ind].id,
                                                                            cartList[ind].qty,
                                                                            cartList[ind].color,
                                                                            cartList[ind].img,
                                                                            cartList[ind].name,
                                                                            cartList[ind].price,
                                                                            cartList[ind].productid,
                                                                            cartList[ind].size,
                                                                            cartList[ind].userid,
                                                                            cartList[ind].associatedmodel,
                                                                            cartList[ind].packageid);
                                                                      },
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left: width *
                                                                              0.003),
                                                                    ),
                                                                    Text(
                                                                      cartList[
                                                                              ind]
                                                                          .qty
                                                                          .toString(),
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              width * 0.04),
                                                                      textScaleFactor:
                                                                          0.86,
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          right:
                                                                              width * 0.003),
                                                                    ),
                                                                    new IconButton(
                                                                      icon: Icon(
                                                                          subIcon(),
                                                                          size: width *
                                                                              0.045,
                                                                          color: Colors
                                                                              .amber
                                                                              .shade500),
                                                                      onPressed:
                                                                          () {
                                                                        setState(
                                                                            () {
                                                                          if (cartList[ind].qty <=
                                                                              1) {
                                                                            cartList[ind].qty =
                                                                                cartList[ind].qty;
                                                                          } else {
                                                                            cartList[ind].qty -=
                                                                                1;
                                                                          }
                                                                        });
                                                                        updateQty(
                                                                            cartList[ind].id,
                                                                            cartList[ind].qty,
                                                                            cartList[ind].color,
                                                                            cartList[ind].img,
                                                                            cartList[ind].name,
                                                                            cartList[ind].price,
                                                                            cartList[ind].productid,
                                                                            cartList[ind].size,
                                                                            cartList[ind].userid,
                                                                            cartList[ind].associatedmodel,
                                                                            cartList[ind].packageid);
                                                                      },
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Column(
                                                    children: <Widget>[
                                                      SizedBox(
                                                          height: height * 0.07,
                                                          width: width * 0.12,
                                                          child: Container(
                                                            alignment: Alignment
                                                                .center,
                                                            child: Text(
                                                              (cartList[ind].qty *
                                                                          cartList[ind]
                                                                              .price)
                                                                      .toString() +
                                                                  ' ETB',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      width *
                                                                          0.034),
                                                              textScaleFactor:
                                                                  0.86,
                                                            ),
                                                          )),
                                                      Icon(
                                                        Icons.add_shopping_cart,
                                                        color: Colors.black12,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              )),
                                        ],
                                      ),
                                    ));
                                  })),
                        ],
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        left: 0,
                        child: Container(
                            alignment: Alignment.bottomCenter,
                            height: height * 0.144,
                            child: Card(
                              elevation: 0.0,
                              borderOnForeground: true,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(width * 0.05),
                                      topRight: Radius.circular(width * 0.05))),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      (totalQty())
                                          ? Padding(
                                              padding: EdgeInsets.only(
                                                  top: height * 0.013),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                mainAxisSize: MainAxisSize.max,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: <Widget>[
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: width * 0.02,
                                                          right: width * 0.02),
                                                      child: Icon(
                                                        Icons
                                                            .check_circle_outline,
                                                        color: Colors.green,
                                                        size: width * 0.04,
                                                      )),
                                                  Text(
                                                    'Discount :',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    '${calculateDiscount()} ETB',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : Container(),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: height * 0.013),
                                        child: Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: width * 0.02,
                                                  right: width * 0.02),
                                              child: Icon(
                                                Icons.account_balance,
                                                color: Colors.green,
                                                size: width * 0.04,
                                              ),
                                            ),
                                            Text(
                                              'Sub-Total :',
                                              textScaleFactor: 0.8,
                                              style: TextStyle(
                                                  fontSize: width * 0.04,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(3.0),
                                            ),
                                            (!totalQty())
                                                ? Text(
                                                    '$cartSubTotal ETB',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  )
                                                : Text(
                                                    '${totalPriceWithDiscount()} ETB',
                                                    textScaleFactor: 0.8,
                                                    style: TextStyle(
                                                        fontSize: width * 0.04,
                                                        color: Colors.black54),
                                                  ),
                                            Padding(
                                              padding: EdgeInsets.all(3.0),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(width * 0.02),
                                    child: Container(
                                      alignment: Alignment.bottomCenter,
                                      child: MaterialButton(
                                          minWidth: width * 0.98,
                                          elevation: 0,
                                          color: Color(0x8013A89E)
                                              .withOpacity(0.2),
                                          child: Text(
                                            'Confirm Order',
                                            textScaleFactor: 0.8,
                                            style: TextStyle(
                                                fontSize: width * 0.04),
                                          ),
                                          focusElevation: 0,
                                          hoverElevation: 0,
                                          highlightElevation: 0,
                                          splashColor: Color(0x8013A89E)
                                              .withOpacity(0.2),
                                          highlightColor: Color(0x8013A89E)
                                              .withOpacity(0.2),
                                          textColor: Color(0xFF13A89E),
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        Checkout(
                                                            cartList: cartList,
                                                            userData: userData,
                                                            setting:
                                                                widget.setting,
                                                            totalPrice:
                                                                cartSubTotal)));
                                          },
                                          shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30.0),
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      )
                    ],
                  )
                : Center(
                    child: CircularProgressIndicator(
                    strokeWidth: 1,
                    valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                  ))
            : Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.remove_shopping_cart),
                  Text(
                    'Empty',
                    style: TextStyle(
                        fontSize: width * 0.08, fontWeight: FontWeight.bold),
                  ),
                  Text('Your cart seems to be empty!',
                      style: TextStyle(fontSize: width * 0.05)),
                  SizedBox(
                    height: height * 0.08,
                  ),
                  Center(
                      child: CircularProgressIndicator(
                    strokeWidth: 1,
                    valueColor: AlwaysStoppedAnimation(Color(0xFF13A89E)),
                  )),
                ],
              ));
  }

  updateQty(id, qty, color, img, name, price, productId, size, userid,
      associatedModel, packageid) async {
    if (associatedModel == "App\\Product") {
      await ClientDatabaseProvider.db.updateCartQty(new CartModel(
          id: id,
          qty: qty,
          color: color,
          img: img,
          name: name,
          price: price,
          productid: productId,
          size: size,
          userid: userid,
          associatedmodel: associatedModel,
          packageid: null));
    } else if (associatedModel == "App\\SpecialPackage") {
      await ClientDatabaseProvider.db.updateCartQty(new CartModel(
          id: id,
          qty: qty,
          color: null,
          img: img,
          name: name,
          price: price,
          productid: null,
          size: null,
          userid: userid,
          associatedmodel: associatedModel,
          packageid: packageid));
    }

    await ClientDatabaseProvider.db
        .getAllCart(userData['id'])
        .then((returnedCartList) {
      cartList.clear();
      for (var item in returnedCartList) {
        setState(() {
          cartList.add(item);
        });
      }
      setState(() {
        cartCount = returnedCartList.length;
      });
    });
    await totalPrice();
  }

  totalPrice() {
    cartSubTotal = 0;
    cartList.forEach((f) {
      setState(() {
        cartSubTotal += f.price * f.qty;
      });
    });
    return cartSubTotal;
  }

  totalPriceWithDiscount() {
    return cartSubTotal - calculateDiscount();
  }

  bool totalQty() {
    cartListQty = 0;
    cartList.forEach((f) {
      if (f != null) {
        setState(() {
          cartListQty += f.qty;
        });
      }
    });
    return (cartListQty >=
            int.parse(widget.setting.first.productsAmountForDiscount))
        ? true
        : false;
  }

  calculateDiscount() {
    return (int.parse(widget.setting.first.productsAmountForDiscount) / 100) *
        cartSubTotal;
  }

  verticalDivider() => Container(
        padding: EdgeInsets.all(2.0),
      );

  _verticalD() => Container(
        margin: EdgeInsets.only(left: 3.0, right: 0.0, top: 07.0, bottom: 0.0),
      );

  void showDemoDialog<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // The value passed to Navigator.pop() or null.
      if (value != null) {
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text('You selected: $value')));
      }
    });
  }
}
